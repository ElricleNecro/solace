#include <memory>
#include <string>

#include "Solace/core/entrypoint.h"
#include "Solace/spch.h"

#include "Editor/editor_layer.h"

namespace Solace {
	class Editor : public Application {
	public:
		Editor(const ApplicationSpecification &spec) : Application(spec) {
			this->push_overlay(new EditorLayer(this->get_width(), this->get_height()));
		}
	};

	Application *create_application(CLIArgs args) {
		ApplicationSpecification spec;

		spec.height = 1080;
		spec.width = 1920;
		spec.title = "Solace Editor";
		spec.cli_args = args;

		return new Editor(spec);
	}
} // namespace Solace
