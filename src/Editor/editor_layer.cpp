#include "Editor/editor_layer.h"

#include <chrono>
#include <cmath>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "imgui/imgui_internal.h"

#include <imguizmo/ImGuizmo.h>

#include "Solace/Solace.h"
#include "Solace/core/application.h"
#include "Solace/core/log.h"
#include "Solace/debug/instrumentor.h"
#include "Solace/math/math.h"
#include "Solace/scene/scene_serialiser.h"
#include "Solace/utils/platform_utils.h"

namespace Solace {
	EditorLayer::EditorLayer(int width, int height) : Layer("Solace Editor") /*, camera_controller(width / height, true)*/ {
		this->viewport_size = { width, height };
	}

	void EditorLayer::on_attach(void) {
		SL_PROFILE_FUNCTION();

		this->icon_play = Texture2D::create("assets/editor/icons/PlayButton.png");
		this->icon_simulate = Texture2D::create("assets/editor/icons/SimulateButton.png");
		this->icon_stop = Texture2D::create("assets/editor/icons/StopButton.png");

		FrameBufferSpecification spec;
		spec.attachments = { FrameBufferTextureFormat::RGBA8, FrameBufferTextureFormat::RED_INTEGER, FrameBufferTextureFormat::Depth };
		spec.width = Application::get().get_width();
		spec.height = Application::get().get_height();
		this->framebuffer = FrameBuffer::create(spec);

		//this->camera_controller.set_zoom_level(5.f);

		if( Application::get().get_spec().cli_args.argc > 1 ) {
			this->open_scene(Application::get().get_spec().cli_args[1]);
		} else {
			this->editor_scene = make_ref<Scene>();
			this->active_scene = this->editor_scene;
		}
		this->active_scene->on_viewport_resize(Application::get().get_width(), Application::get().get_height());

#if 0
		this->square = this->active_scene->add("Square");
		this->square.add<SpriteRendererComponent>(glm::vec4{ 0.0f, 1.0f, 0.0f, 1.0f });

		this->camera = this->active_scene->add("Camera");
		this->camera.add<CameraComponent>();

		this->second_camera = this->active_scene->add("Clip Camera");
		this->second_camera.add<CameraComponent>(false);

		class CameraController : public ScriptableEntity {
		public:
			void on_create(void) {
				auto &transform = this->get<TransformComponent>().translation;
				transform.x = rand() % 10 - 5.0f;
			}

			void on_destroy(void) {
			}

			void on_update([[maybe_unused]] float timestep) {
				auto &translation = this->get<TransformComponent>().translation;
				static const float speed = 5.0f;

				if (Application::get().get_events()[SDL_SCANCODE_W]) {
					translation.y -= speed * timestep;
				}
				if (Application::get().get_events()[SDL_SCANCODE_S]) {
					translation.y += speed * timestep;
				}
				if (Application::get().get_events()[SDL_SCANCODE_A]) {
					translation.x += speed * timestep;
				}
				if (Application::get().get_events()[SDL_SCANCODE_D]) {
					translation.x -= speed * timestep;
				}
			}
		};
		this->camera.add<NativeScriptComponent>().bind<CameraController>();
		this->second_camera.add<NativeScriptComponent>().bind<CameraController>();
#endif

		this->hierarchy_panel.set_context(this->active_scene);
		this->editor_camera = EditorCamera(30.0f, 1.778f, 0.1f, 1000.0f);
		this->editor_camera.set_viewport_size(Application::get().get_width(), Application::get().get_height());

		Renderer2D::set_line_width(4.f);
	}

	void EditorLayer::on_update(float elapsed_time) {
		SL_PROFILE_FUNCTION();

		if (FrameBufferSpecification spec = this->framebuffer->get_specification();
		    this->viewport_size.x > 0.0f && this->viewport_size.y > 0.0f &&
		    (spec.width != this->viewport_size.x || spec.height != this->viewport_size.y)) {
			this->framebuffer->resize((uint32_t)this->viewport_size.x, (uint32_t)this->viewport_size.y);
			//this->camera_controller.resize(this->viewport_size.x, this->viewport_size.y);

			this->editor_camera.set_viewport_size((uint32_t)this->viewport_size.x, (uint32_t)this->viewport_size.y);
			this->active_scene->on_viewport_resize((uint32_t)this->viewport_size.x, (uint32_t)this->viewport_size.y);
		}

#ifdef SL_PROFILE
		Renderer2D::ResetStats();
#endif

		this->framebuffer->bind();
		RenderCommand::set_clear_color(this->clear_color);
		RenderCommand::clear();
		this->framebuffer->clear_color_attachment(1, -1);

		switch (this->scene_state) {
		case SceneState::Edit:
			//if (this->viewport_focused)
			//this->camera_controller.on_update(elapsed_time);

			this->editor_camera.on_update(elapsed_time);

			this->active_scene->on_update_editor(this->editor_camera, elapsed_time);
			break;

		case SceneState::Simulate:
			this->editor_camera.on_update(elapsed_time);

			this->active_scene->on_update_simulation(this->editor_camera, elapsed_time);
			break;

		case SceneState::Play:
			this->active_scene->on_update_runtime(elapsed_time);
			break;
		}

		auto [mx, my] = ImGui::GetMousePos();
		mx -= this->viewport_bounds[0].x;
		my -= this->viewport_bounds[0].y;

		glm::vec2 viewport_size = this->viewport_bounds[1] - this->viewport_bounds[0];
		my = viewport_size.y - my;

		int mouse_x = (int)mx;
		int mouse_y = (int)my;

		if (mouse_x >= 0 && mouse_y >= 0 && mouse_x < viewport_size.x && mouse_y < viewport_size.y) {
			int pixel_data = this->framebuffer->read_pixel(1, mouse_x, mouse_y);
			if (pixel_data == -1)
				this->hovered_entity = {};
			else
				this->hovered_entity = Entity(pixel_data, this->active_scene.get());
		}

		this->on_overlay_render();

		this->framebuffer->unbind();
	}

	void EditorLayer::ui_toolbar(void) {
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 2));
		ImGui::PushStyleVar(ImGuiStyleVar_ItemInnerSpacing, ImVec2(0, 0));

		auto &colors = ImGui::GetStyle().Colors;
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0, 0, 0, 0));
		ImGui::PushStyleColor(
			ImGuiCol_ButtonHovered,
			ImVec4(colors[ImGuiCol_ButtonHovered].x, colors[ImGuiCol_ButtonHovered].y, colors[ImGuiCol_ButtonHovered].z, 0.5f));
		ImGui::PushStyleColor(
			ImGuiCol_ButtonActive,
			ImVec4(colors[ImGuiCol_ButtonActive].x, colors[ImGuiCol_ButtonActive].y, colors[ImGuiCol_ButtonActive].z, 0.5f));

		ImGui::Begin("##toolbar", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);

		bool enable_toolbar = (bool)this->active_scene;
		ImVec4 tint_color = ImVec4(1, 1, 1, 1);
		if( ! enable_toolbar )
			tint_color.w = 0.5f;

		float size = ImGui::GetWindowHeight() - 4.0f;
		{
			auto icon = this->scene_state == SceneState::Play ? this->icon_stop : this->icon_play;

			ImGui::SetCursorPosX(ImGui::GetWindowContentRegionMax().x * 0.5f - (size * 0.5f));
			if (ImGui::ImageButton(reinterpret_cast<ImTextureID>(icon->get_renderer_id()), ImVec2(size, size), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0.f, 0.f, 0.f, 0.f), tint_color) && enable_toolbar ) {
				if (this->scene_state == SceneState::Edit || this->scene_state == SceneState::Simulate )
					this->scene_play();
				else if (this->scene_state == SceneState::Play)
					this->scene_stop();
			}
		}

		ImGui::SameLine();
		{
			auto icon = this->scene_state == SceneState::Simulate ? this->icon_stop : this->icon_simulate;

			// ImGui::SetCursorPosX(ImGui::GetWindowContentRegionMax().x * 0.5f - (size * 0.5f));
			if (ImGui::ImageButton(reinterpret_cast<ImTextureID>(icon->get_renderer_id()), ImVec2(size, size), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0.f, 0.f, 0.f, 0.f), tint_color) && enable_toolbar ) {
				if (this->scene_state == SceneState::Edit || this->scene_state == SceneState::Play )
					this->scene_simulate();
				else if (this->scene_state == SceneState::Simulate)
					this->scene_stop();
			}
		}

		ImGui::PopStyleVar(2);
		ImGui::PopStyleColor(3);

		ImGui::End();
	}

	void EditorLayer::on_imgui_render(void) {
		SL_PROFILE_FUNCTION();

		ImGuiStyle &style = ImGui::GetStyle();
		float min_win_size_x = style.WindowMinSize.x;
		style.WindowMinSize.x = 370.0f;

		ImGuiID dockspace = ImGui::DockSpaceOverViewport(nullptr, ImGuiDockNodeFlags_AutoHideTabBar);

		if (!this->initial_layout_done) {
			this->initial_layout_done = true;

			ImGuiDockNode *root = ImGui::DockBuilderGetNode(dockspace);
			if (root && root->ChildNodes[0] && root->CentralNode) {
				this->viewport_dock_id = root->CentralNode->ID;
				this->hierarchy_panel_dock_id = root->ChildNodes[0]->ID;
				this->settings_dock_id = root->ChildNodes[1]->ID;
				this->browser_dock_id = root->ChildNodes[1]->ChildNodes[1]->ID;
			} else {
				ImGui::DockBuilderRemoveNode(dockspace);

				this->viewport_dock_id = ImGui::DockBuilderAddNode(dockspace, ImGuiDockNodeFlags_DockSpace);
				ImGui::DockBuilderSetNodeSize(this->viewport_dock_id,
							      ImVec2{ (float)Application::get().get_width(),
								      (float)Application::get().get_height() });

				this->hierarchy_panel_dock_id =
					ImGui::DockBuilderSplitNode(this->viewport_dock_id, ImGuiDir_Left, 0.2f, nullptr, &this->viewport_dock_id);
				this->hierarchy_panel_dock_id = this->hierarchy_panel.set_dock_id(this->hierarchy_panel_dock_id);
				this->settings_dock_id =
					ImGui::DockBuilderSplitNode(this->viewport_dock_id, ImGuiDir_Right, 0.2f, nullptr, &this->viewport_dock_id);
				this->stats_dock_id =
					ImGui::DockBuilderSplitNode(this->settings_dock_id, ImGuiDir_Down, 0.2f, nullptr, &this->settings_dock_id);
				this->browser_dock_id =
					ImGui::DockBuilderSplitNode(this->viewport_dock_id, ImGuiDir_Down, 0.3f, nullptr, &this->viewport_dock_id);
				this->toolbar_dock_id =
					ImGui::DockBuilderSplitNode(this->viewport_dock_id, ImGuiDir_Up, 0.05f, nullptr, &this->viewport_dock_id);

				ImGui::DockBuilderFinish(dockspace);
			}
		}

		style.WindowMinSize.x = min_win_size_x;

		if (ImGui::BeginMainMenuBar()) {
			if (ImGui::BeginMenu("File")) {
				if (ImGui::MenuItem("New", "Ctrl+N")) {
					this->new_scene();
				}

				if (ImGui::MenuItem("Open...", "Ctrl+O")) {
					this->open_scene();
				}

				if (ImGui::MenuItem("Save...", "Ctrl+S")) {
					this->save_scene(this->active_scene_path);
				}

				if (ImGui::MenuItem("Save as...", "Ctrl+Shift+S")) {
					this->save_scene();
				}

				if (ImGui::MenuItem("Exit", "Ctrl+Q")) {
					Application::get().quit();
				}

				ImGui::EndMenu();
			}

			if (ImGui::BeginMenu("Entity")) {
				if (ImGui::MenuItem("Duplicate", "Ctrl+D")) {
					this->on_duplicate_entity();
				}

				if (ImGui::MenuItem("Translate", "Z")) {
					this->imguizmo_op = ImGuizmo::OPERATION::TRANSLATE;
				}

				if (ImGui::MenuItem("Rotate", "E")) {
					this->imguizmo_op = ImGuizmo::OPERATION::ROTATE;
				}

				if (ImGui::MenuItem("Scale", "R")) {
					this->imguizmo_op = ImGuizmo::OPERATION::SCALE;
				}

				ImGui::EndMenu();
			}

			ImGui::EndMainMenuBar();
		}

		ImGui::SetNextWindowDockID(this->hierarchy_panel_dock_id, ImGuiCond_FirstUseEver);
		this->hierarchy_panel.on_imgui_render();

		ImGui::SetNextWindowDockID(this->browser_dock_id, ImGuiCond_FirstUseEver);
		this->browser_panel.on_imgui_render();

		ImGui::SetNextWindowDockID(this->settings_dock_id, ImGuiCond_FirstUseEver);
		if (ImGui::Begin("Settings")) {
			ImGui::Checkbox("Show physics colliders", &this->show_physics_colliders);
			ImGui::ColorEdit4("Collider color",
					  glm::value_ptr(this->collider_color)); // Edit 3 floats representing a color
			ImGui::ColorEdit4("clear color",
					  glm::value_ptr(this->clear_color)); // Edit 3 floats representing a color
			ImGui::End();
		}

		ImGui::SetNextWindowDockID(this->stats_dock_id, ImGuiCond_FirstUseEver);
		if (ImGui::Begin("Stats")) {
			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			if (this->hovered_entity)
				ImGui::Text("Hovered entity: %s", this->hovered_entity.get<TagComponent>().tag.c_str());
			else
				ImGui::Text("No entity hovered.");

#ifdef SL_PROFILE
			auto stats = Renderer2D::get_stats();
			ImGui::Separator();
			ImGui::Text("Statistics:");
			ImGui::Text("Draw calls: %d", stats.draw_calls);
			ImGui::Text("Quads: %d", stats.quad_count);

			ImGui::Text("Vertices: %d", stats.get_total_vertex_count());
			ImGui::Text("Indices: %d", stats.get_total_index_count());
			ImGui::Separator();
#endif
			ImGui::End();
		}

		ImGui::SetNextWindowDockID(this->toolbar_dock_id, ImGuiCond_FirstUseEver);
		this->ui_toolbar();

		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2{ 0, 0 });
		ImGui::SetNextWindowDockID(this->viewport_dock_id, ImGuiCond_FirstUseEver);
		if (ImGui::Begin("Viewport")) {
			auto viewport_min_region = ImGui::GetWindowContentRegionMin();
			auto viewport_max_region = ImGui::GetWindowContentRegionMax();
			auto offset = ImGui::GetWindowPos();
			this->viewport_bounds[0] = { viewport_min_region.x + offset.x, viewport_min_region.y + offset.y };
			this->viewport_bounds[1] = { viewport_max_region.x + offset.x, viewport_max_region.y + offset.y };

			this->viewport_focused = ImGui::IsWindowFocused();
			this->viewport_hovered = ImGui::IsWindowHovered();
			//SL_INFO("Should we block: {}", !this->viewport_focused || !this->viewport_hovered);
			Application::get().get_imgui_layer()->set_block_events(!this->viewport_focused && !this->viewport_hovered);

			ImVec2 im_viewport_size = ImGui::GetContentRegionAvail();
			glm::vec2 glm_viewport_size(im_viewport_size.x, im_viewport_size.y);
			if (im_viewport_size.x > 0 && im_viewport_size.y > 0 && this->viewport_size != glm_viewport_size) {
				this->viewport_size = { im_viewport_size.x, im_viewport_size.y };
			}

			uint64_t tid = this->framebuffer->get_color_attachement_renderer_id();
			ImGui::Image(reinterpret_cast<void *>(tid),
				     ImVec2{ this->viewport_size.x, this->viewport_size.y },
				     ImVec2{ 0, 1 },
				     ImVec2{ 1, 0 });

			if (ImGui::BeginDragDropTarget()) {
				if (const ImGuiPayload *payload = ImGui::AcceptDragDropPayload("CONTENT_BROWSER_ITEM")) {
					const char *path = reinterpret_cast<const char *>(payload->Data);
					SL_CORE_TRACE("Opening scene '{}'.", path);
					this->open_scene(path);
				}

				ImGui::EndDragDropTarget();
			}

			// Guizmos:
			Entity selected = this->hierarchy_panel;
			if (selected && this->imguizmo_op != -1) {
				ImGuizmo::SetOrthographic(false);
				ImGuizmo::SetDrawlist();

				ImGuizmo::SetRect(this->viewport_bounds[0].x,
						  this->viewport_bounds[0].y,
						  this->viewport_bounds[1].x - this->viewport_bounds[0].x,
						  this->viewport_bounds[1].y - this->viewport_bounds[0].y);

				// Camera:
				//auto camera_entity = this->active_scene->get_primary_camera_entity();
				//const auto &camera = camera_entity.get<CameraComponent>().camera;
				//const glm::mat4 &camera_proj = camera;
				//glm::mat4 camera_view = glm::inverse(camera_entity.get<TransformComponent>().get_transform());
				const glm::mat4 &camera_proj = this->editor_camera.get_projection();
				glm::mat4 camera_view = this->editor_camera.get_view_matrix();

				// Entity transform
				auto &tc = selected.get<TransformComponent>();
				glm::mat4 transform = tc;

				bool snap = Application::get_events_data()[SDL_SCANCODE_LCTRL] || Application::get_events_data()[SDL_SCANCODE_RCTRL];
				float snap_value = 0.5f;
				if (this->imguizmo_op == ImGuizmo::OPERATION::ROTATE) {
					snap_value = 45.0f;
				}
				float snap_values[3] = { snap_value, snap_value, snap_value };

				ImGuizmo::Manipulate(glm::value_ptr(camera_view),
						     glm::value_ptr(camera_proj),
						     (ImGuizmo::OPERATION)this->imguizmo_op,
						     ImGuizmo::LOCAL,
						     glm::value_ptr(transform),
						     nullptr,
						     snap ? snap_values : nullptr);

				if (ImGuizmo::IsUsing()) {
					glm::vec3 rotation;
					Math::decompose_transform(transform, tc.translation, rotation, tc.scale);

					glm::vec3 delta_rotation = rotation - tc.rotation;
					tc.rotation += delta_rotation;
				}
			}

			ImGui::End();
		}
		ImGui::PopStyleVar();
	}

	bool EditorLayer::on_event([[maybe_unused]] Events &evt) {
		if (this->scene_state == SceneState::Edit && this->editor_camera.on_event(evt))
			return true;

		bool control = evt[SDL_SCANCODE_LCTRL] || evt[SDL_SCANCODE_RCTRL];
		bool shift = evt[SDL_SCANCODE_RSHIFT] || evt[SDL_SCANCODE_LSHIFT];

		if (!evt[SDL_SCANCODE_LALT] && evt.mouse()[SDL_BUTTON_LEFT] && this->viewport_hovered && !ImGuizmo::IsUsing() &&
		    !ImGuizmo::IsOver()) {
			this->hierarchy_panel.set_selected_entity(this->hovered_entity);
			return true;
		}

		switch ((SDL_Scancode)evt) {
		case SDL_SCANCODE_N:
			if (control) {
				this->new_scene();
				return true;
			}
			break;

		case SDL_SCANCODE_O:
			if (control) {
				this->open_scene();
				return true;
			}
			break;

		case SDL_SCANCODE_S:
			if (control && shift) {
				this->save_scene();
				return true;
			} else if (control) {
				this->save_scene(this->active_scene_path);
				return true;
			}
			break;

		case SDL_SCANCODE_A:
			if (control) {
				Application::get().quit();
				return true;
			}
			break;

		case SDL_SCANCODE_D:
			if (control) {
				this->on_duplicate_entity();
			}
			break;

		case SDL_SCANCODE_Q:
			this->imguizmo_op = -1;
			return true;
			break;

		case SDL_SCANCODE_W:
			this->imguizmo_op = ImGuizmo::OPERATION::TRANSLATE;
			return true;
			break;

		case SDL_SCANCODE_E:
			this->imguizmo_op = ImGuizmo::OPERATION::ROTATE;
			return true;
			break;

		case SDL_SCANCODE_R:
			this->imguizmo_op = ImGuizmo::OPERATION::SCALE;
			return true;
			break;

		default:
			break;
		}

		return false;
		//return this->camera_controller.on_event(evt);
	}

	void EditorLayer::on_detach(void) {
		SL_PROFILE_FUNCTION();
	}

	void EditorLayer::on_overlay_render(void) {
		switch (this->scene_state) {
		case SceneState::Edit:
		case SceneState::Simulate:
			Renderer2D::begin_scene(this->editor_camera);
			break;
		case SceneState::Play:
			Entity camera = this->active_scene->get_primary_camera_entity();
			if(! camera)
				return;
			Renderer2D::begin_scene(camera.get<CameraComponent>().camera, camera.get<TransformComponent>());
			break;
		}

		if (this->show_physics_colliders) {
			{
				auto view = this->active_scene->get_all_entities_with<TransformComponent, CircleCollider2DComponent>();
				for (auto entity : view) {
					auto [tc, cc2d] = view.get<TransformComponent, CircleCollider2DComponent>(entity);
					glm::vec3 translation = tc.translation + glm::vec3(cc2d.offset, 0.001f);
					glm::vec3 scale = tc.scale * glm::vec3(2.0f * cc2d.radius);

					glm::mat4 transform = glm::translate(glm::mat4(1.0f), translation) *
							      glm::rotate(glm::mat4(1.0f), tc.rotation.z, glm::vec3(0.f, 0.f, 1.f)) *
							      glm::scale(glm::mat4(1.0f), scale);

					Renderer2D::draw_circle(transform, this->collider_color, 0.005f);
				}
			}

			{
				auto view = this->active_scene->get_all_entities_with<TransformComponent, BoxCollider2DComponent>();
				for (auto entity : view) {
					auto [tc, bc2d] = view.get<TransformComponent, BoxCollider2DComponent>(entity);
					glm::vec3 translation = tc.translation + glm::vec3(bc2d.offset, 0.001f);
					glm::vec3 scale = tc.scale * glm::vec3(2.f * bc2d.size, 1.0f);

					glm::mat4 transform = glm::translate(glm::mat4(1.0f), translation) *
							      glm::rotate(glm::mat4(1.0f), tc.rotation.z, glm::vec3(0.f, 0.f, 1.f)) *
							      glm::scale(glm::mat4(1.0f), scale);

					Renderer2D::draw_rect(transform, this->collider_color);
				}
			}
		}

		if( Entity selected = this->hierarchy_panel.get_selected_entity() ) {
			const TransformComponent &transform = selected.get<TransformComponent>();
			Renderer2D::draw_rect(transform.get_transform(), glm::vec4(1.f, 0.5f, 0.f, 1.f));
		}

		Renderer2D::end_scene();
	}

	void EditorLayer::new_scene(void) {
		if( this->scene_state != SceneState::Edit )
			return;

		this->editor_scene = make_ref<Scene>();
		this->active_scene = this->editor_scene;

		this->active_scene->on_viewport_resize(this->viewport_size.x, this->viewport_size.y);

		this->hierarchy_panel.set_context(this->active_scene);
		this->active_scene_path = std::filesystem::path();
	}

	void EditorLayer::open_scene(std::filesystem::path path) {
		if (this->scene_state != SceneState::Edit)
			this->scene_stop();

		if (path.empty())
			path = FileDialogs::open_file("solace");

		if (path.empty()) {
			SL_CORE_WARN("No scene were selected.");
			return;
		}

		this->editor_scene = make_ref<Scene>();
		if (SceneSerialiser(this->editor_scene).deserialise(path)) {
			this->editor_scene->on_viewport_resize(this->viewport_size.x, this->viewport_size.y);

			this->active_scene = this->editor_scene;
			this->hierarchy_panel.set_context(this->active_scene);

			this->active_scene_path = path;
		} else {
			SL_CORE_ERROR("Unable to load scene '{}'.", path.c_str());
		}
	}

	void EditorLayer::save_scene(std::filesystem::path path) {
		if (this->scene_state != SceneState::Edit)
			this->scene_stop();

		if (path.empty()) {
			path = FileDialogs::save_file("solace");
			this->active_scene_path = path;
		}

		if (!path.empty()) {
			SceneSerialiser(this->active_scene).serialise(path);
		}
	}

	void EditorLayer::scene_play(void) {
		if( this->scene_state == SceneState::Simulate )
			this->scene_stop();

		this->scene_state = SceneState::Play;

		this->active_scene = Scene::copy(this->editor_scene);
		this->active_scene->on_runtime_start();

		this->previous_imguizmo_op = this->imguizmo_op;
		this->imguizmo_op = -1;
	}

	void EditorLayer::scene_stop(void) {
		switch(this->scene_state) {
			case SceneState::Play:
				this->active_scene->on_runtime_stop();
				break;
			case SceneState::Simulate:
				this->active_scene->on_simulation_stop();
				break;
			case SceneState::Edit:
			default:
				SOLACE_CORE_ASSERT(false, "We shouldn't hit this point.");
		}

		this->scene_state = SceneState::Edit;

		this->active_scene = this->editor_scene;
		this->imguizmo_op = this->previous_imguizmo_op;
	}

	void EditorLayer::scene_simulate(void) {
		if( this->scene_state == SceneState::Play )
			this->scene_stop();

		this->scene_state = SceneState::Simulate;

		this->active_scene = Scene::copy(this->editor_scene);
		this->active_scene->on_simulation_start();

		this->previous_imguizmo_op = this->imguizmo_op;
		this->imguizmo_op = -1;
	}

	void EditorLayer::on_duplicate_entity(void) {
		if (this->scene_state != SceneState::Edit)
			return;

		Entity entity = this->hierarchy_panel.get_selected_entity();
		if (entity) {
			this->editor_scene->duplicate(entity);
		}
	}
} // namespace Solace
