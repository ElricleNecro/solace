#include "Solace/spch.h"

#include "Editor/panels/content_browser_panel.h"

namespace Solace {
	const std::filesystem::path ContentBrowserPanel::root = "assets";

	ContentBrowserPanel::ContentBrowserPanel(void) {
		this->dir_icon = Texture2D::create("assets/editor/icons/content_browser/DirectoryIcon.png");
		this->file_icon = Texture2D::create("assets/editor/icons/content_browser/FileIcon.png");
	}

	void ContentBrowserPanel::on_imgui_render(void) {
		if (ImGui::Begin("Content Browser")) {
			if (this->current_path != ContentBrowserPanel::root) {
				if (ImGui::Button("<-")) {
					this->current_path = this->current_path.parent_path();
				}
			}

			static float padding = 16.0f;
			static float thumbnail_size = 128.0f;

			const float cell_size = thumbnail_size + padding;
			const float panel_width = ImGui::GetContentRegionAvail().x;
			int column_count = (int)(panel_width / cell_size);
			if (column_count < 1)
				column_count = 1;

			ImGui::Columns(column_count, 0, false);

			for (auto &entry : std::filesystem::directory_iterator(this->current_path)) {
				const auto &path = entry.path();
				auto rel_path = std::filesystem::relative(path, ContentBrowserPanel::root);
				std::string fname = rel_path.filename().string();

				ImGui::PushID(fname.c_str());
				Ref<Texture2D> icon = entry.is_directory() ? this->dir_icon : this->file_icon;
				ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0, 0, 0, 0));
				ImGui::ImageButton(reinterpret_cast<ImTextureID>(icon->get_renderer_id()), { thumbnail_size, thumbnail_size }, { 0, 1 }, { 1, 0 });
				ImGui::PopStyleColor();

				if (ImGui::BeginDragDropSource()) {
					const auto fpath = this->current_path / rel_path.filename();
					const char *item_path = fpath.c_str();

					//SL_CORE_TRACE("Sending payload '{}'.", item_path);
					ImGui::SetDragDropPayload("CONTENT_BROWSER_ITEM", item_path, (1 + strlen(item_path)) * sizeof(char), ImGuiCond_Once);

					ImGui::EndDragDropSource();
				}

				if (entry.is_directory() && ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
					this->current_path /= rel_path.filename();
				}
				ImGui::TextWrapped("%s", fname.c_str());

				ImGui::PopID();

				ImGui::NextColumn();
			}

			ImGui::Columns(1);
			ImGui::SliderFloat("Thumbnail size", &thumbnail_size, 16, 512);
			ImGui::SliderFloat("Padding", &padding, 0, 32);

			ImGui::End();
		}
	}
} // namespace Solace
