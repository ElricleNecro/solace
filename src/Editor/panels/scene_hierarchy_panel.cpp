#include "Solace/scene/components.h"
#include "Solace/spch.h"

#include "Editor/panels/scene_hierarchy_panel.h"

#include <glm/gtc/type_ptr.hpp>
#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

namespace Solace {
	SceneHierarchyPanel::SceneHierarchyPanel(const Ref<Scene> &scene) {
		this->set_context(scene);
	}

	void SceneHierarchyPanel::set_context(const Ref<Scene> &scene) {
		this->context = scene;
		this->selection_context = {};
	}

	static void draw_vec3_control(const std::string &label, glm::vec3 &values, float reset_value = 0.0f, float column_width = 100.f) {
		ImGuiIO &io = ImGui::GetIO();
		auto bold_font = io.Fonts->Fonts[0];

		ImGui::PushID(label.c_str());
		ImGui::Columns(2);

		ImGui::SetColumnWidth(0, column_width);
		ImGui::Text(label.c_str());
		ImGui::NextColumn();

		ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());
		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{ 0, 0 });

		float line_height = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;
		ImVec2 button_size = { line_height + 3.0f, line_height };

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.8f, 0.1f, 0.15f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.9f, 0.2f, 0.2f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.8f, 0.1f, 0.15f, 1.0f });
		ImGui::PushFont(bold_font);
		if (ImGui::Button("X", button_size))
			values.x = reset_value;
		ImGui::PopFont();
		ImGui::PopStyleColor(3);

		ImGui::SameLine();
		ImGui::DragFloat("##X", &values.x, 0.1f, 0.0f, 0.0f, "%.2f");
		ImGui::PopItemWidth();
		ImGui::SameLine();

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.2f, 0.7f, 0.2f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.3f, 0.8f, 0.3f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.2f, 0.7f, 0.2f, 1.0f });
		ImGui::PushFont(bold_font);
		if (ImGui::Button("Y", button_size))
			values.y = reset_value;
		ImGui::PopFont();
		ImGui::PopStyleColor(3);

		ImGui::SameLine();
		ImGui::DragFloat("##Y", &values.y, 0.1f, 0.0f, 0.0f, "%.2f");
		ImGui::PopItemWidth();
		ImGui::SameLine();

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.1f, 0.25f, 0.8f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.2f, 0.35f, 0.9f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.1f, 0.25f, 0.8f, 1.0f });
		ImGui::PushFont(bold_font);
		if (ImGui::Button("Z", button_size))
			values.z = reset_value;
		ImGui::PopFont();
		ImGui::PopStyleColor(3);

		ImGui::SameLine();
		ImGui::DragFloat("##Z", &values.z, 0.1f, 0.0f, 0.0f, "%.2f");
		ImGui::PopItemWidth();
		ImGui::SameLine();

		ImGui::PopStyleVar();

		ImGui::Columns(1);
		ImGui::PopID();
	}

	template <typename T, typename UIFunction>
	static void draw_component(const std::string &name, Entity entity, UIFunction function) {
		ImGuiTreeNodeFlags tree_flags = ImGuiTreeNodeFlags_DefaultOpen;
		tree_flags |= ImGuiTreeNodeFlags_AllowItemOverlap;
		tree_flags |= ImGuiTreeNodeFlags_Framed;
		tree_flags |= ImGuiTreeNodeFlags_FramePadding;
		tree_flags |= ImGuiTreeNodeFlags_SpanAvailWidth;

		if (entity.has<T>()) {
			auto &component = entity.get<T>();
			const ImVec2 content_region_avail = ImGui::GetContentRegionAvail();

			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2{ 4, 4 });
			const float line_height = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;

			ImGui::Separator();

			bool opened = ImGui::TreeNodeEx((void *)typeid(T).hash_code(), tree_flags, name.c_str());

			ImGui::PopStyleVar();

			ImGui::SameLine(content_region_avail.x - line_height * 0.5f);
			if (ImGui::Button("+", ImVec2{ line_height, line_height })) {
				ImGui::OpenPopup("ComponentSettings");
			}

			bool remove_component = false;

			if (ImGui::BeginPopup("ComponentSettings")) {
				if (ImGui::MenuItem("Remove component"))
					remove_component = true;

				ImGui::EndPopup();
			}

			if (opened) {
				function(component);
				ImGui::TreePop();
			}

			if (remove_component) {
				entity.remove<T>();
			}
		}
	}

	void SceneHierarchyPanel::draw_components(Entity entity) {
		if (entity.has<TagComponent>()) {
			auto &tag = entity.get<TagComponent>().tag;

			char buffer[256] = { 0 };
			strncpy(buffer, tag.c_str(), sizeof(buffer));
			if (ImGui::InputText("##Tag", buffer, sizeof(buffer) - 1)) {
				tag = std::string(buffer);
			}
		}

		ImGui::SameLine();
		ImGui::PushItemWidth(-1);

		if (ImGui::Button("Add component"))
			ImGui::OpenPopup("AddComponent");

		if (ImGui::BeginPopup("AddComponent")) {
			this->display_add_component_entry<CameraComponent>("Camera");
			this->display_add_component_entry<TransformComponent>("Transform");
			this->display_add_component_entry<SpriteRendererComponent>("Sprite Renderer");
			this->display_add_component_entry<CircleRendererComponent>("Circle Renderer");
			this->display_add_component_entry<RigidBody2DComponent>("Rigid Body 2D");
			this->display_add_component_entry<BoxCollider2DComponent>("Box Collider 2D");
			this->display_add_component_entry<CircleCollider2DComponent>("Circle Collider 2D");

			ImGui::EndPopup();
		}

		draw_component<TransformComponent>("Transform", entity, [](auto &tc) {
			draw_vec3_control("Position", tc.translation);
			glm::vec3 rotation = glm::degrees(tc.rotation);
			draw_vec3_control("Rotation", rotation);
			tc.rotation = glm::radians(rotation);
			draw_vec3_control("Scale", tc.scale, 1.0f);
		});

		draw_component<CameraComponent>("Camera", entity, [](auto &camera) {
			ImGui::Checkbox("Primary", &camera.primary);
			ImGui::Checkbox("Fixed aspect ratio", &camera.fixed_aspect_ratio);

			const char *projection_type[] = { "Perspective", "Orthographic" };
			const char *current_projection = projection_type[camera.camera];
			if (ImGui::BeginCombo("Projection", current_projection)) {
				for (size_t idx = 0; idx < 2; idx++) {
					bool is_selected = current_projection == projection_type[idx];
					if (ImGui::Selectable(projection_type[idx], is_selected)) {
						current_projection = projection_type[idx];
						camera.camera.set_type(idx);
					}

					if (is_selected)
						ImGui::SetItemDefaultFocus();
				}
				ImGui::EndCombo();
			}

			switch ((SceneCamera::ProjectionType)camera.camera) {
			case SceneCamera::ProjectionType::Perspective: {
				float fov = camera.camera.get_perspective_fov();
				if (ImGui::DragFloat("FOV", &fov)) {
					camera.camera.set_perspective_fov(fov);
				}

				float near = camera.camera.get_perspective_near();
				if (ImGui::DragFloat("Near", &near)) {
					camera.camera.set_perspective_near(near);
				}

				float far = camera.camera.get_perspective_far();
				if (ImGui::DragFloat("Far", &far)) {
					camera.camera.set_perspective_far(far);
				}
			} break;

			case SceneCamera::ProjectionType::Orthographic: {
				float size = camera.camera.get_orthographic_size();
				if (ImGui::DragFloat("Size", &size)) {
					camera.camera.set_orthographic_size(size);
				}

				float near = camera.camera.get_orthographic_near();
				if (ImGui::DragFloat("Near", &near)) {
					camera.camera.set_orthographic_near(near);
				}

				float far = camera.camera.get_orthographic_far();
				if (ImGui::DragFloat("Far", &far)) {
					camera.camera.set_orthographic_far(far);
				}
			} break;
			}
		});

		// clang-format off
		draw_component<SpriteRendererComponent>("Sprite renderer", entity, [](auto &component) {
			ImGui::ColorEdit4("Color", glm::value_ptr(component.color));
			ImGui::Button("Texture", ImVec2(100.0f, 0.0f));

			if( ImGui::BeginDragDropTarget()) {
				if (const ImGuiPayload *payload = ImGui::AcceptDragDropPayload("CONTENT_BROWSER_ITEM")) {
					const char *path = reinterpret_cast<const char *>(payload->Data);
					component.texture = Texture2D::create(path);
				}

				ImGui::EndDragDropTarget();
			}
			ImGui::DragFloat("Tiling factor", &component.tiling_factor, 0.1f, 0.0f);
		});
		// clang-format on

		// clang-format off
		draw_component<CircleRendererComponent>("Circle renderer", entity, [](auto &component) {
			ImGui::ColorEdit4("Color", glm::value_ptr(component.color));
			ImGui::DragFloat("Thickness", &component.thickness, 0.025f, 0.0f, 1.0f);
			ImGui::DragFloat("Fade", &component.fade, 0.00025f, 0.0f, 1.0f);
		});
		// clang-format on

		draw_component<RigidBody2DComponent>("Rigid Body 2D", entity, [](auto &component) {
			const char *body_type[] = { "Static", "Dynamic", "Kinematic" };
			const char *current_body_type = body_type[(int)component.type];
			if (ImGui::BeginCombo("Body Type", current_body_type)) {
				for (size_t idx = 0; idx < 3; idx++) {
					bool is_selected = current_body_type == body_type[idx];
					if (ImGui::Selectable(body_type[idx], is_selected)) {
						current_body_type = body_type[idx];
						component.type = (RigidBody2DComponent::BodyType)idx;
					}

					if (is_selected)
						ImGui::SetItemDefaultFocus();
				}
				ImGui::EndCombo();
			}

			ImGui::Checkbox("Fixed rotation", &component.fixed_rotation);
		});

		draw_component<BoxCollider2DComponent>("Box Collider 2D", entity, [](auto &component) {
			ImGui::DragFloat2("Offset", glm::value_ptr(component.offset));
			ImGui::DragFloat2("Size", glm::value_ptr(component.size));

			ImGui::DragFloat("Density", &component.density, 0.01f, 0.0f, 1.0f);
			ImGui::DragFloat("Friction", &component.friction, 0.01f, 0.0f, 1.0f);
			ImGui::DragFloat("Restitution", &component.restitution, 0.01f, 0.0f, 1.0f);
			ImGui::DragFloat("Restitution Threshold", &component.restitution_threshold, 0.0f, 0.0f);
		});

		draw_component<CircleCollider2DComponent>("Circle Collider 2D", entity, [](auto &component) {
			ImGui::DragFloat2("Offset", glm::value_ptr(component.offset));
			ImGui::DragFloat("Radius", &component.radius, 0.01f);

			ImGui::DragFloat("Density", &component.density, 0.01f, 0.0f, 1.0f);
			ImGui::DragFloat("Friction", &component.friction, 0.01f, 0.0f, 1.0f);
			ImGui::DragFloat("Restitution", &component.restitution, 0.01f, 0.0f, 1.0f);
			ImGui::DragFloat("Restitution Threshold", &component.restitution_threshold, 0.0f, 0.0f);
		});
	}

	void SceneHierarchyPanel::draw_properties(Entity entity) {
		this->draw_components(entity);
	}

	void SceneHierarchyPanel::draw_entity_node(Entity entity) {
		auto &tag = entity.get<TagComponent>().tag;
		bool removed = false;

		ImGuiTreeNodeFlags flags = (((this->selection_context == entity) ? ImGuiTreeNodeFlags_Selected : 0));
		flags |= ImGuiTreeNodeFlags_OpenOnArrow;
		flags |= ImGuiTreeNodeFlags_SpanAvailWidth;
		bool open = ImGui::TreeNodeEx((void *)(uint64_t)entity, flags, "%s", tag.c_str());

		if (ImGui::BeginPopupContextItem()) {
			if (ImGui::MenuItem("Delete entity")) {
				removed = true;
			}

			ImGui::EndPopup();
		}

		if (ImGui::IsItemClicked()) {
			this->selection_context = entity;
		}

		if (open) {
			this->draw_properties(entity);

			ImGui::TreePop();
		}

		if (removed) {
			this->context->remove(entity);
			if (this->selection_context == entity)
				this->selection_context = {};
		}
	}

	ImGuiID SceneHierarchyPanel::set_dock_id(ImGuiID dock_id) {
		this->dock_id = dock_id;
		this->properties_dock_id = ImGui::DockBuilderSplitNode(this->dock_id, ImGuiDir_Down, 0.5f, nullptr, &this->dock_id);

		return this->dock_id;
	}

	void SceneHierarchyPanel::on_imgui_render(void) {
		if (ImGui::Begin("Scene Hierarchy")) {
			this->context->registry.each([&](auto entity_id) {
				Entity entity{ entity_id, this->context.get() };
				this->draw_entity_node(entity);
			});

			if (ImGui::IsMouseDown(0) && ImGui::IsWindowHovered()) {
				this->selection_context = {};
			}

			// Right click on a blank space:
			if (ImGui::BeginPopupContextWindow(0, 1, false)) {
				if (ImGui::MenuItem("Create empty entity")) {
					this->context->add("Empty entity");
				}

				ImGui::EndPopup();
			}

			ImGui::End();
		}

		ImGui::SetNextWindowDockID(this->properties_dock_id, ImGuiCond_FirstUseEver);
		if (ImGui::Begin("Properties")) {
			if (this->selection_context) {
				this->draw_properties(this->selection_context);
			}

			ImGui::End();
		}
	}

	template<typename T>
	void SceneHierarchyPanel::display_add_component_entry(const std::string &entry) {
		if( !this->selection_context.has<T>() ) {
			if( ImGui::MenuItem(entry.c_str()) ) {
				this->selection_context.add<T>();
				ImGui::CloseCurrentPopup();
			}
		}
	}
} // namespace Solace
