#include "Solace/spch.h"

#include "Solace/core/log.h"
#include "Solace/utils/platform_utils.h"

#include "nfd.h"

namespace Solace {
	namespace FileDialogs {
		std::string open_file(const char *filter) {
			char *out_path = nullptr;
			nfdresult_t result = NFD_OpenDialog(filter, nullptr, &out_path);

			if (result == NFD_OKAY) {
				std::string res(out_path);
				free(out_path);
				return res;
			} else if (result == NFD_CANCEL) {
				return std::string();
			} else {
				SL_CORE_ERROR("An error occured while selecting file: {}.", NFD_GetError());
				return std::string();
			}
		}

		std::string save_file(const char *filter) {
			char *out_path = nullptr;
			nfdresult_t result = NFD_SaveDialog(filter, nullptr, &out_path);

			if (result == NFD_OKAY) {
				std::string res(out_path);
				free(out_path);
				return res;
			} else if (result == NFD_CANCEL) {
				return std::string();
			} else {
				SL_CORE_ERROR("An error occured while selecting file: {}.", NFD_GetError());
				return std::string();
			}
		}
	} // namespace FileDialogs
} // namespace Solace
