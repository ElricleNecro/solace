#include "Solace/spch.h"

#include "Solace/core/application.h"
#include "Solace/utils/platform_utils.h"

#include <commdlg.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>

namespace Solace {
	namespace FileDialogs {
		std::string open_file(const char *filter) {
			SDL_SysWMinfo wmInfo;
			SDL_VERSION(&wmInfo.version);
			SDL_GetWindowWMInfo(Application::get_sdl_window(), &wmInfo);
			HWND hwnd = wmInfo.info.win.window;

			OPENFILENAMEA ofn;
			CHAR sz_file[260] = { 0 };
			ZeroMemory(&ofn, sizeof(OPENFILENAME));

			ofn.LStructSize = sizeof(OPENFILENAME);
			ofn.hwndOwner = hwnd;
			ofn.LpstrFile = sz_file;
			ofn.nMaxFile = sizeof(sz_file);
			ofn.LpstrFilter = filter;
			ofn.nFilterIndex = 1;
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR;

			if (GetOpenFileNameA(&ofn) == TRUE) {
				return ofn.LpstrFile;
			}

			return std::string();
		}

		std::string save_file(const char *filter) {
			SDL_SysWMinfo wmInfo;
			SDL_VERSION(&wmInfo.version);
			SDL_GetWindowWMInfo(Application::get_sdl_window(), &wmInfo);
			HWND hwnd = wmInfo.info.win.window;

			OPENFILENAMEA ofn;
			CHAR sz_file[260] = { 0 };
			ZeroMemory(&ofn, sizeof(OPENFILENAME));

			ofn.LStructSize = sizeof(OPENFILENAME);
			ofn.hwndOwner = hwnd;
			ofn.LpstrFile = sz_file;
			ofn.nMaxFile = sizeof(sz_file);
			ofn.LpstrFilter = filter;
			ofn.nFilterIndex = 1;
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR;

			if (GetSaveFileNameA(&ofn) == TRUE) {
				return ofn.LpstrFile;
			}

			return std::string();
		}
	} // namespace FileDialogs
} // namespace Solace
