#include "spch.h"

#include <glm/gtc/matrix_transform.hpp>

#include "Solace/renderer/orthographic_camera.h"
#include "Solace/spch.h"

namespace Solace {
	OrthographicCamera::OrthographicCamera(float left, float right, float bottom, float top)
		: projection_matrix(glm::ortho(left, right, bottom, top, -1.0f, 1.0f)), view_matrix(1.0f) {
		SL_PROFILE_FUNCTION();

		this->view_proj_matrix = this->projection_matrix * this->view_matrix;
	}

	void OrthographicCamera::recalculate_view_matrix(void) {
		SL_PROFILE_FUNCTION();

		glm::mat4 transform = glm::translate(glm::mat4(1.0f), this->position) * glm::rotate(glm::mat4(1.0f), glm::radians(this->rotation), glm::vec3(0, 0, 1));

		this->view_matrix = glm::inverse(transform);
		this->view_proj_matrix = this->projection_matrix * this->view_matrix;
	}

	void OrthographicCamera::set_projection(float left, float right, float bottom, float top) {
		SL_PROFILE_FUNCTION();

		this->projection_matrix = glm::ortho(left, right, bottom, top, -1.0f, 1.0f);
		this->view_proj_matrix = this->projection_matrix * this->view_matrix;
	}
} // namespace Solace
