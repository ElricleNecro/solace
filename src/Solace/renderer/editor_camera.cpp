#include "Solace/renderer/editor_camera.h"
#include "Solace/Solace.h"
#include "Solace/spch.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

namespace Solace {

	EditorCamera::EditorCamera(float fov, float aspect_ratio, float near_clip, float far_clip)
		: Camera(glm::perspective(glm::radians(fov), aspect_ratio, near_clip, far_clip)), fov(fov), aspect_ratio(aspect_ratio), near_clip(near_clip), far_clip(far_clip) {
		this->update_view();
	}

	void EditorCamera::update_projection() {
		this->aspect_ratio = this->viewport_width / this->viewport_height;
		this->projection = glm::perspective(glm::radians(this->fov), this->aspect_ratio, this->near_clip, this->far_clip);
	}

	void EditorCamera::update_view() {
		// yaw = this->pitch = 0.0f; // Lock the camera's rotation
		this->position = this->calculate_position();

		glm::quat orientation = get_orientation();

		this->view_matrix = glm::translate(glm::mat4(1.0f), this->position) * glm::toMat4(orientation);
		this->view_matrix = glm::inverse(this->view_matrix);
	}

	std::pair<float, float> EditorCamera::pan_speed() const {
		float x = std::min(this->viewport_width / 1000.0f, 2.4f); // max = 2.4f
		float xFactor = 0.0366f * (x * x) - 0.1778f * x + 0.3021f;

		float y = std::min(this->viewport_height / 1000.0f, 2.4f); // max = 2.4f
		float yFactor = 0.0366f * (y * y) - 0.1778f * y + 0.3021f;

		return { xFactor, yFactor };
	}

	float EditorCamera::rotation_speed() const {
		return 0.8f;
	}

	float EditorCamera::zoom_speed() const {
		float distance = this->distance * 0.2f;
		distance = std::max(distance, 0.0f);

		float speed = distance * distance;
		speed = std::min(speed, 100.0f); // max speed = 100

		return speed;
	}

	void EditorCamera::on_update([[maybe_unused]] float ts) {
		Events event = Application::get_events_data();
		if (!event.used() && event[SDL_SCANCODE_LALT]) {
			SL_CORE_INFO("LALT is pressed");
			auto mouse_position = Application::get_events_data().mouse_position();
			const glm::vec2 &mouse{ std::get<0>(mouse_position), std::get<1>(mouse_position) };
			//glm::vec2 delta = (mouse - this->initial_mouse_position) * 0.003f;
			glm::vec2 delta = { event.dx(), event.dy() };
			delta *= 0.03f;
			SL_CORE_INFO("Raw mouse position: {}, {}", std::get<0>(mouse_position), std::get<1>(mouse_position));
			SL_CORE_INFO("Left mouse button pressed: {}, {} (mouse pos: {}, {})", delta.x, delta.y, mouse.x, mouse.y);

			this->initial_mouse_position = mouse;

			if (event.mouse()[SDL_BUTTON_MIDDLE]) {
				this->mouse_pan(delta);
				event.set_used();
			} else if (event.mouse()[SDL_BUTTON_LEFT]) {
				this->mouse_rotate(delta);
				event.set_used();
			} else if (event.mouse()[SDL_BUTTON_RIGHT]) {
				this->mouse_zoom(delta.y);
				event.set_used();
			}
		}

		this->update_view();
	}

	bool EditorCamera::on_event(Events &e) {
		float delta = e.wheel_y() * 0.1f;

		this->mouse_zoom(delta);
		this->update_view();

		return false;
	}

	void EditorCamera::mouse_pan(const glm::vec2 &delta) {
		auto [x_speed, y_speed] = this->pan_speed();

		this->focal_point += -this->get_right_direction() * delta.x * x_speed * this->distance;
		this->focal_point += this->get_up_direction() * delta.y * y_speed * this->distance;
	}

	void EditorCamera::mouse_rotate(const glm::vec2 &delta) {
		float yawSign = get_up_direction().y < 0 ? -1.0f : 1.0f;
		yaw += yawSign * delta.x * this->rotation_speed();
		this->pitch += delta.y * this->rotation_speed();
	}

	void EditorCamera::mouse_zoom(float delta) {
		this->distance -= delta * this->zoom_speed();
		if (this->distance < 1.0f) {
			this->focal_point += get_forward_direction();
			this->distance = 1.0f;
		}
	}

	glm::vec3 EditorCamera::get_up_direction() const {
		return glm::rotate(this->get_orientation(), glm::vec3(0.0f, 1.0f, 0.0f));
	}

	glm::vec3 EditorCamera::get_right_direction() const {
		return glm::rotate(this->get_orientation(), glm::vec3(1.0f, 0.0f, 0.0f));
	}

	glm::vec3 EditorCamera::get_forward_direction() const {
		return glm::rotate(this->get_orientation(), glm::vec3(0.0f, 0.0f, -1.0f));
	}

	glm::vec3 EditorCamera::calculate_position() const {
		return this->focal_point - this->get_forward_direction() * this->distance;
	}

	glm::quat EditorCamera::get_orientation() const {
		return glm::quat(glm::vec3(-this->pitch, -this->yaw, 0.0f));
	}

} // namespace Solace
