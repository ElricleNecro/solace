#include "Solace/spch.h"

#include "Solace/renderer/rendererapi.h"
#include "Solace/renderer/texture.h"

#include "Solace/renderer/opengl/opengl_texture.h"

namespace Solace {
	Ref<Texture2D> Texture2D::create(const std::string &path) {
		switch (RendererAPI::get_api()) {
		case RendererAPI::API::None:
			SOLACE_CORE_ASSERT(false, "RendererAPI::API::None is not supported currently.");
			return nullptr;
		case RendererAPI::API::OpenGL:
			return make_ref<OpenGLTexture2D>(path);
		}

		SOLACE_CORE_ASSERT(false, "Unknown renderer API.");
		return nullptr;
	}

	Ref<Texture2D> Texture2D::create(const uint32_t width, const uint32_t height) {
		switch (RendererAPI::get_api()) {
		case RendererAPI::API::None:
			SOLACE_CORE_ASSERT(false, "RendererAPI::API::None is not supported currently.");
			return nullptr;
		case RendererAPI::API::OpenGL:
			return make_ref<OpenGLTexture2D>(width, height);
		}

		SOLACE_CORE_ASSERT(false, "Unknown renderer API.");
		return nullptr;
	}
} // namespace Solace
