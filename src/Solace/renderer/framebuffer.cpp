#include "Solace/renderer/framebuffer.h"
#include "Solace/spch.h"

#include "Solace/renderer/opengl/opengl_framebuffer.h"
#include "Solace/renderer/renderer.h"
#include "Solace/renderer/rendererapi.h"

namespace Solace {
	Ref<FrameBuffer> FrameBuffer::create(const FrameBufferSpecification &spec) {
		switch (Renderer::get_api()) {
		case RendererAPI::API::None:
			SOLACE_CORE_ASSERT(false, "RendererAPI::None is not supported.");
			return nullptr;
		case RendererAPI::API::OpenGL:
			return make_ref<OpenGLFrameBuffer>(spec);
		}

		SOLACE_CORE_ASSERT(false, "Unknown API!");
		return nullptr;
	}
} // namespace Solace
