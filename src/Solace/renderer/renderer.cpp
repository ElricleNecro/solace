#include "Solace/renderer/renderer.h"
#include "Solace/core/core.h"
#include "Solace/renderer/opengl/opengl_context.h"
#include "Solace/renderer/opengl/opengl_shader.h"
#include "Solace/renderer/rendercommand.h"
#include "Solace/renderer/renderer2D.h"
#include "Solace/spch.h"

namespace Solace {
	glm::mat4 Renderer::view_proj_matrix;
	Ref<Shader> Renderer::last_shader;

	Ref<Renderer> Renderer::create(void) {
		switch (RendererAPI::get_api()) {
		case RendererAPI::API::None:
			SOLACE_CORE_ASSERT(false, "RendererAPI::API::None is not supported currently.");
			return nullptr;
		case RendererAPI::API::OpenGL:
			return make_ref<OpenGLRenderer>();
		}

		SOLACE_CORE_ASSERT(false, "Unknown renderer API.");
		return nullptr;
	}

	void Renderer::init(void) {
		SL_PROFILE_FUNCTION();

		RenderCommand::init();
		Renderer2D::init();
	}

	void Renderer::shutdown(void) {
		Renderer2D::shutdown();
	}

	void Renderer::begin_scene(const OrthographicCamera &camera) {
		Renderer::view_proj_matrix = camera.get_vp_matrix();
	}

	void Renderer::end_scene(void) {
		if (Renderer::last_shader != nullptr) {
			Renderer::last_shader->unbind();
			Renderer::last_shader.reset();
		}
	}

	void Renderer::submit(const Ref<Shader> &shader, const Ref<VertexArray> &vao, const glm::mat4 &transform) {
		if (Renderer::last_shader != nullptr)
			Renderer::last_shader->unbind();

		Renderer::last_shader = shader;
		Renderer::last_shader->bind();
		Renderer::last_shader->set_mat("view_proj", Renderer::view_proj_matrix);
		Renderer::last_shader->set_mat("transform", transform);
		Renderer::submit(vao);
	}

	void Renderer::submit(const Ref<VertexArray> &vao) {
		vao->bind();
		RenderCommand::draw_indexed(vao);
		vao->unbind();
	}

	void Renderer::viewport(int width, int height) {
		RenderCommand::viewport(0, 0, width, height);
	}
} // namespace Solace
