#include "Solace/renderer/shader.h"
#include "Solace/core/core.h"
#include "Solace/renderer/opengl/opengl_shader.h"
#include "Solace/renderer/rendererapi.h"

namespace Solace {
	Ref<Shader> Shader::create(const std::string &filepath) {
		switch (RendererAPI::get_api()) {
		case RendererAPI::API::None:
			SOLACE_CORE_ASSERT(false, "RendererAPI::API::None is not supported currently.");
			return nullptr;
		case RendererAPI::API::OpenGL:
			return std::make_shared<OpenGLShader>(filepath);
		}

		SOLACE_CORE_ASSERT(false, "Unknown renderer API.");
		return nullptr;
	}

	Ref<Shader> Shader::create(const std::string &name, const std::string &vertex, const std::string &fragment) {
		switch (RendererAPI::get_api()) {
		case RendererAPI::API::None:
			SOLACE_CORE_ASSERT(false, "RendererAPI::API::None is not supported currently.");
			return nullptr;
		case RendererAPI::API::OpenGL:
			return std::make_shared<OpenGLShader>(name, vertex, fragment);
		}

		SOLACE_CORE_ASSERT(false, "Unknown renderer API.");
		return nullptr;
	}

	void ShaderLibrary::add(const std::string &name, const Ref<Shader> &shader) {
		SOLACE_CORE_ASSERT(!this->exist(name), "Shader already exists");
		this->shaders[name] = shader;
	}

	void ShaderLibrary::add(const Ref<Shader> &shader) {
		this->add(shader->get_name(), shader);
	}

	Ref<Shader> ShaderLibrary::load(const std::string &file_path) {
		auto shader = Shader::create(file_path);

		this->add(shader);

		return shader;
	}

	Ref<Shader> ShaderLibrary::load(const std::string &name, const std::string &file_path) {
		auto shader = Shader::create(file_path);

		this->add(name, shader);

		return shader;
	}

	bool ShaderLibrary::exist(const std::string &name) const {
		return this->shaders.find(name) != this->shaders.end();
	}

	Ref<Shader> ShaderLibrary::get(const std::string &name) {
		SOLACE_CORE_ASSERT(this->exist(name), "Shader does not exists");
		return this->shaders[name];
	}
} // namespace Solace
