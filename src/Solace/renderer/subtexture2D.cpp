#include "Solace/renderer/subtexture2D.h"
#include "Solace/spch.h"

namespace Solace {
	SubTexture2D::SubTexture2D(const Ref<Texture2D> &texture, const glm::vec2 &min, const glm::vec2 &max) : texture(texture) {
		this->tex_coords[0] = { min.x, min.y };
		this->tex_coords[1] = { max.x, min.y };
		this->tex_coords[2] = { max.x, max.y };
		this->tex_coords[3] = { min.x, max.y };
	}

	Ref<SubTexture2D> SubTexture2D::create_from_coordinates(const Ref<Texture2D> &texture, const glm::vec2 &coords, const glm::vec2 &sprite_size, const glm::vec2 &size_offet) {
		const glm::vec2 min = { coords.x * sprite_size.x / texture->get_width(), coords.y * sprite_size.y / texture->get_height() };
		const glm::vec2 max = { (coords.x + size_offet.x) * sprite_size.x / texture->get_width(), (coords.y + size_offet.y) * sprite_size.y / texture->get_height() };

		return Solace::make_ref<SubTexture2D>(texture, min, max);
	}
} // namespace Solace
