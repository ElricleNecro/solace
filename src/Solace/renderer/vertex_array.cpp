#include "Solace/renderer/vertex_array.h"
#include "Solace/renderer/opengl/opengl_vertex_array.h"
#include "Solace/renderer/rendererapi.h"

namespace Solace {
	Ref<VertexArray> VertexArray::create(void) {
		switch (RendererAPI::get_api()) {
		case RendererAPI::API::None:
			SOLACE_CORE_ASSERT(false, "RendererAPI::API::None is not supported currently.");
			return nullptr;
		case RendererAPI::API::OpenGL:
			return make_ref<OpenGLVertexArray>();
		}

		SOLACE_CORE_ASSERT(false, "Unknown renderer API.");
		return nullptr;
	}
} // namespace Solace
