#include "Solace/renderer/rendercommand.h"
#include "Solace/renderer/opengl/opengl_rendererapi.h"

namespace Solace {
	const Ref<RendererAPI> RenderCommand::renderer_api = make_ref<OpenGLRendererAPI>();
} // namespace Solace
