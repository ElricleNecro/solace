#include "Solace/renderer/buffer.h"
#include "Solace/core/core.h"
#include "Solace/renderer/rendererapi.h"
#include "Solace/spch.h"

#include "Solace/renderer/opengl/opengl_buffer.h"

namespace Solace {
	// static uint32_t ShaderDataTypeSize(ShaderDataType type);

	static uint32_t ShaderDataTypeSize(ShaderDataType type) {
		switch (type) {
		case ShaderDataType::Float:
			return sizeof(float);
		case ShaderDataType::Float2:
			return sizeof(float) * 2;
		case ShaderDataType::Float3:
			return sizeof(float) * 3;
		case ShaderDataType::Float4:
			return sizeof(float) * 4;

		case ShaderDataType::Mat3:
			return sizeof(float) * 3 * 3;
		case ShaderDataType::Mat4:
			return sizeof(float) * 4 * 4;

		case ShaderDataType::Int:
			return sizeof(int);
		case ShaderDataType::Int2:
			return sizeof(int) * 2;
		case ShaderDataType::Int3:
			return sizeof(int) * 3;
		case ShaderDataType::Int4:
			return sizeof(int) * 4;

		case ShaderDataType::Bool:
			return sizeof(bool);

		default:
			SOLACE_CORE_ASSERT(false, "Unknown ShaderDataType!");
			return 0;
		}
	}

	BufferElement::BufferElement(ShaderDataType type, const std::string &name, bool normalised)
		: name(name), type(type), size(ShaderDataTypeSize(type)), offset(0), normalised(normalised) {
	}

	uint32_t BufferElement::get_component_count(void) const {
		switch (this->type) {
		case ShaderDataType::Float:
			return 1;
		case ShaderDataType::Float2:
			return 2;
		case ShaderDataType::Float3:
			return 3;
		case ShaderDataType::Float4:
			return 4;
		case ShaderDataType::Int:
			return 1;
		case ShaderDataType::Int2:
			return 2;
		case ShaderDataType::Int3:
			return 3;
		case ShaderDataType::Int4:
			return 4;
		case ShaderDataType::Mat3:
			return 3;
		case ShaderDataType::Mat4:
			return 4;
		case ShaderDataType::Bool:
			return 1;
		case ShaderDataType::None:
			SOLACE_CORE_ASSERT(false, "Unknown shader type!");
		}

		return 0;
	}

	BufferLayout::BufferLayout(const std::initializer_list<BufferElement> &elements) : elems(elements) {
		this->calculate_offset_and_stride();
	}

	void BufferLayout::calculate_offset_and_stride(void) {
		this->stride = 0;

		uint32_t offset = 0;
		for (auto &element : this->elems) {
			element.offset = offset;
			offset += element.size;
			this->stride += element.size;
		}
	}

	Ref<VertexBuffer> VertexBuffer::create(uint32_t size) {
		switch (RendererAPI::get_api()) {
		case RendererAPI::API::None:
			SOLACE_CORE_ASSERT(false, "RendererAPI::API::None is not supported currently.");
			return nullptr;
		case RendererAPI::API::OpenGL:
			return make_ref<OpenGLVertexBuffer>(size);
		}

		SOLACE_CORE_ASSERT(false, "Unknown renderer API.");
		return nullptr;
	}

	Ref<VertexBuffer> VertexBuffer::create(const float *vertices, uint32_t size) {
		switch (RendererAPI::get_api()) {
		case RendererAPI::API::None:
			SOLACE_CORE_ASSERT(false, "RendererAPI::API::None is not supported currently.");
			return nullptr;
		case RendererAPI::API::OpenGL:
			return std::make_shared<OpenGLVertexBuffer>(vertices, size);
		}

		SOLACE_CORE_ASSERT(false, "Unknown renderer API.");
		return nullptr;
	}

	Ref<IndexBuffer> IndexBuffer::create(const uint32_t *vertices, const uint32_t count) {
		switch (RendererAPI::get_api()) {
		case RendererAPI::API::None:
			SOLACE_CORE_ASSERT(false, "RendererAPI::API::None is not supported currently.");
			return nullptr;
		case RendererAPI::API::OpenGL:
			return std::make_shared<OpenGLIndexBuffer>(vertices, count);
		}

		SOLACE_CORE_ASSERT(false, "Unknown renderer API.");
		return nullptr;
	}
} // namespace Solace
