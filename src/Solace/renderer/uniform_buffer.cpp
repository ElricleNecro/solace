#include "Solace/spch.h"

#include "Solace/renderer/renderer.h"
#include "Solace/renderer/rendererapi.h"
#include "Solace/renderer/uniform_buffer.h"

#include "Solace/renderer/opengl/opengl_uniform_buffer.h"

namespace Solace {
	Ref<UniformBuffer> UniformBuffer::create(size_t size, uint32_t binding) {
		switch (Renderer::get_api()) {
		case RendererAPI::API::None:
			SOLACE_CORE_ASSERT(false, "RendererAPI::None is currrently not supported!");
			return nullptr;

		case RendererAPI::API::OpenGL:
			return make_ref<OpenGLUniformBuffer>(size, binding);
		}

		SOLACE_CORE_ASSERT(false, "Unknown renderer API.");
		return nullptr;
	}
} // namespace Solace
