#include <glm/gtc/matrix_transform.hpp>

#include "Solace/renderer/rendercommand.h"
#include "Solace/renderer/shader.h"
#include "Solace/renderer/uniform_buffer.h"
#include "Solace/renderer/vertex_array.h"
#include "Solace/spch.h"

#include "Solace/renderer/renderer2D.h"

namespace Solace {
	struct QuadVertex {
		glm::vec3 position;
		glm::vec4 color;
		glm::vec2 tex_coord;

		float tex_index;
		float tiling_factor;

		// Editor-only:
		int entity_id = -1;
	};

	struct CircleVertex {
		glm::vec3 world_position;
		glm::vec3 local_position;
		glm::vec4 color;
		float thickness;
		float fade;

		// Editor-only:
		int entity_id = -1;
	};

	struct LineVertex {
		glm::vec3 position;
		glm::vec4 color;

		// Editor-only:
		int entity_id = -1;
	};

	struct Renderer2DData {
		static const uint32_t max_quads = 20000;
		static const uint32_t max_vertices = Renderer2DData::max_quads * 4;
		static const uint32_t max_indices = Renderer2DData::max_quads * 6;
		static const uint32_t max_texture_slots = 32;

		Ref<VertexArray> quad_vao;
		Ref<VertexBuffer> quad_vbo;
		Ref<Shader> quad_shader;
		Ref<Texture2D> white_texture;

		Ref<VertexArray> circle_vao;
		Ref<VertexBuffer> circle_vbo;
		Ref<Shader> circle_shader;

		Ref<VertexArray> line_vao;
		Ref<VertexBuffer> line_vbo;
		Ref<Shader> line_shader;

		uint32_t quad_index_count = 0;

		QuadVertex *quad_vertex_buffer_base = nullptr;
		QuadVertex *quad_vertex_buffer_ptr = nullptr;

		uint32_t circle_index_count = 0;

		CircleVertex *circle_vertex_buffer_base = nullptr;
		CircleVertex *circle_vertex_buffer_ptr = nullptr;

		float line_width = 2.0f;
		uint32_t line_index_count = 0;

		LineVertex *line_vertex_buffer_base = nullptr;
		LineVertex *line_vertex_buffer_ptr = nullptr;

		std::array<Ref<Texture2D>, Renderer2DData::max_texture_slots> tex_slots;
		uint32_t texture_slot_index = 1; // 0 = white texture.

		glm::vec4 quad_vertex_position[4];

#ifdef SL_PROFILE
		Renderer2D::Statistics stats;
#endif

		struct CameraData {
			glm::mat4 view_proj;
		};
		CameraData camera_buffer;
		Ref<UniformBuffer> camera_uniform_buffer;
	};

	static Renderer2DData data;

	void Renderer2D::init(void) {
		SL_PROFILE_FUNCTION();

		data.quad_vao = Solace::VertexArray::create();

		data.quad_vbo = VertexBuffer::create(Renderer2DData::max_vertices * sizeof(QuadVertex));
		data.quad_vbo->set_layout({
			{ ShaderDataType::Float3, "position" },
			{ ShaderDataType::Float4, "color" },
			{ ShaderDataType::Float2, "tex_coord" },
			{ ShaderDataType::Float, "tex_index" },
			{ ShaderDataType::Float, "tiling_factor" },
			{ ShaderDataType::Int, "entity_id" },
		});
		data.quad_vao->add_vertex_buffer(data.quad_vbo);

		data.quad_vertex_buffer_base = new QuadVertex[Renderer2DData::max_vertices];

		uint32_t *quad_indices = new uint32_t[Renderer2DData::max_indices];
		uint32_t offset = 0;
		for (uint32_t idx = 0; idx < Renderer2DData::max_indices; idx += 6) {
			quad_indices[idx + 0] = offset + 0;
			quad_indices[idx + 1] = offset + 1;
			quad_indices[idx + 2] = offset + 2;

			quad_indices[idx + 3] = offset + 2;
			quad_indices[idx + 4] = offset + 3;
			quad_indices[idx + 5] = offset + 0;

			offset += 4;
		}

		Ref<IndexBuffer> quad_vio = IndexBuffer::create(quad_indices, Renderer2DData::max_indices);
		data.quad_vao->set_index_buffer(quad_vio);

		delete[] quad_indices;

		data.circle_vao = Solace::VertexArray::create();

		data.circle_vbo = VertexBuffer::create(Renderer2DData::max_vertices * sizeof(CircleVertex));
		data.circle_vbo->set_layout({
			{ ShaderDataType::Float3, "world_position" },
			{ ShaderDataType::Float3, "local_position" },
			{ ShaderDataType::Float4, "color" },
			{ ShaderDataType::Float, "thickness" },
			{ ShaderDataType::Float, "fade" },
			{ ShaderDataType::Int, "entity_id" },
		});
		data.circle_vao->add_vertex_buffer(data.circle_vbo);
		data.circle_vao->set_index_buffer(quad_vio); // Use quad index buffer

		data.circle_vertex_buffer_base = new CircleVertex[Renderer2DData::max_vertices];

		data.line_vao = Solace::VertexArray::create();

		data.line_vbo = VertexBuffer::create(Renderer2DData::max_vertices * sizeof(LineVertex));
		data.line_vbo->set_layout({
			{ ShaderDataType::Float3, "position" },
			{ ShaderDataType::Float4, "color" },
			{ ShaderDataType::Int, "entity_id" },
		});
		data.line_vao->add_vertex_buffer(data.line_vbo);

		data.line_vertex_buffer_base = new LineVertex[Renderer2DData::max_vertices];

		data.white_texture = Texture2D::create(1, 1);
		static uint32_t tex_data = 0xffffffff;
		data.white_texture->set_data(&tex_data, sizeof(uint32_t));
		data.tex_slots[0] = data.white_texture;

		data.quad_shader = Shader::create("assets/shaders/renderer2D_quad.glsl");
		data.quad_shader->bind();
		int32_t samplers[data.max_texture_slots] = { 0 };
		for (uint32_t idx = 0; idx < data.max_texture_slots; idx++)
			samplers[idx] = idx;
		data.quad_shader->set_int("u_textures", samplers, data.max_texture_slots);
		data.quad_shader->unbind();

		data.circle_shader = Shader::create("assets/shaders/renderer2D_circle.glsl");
		data.line_shader = Shader::create("assets/shaders/renderer2D_line.glsl");

		data.quad_vertex_position[0] = { -0.5f, -0.5f, 0.0f, 1.0f };
		data.quad_vertex_position[1] = { 0.5f, -0.5f, 0.0f, 1.0f };
		data.quad_vertex_position[2] = { 0.5f, 0.5f, 0.0f, 1.0f };
		data.quad_vertex_position[3] = { -0.5f, 0.5f, 0.0f, 1.0f };

		data.camera_uniform_buffer = UniformBuffer::create(sizeof(Renderer2DData::CameraData), 0);
	}

	void Renderer2D::shutdown(void) {
		SL_PROFILE_FUNCTION();

		delete[] data.quad_vertex_buffer_base;
		delete[] data.circle_vertex_buffer_base;
		delete[] data.line_vertex_buffer_base;
	}

	void Renderer2D::begin_scene(const Camera &camera, const glm::mat4 &transform) {
		SL_PROFILE_FUNCTION();

		data.camera_buffer.view_proj = camera.get_projection() * glm::inverse(transform);
		data.camera_uniform_buffer->set_data(&data.camera_buffer, sizeof(Renderer2DData::CameraData));

		Renderer2D::start_batch();
	}

	void Renderer2D::begin_scene(const EditorCamera &camera) {
		SL_PROFILE_FUNCTION();

		data.camera_buffer.view_proj = camera.get_view_projection();
		data.camera_uniform_buffer->set_data(&data.camera_buffer, sizeof(Renderer2DData::CameraData));

		Renderer2D::start_batch();
	}

	void Renderer2D::begin_scene(const OrthographicCamera &camera) {
		SL_PROFILE_FUNCTION();

		data.camera_buffer.view_proj = camera.get_vp_matrix();
		data.camera_uniform_buffer->set_data(&data.camera_buffer, sizeof(Renderer2DData::CameraData));

		Renderer2D::start_batch();
	}

	void Renderer2D::start_batch(void) {
		data.quad_index_count = 0;
		data.quad_vertex_buffer_ptr = data.quad_vertex_buffer_base;

		data.circle_index_count = 0;
		data.circle_vertex_buffer_ptr = data.circle_vertex_buffer_base;

		data.line_index_count = 0;
		data.line_vertex_buffer_ptr = data.line_vertex_buffer_base;

		data.texture_slot_index = 1;
	}

	void Renderer2D::end_scene(void) {
		SL_PROFILE_FUNCTION();

		Renderer2D::flush();
	}

	void Renderer2D::flush(void) {
		SL_PROFILE_FUNCTION();

		if (data.quad_index_count) {
			data.quad_vbo->set_data(data.quad_vertex_buffer_base, (uint32_t)((uint8_t *)data.quad_vertex_buffer_ptr - (uint8_t *)data.quad_vertex_buffer_base));

			for (uint32_t idx = 0; idx < data.texture_slot_index; idx++) {
				data.tex_slots[idx]->bind(idx);
			}

			data.quad_vao->bind();
			data.quad_shader->bind();
			RenderCommand::draw_indexed(data.quad_vao, data.quad_index_count);
			data.quad_shader->unbind();
			data.quad_vao->unbind();

			for (uint32_t idx = 0; idx < data.texture_slot_index; idx++) {
				data.tex_slots[idx]->unbind();
			}

#ifdef SL_PROFILE
			data.stats.draw_calls++;
#endif
		}

		if (data.circle_index_count) {
			data.circle_vbo->set_data(data.circle_vertex_buffer_base, (uint32_t)((uint8_t *)data.circle_vertex_buffer_ptr - (uint8_t *)data.circle_vertex_buffer_base));

			data.circle_vao->bind();
			data.circle_shader->bind();
			RenderCommand::draw_indexed(data.circle_vao, data.circle_index_count);
			data.circle_shader->unbind();
			data.circle_vao->unbind();

#ifdef SL_PROFILE
			data.stats.draw_calls++;
#endif
		}

		if (data.line_index_count) {
			data.line_vbo->set_data(data.line_vertex_buffer_base, (uint32_t)((uint8_t *)data.line_vertex_buffer_ptr - (uint8_t *)data.line_vertex_buffer_base));

			data.line_vao->bind();
			data.line_shader->bind();
			RenderCommand::set_line_width(data.line_width);
			RenderCommand::draw_lines(data.line_vao, data.line_index_count);
			data.line_shader->unbind();
			data.line_vao->unbind();

#ifdef SL_PROFILE
			data.stats.draw_calls++;
#endif
		}
	}

	void Renderer2D::draw_quad(const glm::vec3 &position, const glm::vec2 &size, float rotation, const glm::vec4 &color, float tiling_factor) {
		Renderer2D::draw_quad(position, size, rotation, data.white_texture, color, tiling_factor);
	}

	void Renderer2D::draw_quad(const glm::vec3 &position,
				   const glm::vec2 &size,
				   float rotation,
				   const Ref<Texture2D> &texture,
				   const glm::vec2 *coords,
				   const glm::vec4 &color,
				   float tiling_factor) {
		SL_PROFILE_FUNCTION();

		glm::mat4 transform = glm::translate(glm::mat4(1.f), position) * glm::rotate(glm::mat4(1.0f), rotation, { 0.0f, 0.0f, 1.0f }) *
				      glm::scale(glm::mat4(1.f), { size.x, size.y, 1.f });

		Renderer2D::draw_quad(transform, texture, coords, color, tiling_factor);
	}

	void Renderer2D::draw_quad(const glm::mat4 &transform, const glm::vec4 &color, float tiling_factor) {
		Renderer2D::draw_quad(transform, data.white_texture, color, tiling_factor);
	}

	void Renderer2D::draw_quad(const glm::mat4 &transform, const Ref<Texture2D> &texture, const glm::vec2 *coords, const glm::vec4 &color, float tiling_factor, int entity_id) {
		SL_PROFILE_FUNCTION();

		if (data.quad_index_count >= Renderer2DData::max_indices || data.texture_slot_index >= Renderer2DData::max_texture_slots) {
			Renderer2D::flush();
			Renderer2D::start_batch();
		}

		//constexpr glm::vec4 color = { 1.f, 1.f, 1.f, 1.f };
		float tex_index = -1.f;

		for (uint32_t idx = 1; idx < data.texture_slot_index; idx++) {
			if (*data.tex_slots[idx].get() == *texture.get()) {
				tex_index = (float)idx;
				break;
			}
		}

		if (tex_index <= 0.f) {
			tex_index = (float)data.texture_slot_index;
			data.tex_slots[data.texture_slot_index] = texture;
			data.texture_slot_index++;
		}

		data.quad_vertex_buffer_ptr->position = transform * data.quad_vertex_position[0];
		data.quad_vertex_buffer_ptr->color = color;
		data.quad_vertex_buffer_ptr->tex_coord = coords[0];
		data.quad_vertex_buffer_ptr->tex_index = tex_index;
		data.quad_vertex_buffer_ptr->tiling_factor = tiling_factor;
		data.quad_vertex_buffer_ptr->entity_id = entity_id;
		data.quad_vertex_buffer_ptr++;

		data.quad_vertex_buffer_ptr->position = transform * data.quad_vertex_position[1];
		data.quad_vertex_buffer_ptr->color = color;
		data.quad_vertex_buffer_ptr->tex_coord = coords[1];
		data.quad_vertex_buffer_ptr->tex_index = tex_index;
		data.quad_vertex_buffer_ptr->tiling_factor = tiling_factor;
		data.quad_vertex_buffer_ptr->entity_id = entity_id;
		data.quad_vertex_buffer_ptr++;

		data.quad_vertex_buffer_ptr->position = transform * data.quad_vertex_position[2];
		data.quad_vertex_buffer_ptr->color = color;
		data.quad_vertex_buffer_ptr->tex_coord = coords[2];
		data.quad_vertex_buffer_ptr->tex_index = tex_index;
		data.quad_vertex_buffer_ptr->tiling_factor = tiling_factor;
		data.quad_vertex_buffer_ptr->entity_id = entity_id;
		data.quad_vertex_buffer_ptr++;

		data.quad_vertex_buffer_ptr->position = transform * data.quad_vertex_position[3];
		data.quad_vertex_buffer_ptr->color = color;
		data.quad_vertex_buffer_ptr->tex_coord = coords[3];
		data.quad_vertex_buffer_ptr->tex_index = tex_index;
		data.quad_vertex_buffer_ptr->tiling_factor = tiling_factor;
		data.quad_vertex_buffer_ptr->entity_id = entity_id;
		data.quad_vertex_buffer_ptr++;

		data.quad_index_count += 6;

#ifdef SL_PROFILE
		data.stats.quad_count++;
#endif
	}

	void Renderer2D::draw_circle(const glm::mat4 &transform, const glm::vec4 &color, float thickness, float fade, int entity_id) {
		SL_PROFILE_FUNCTION();

		if (data.quad_index_count >= Renderer2DData::max_indices || data.texture_slot_index >= Renderer2DData::max_texture_slots) {
			Renderer2D::flush();
			Renderer2D::start_batch();
		}

		for (size_t i = 0; i < 4; i++) {
			data.circle_vertex_buffer_ptr->world_position = transform * data.quad_vertex_position[i];
			data.circle_vertex_buffer_ptr->local_position = data.quad_vertex_position[i] * 2.0f;
			data.circle_vertex_buffer_ptr->color = color;
			data.circle_vertex_buffer_ptr->thickness = thickness;
			data.circle_vertex_buffer_ptr->fade = fade;
			data.circle_vertex_buffer_ptr->entity_id = entity_id;
			data.circle_vertex_buffer_ptr++;
		}

		data.circle_index_count += 6;

#ifdef SL_PROFILE
		data.stats.quad_count++;
#endif
	}

	void Renderer2D::draw_sprite(const glm::mat4 &transform, const SpriteRendererComponent &src, int entity_id) {
		static const glm::vec2 tex_coords[] = {
			{ 0.f, 0.f },
			{ 1.f, 0.f },
			{ 1.f, 1.f },
			{ 0.f, 1.f },
		};
		Renderer2D::draw_quad(transform, src.texture != nullptr ? src.texture : data.white_texture, tex_coords, src.color, src.tiling_factor, entity_id);
	}

	void Renderer2D::draw_line(const glm::vec3 &p0, const glm::vec3 &p1, const glm::vec4 &color, int entity_id) {
		SL_PROFILE_FUNCTION();

		if (data.line_index_count >= Renderer2DData::max_indices) {
			Renderer2D::flush();
			Renderer2D::start_batch();
		}

		data.line_vertex_buffer_ptr->position = p0;
		data.line_vertex_buffer_ptr->color = color;
		data.line_vertex_buffer_ptr->entity_id = entity_id;
		data.line_vertex_buffer_ptr++;

		data.line_vertex_buffer_ptr->position = p1;
		data.line_vertex_buffer_ptr->color = color;
		data.line_vertex_buffer_ptr->entity_id = entity_id;
		data.line_vertex_buffer_ptr++;

		data.line_index_count += 2;
	}

	void Renderer2D::draw_rect(const glm::vec3 &position, const glm::vec2 &size, const glm::vec4 &color, int entity_id) {
		glm::vec3 p0 = glm::vec3(position.x - size.x * 0.5f, position.y - size.y * 0.5f, position.z);
		glm::vec3 p1 = glm::vec3(position.x + size.x * 0.5f, position.y - size.y * 0.5f, position.z);
		glm::vec3 p2 = glm::vec3(position.x + size.x * 0.5f, position.y + size.y * 0.5f, position.z);
		glm::vec3 p3 = glm::vec3(position.x - size.x * 0.5f, position.y + size.y * 0.5f, position.z);

		Renderer2D::draw_line(p0, p1, color, entity_id);
		Renderer2D::draw_line(p1, p2, color, entity_id);
		Renderer2D::draw_line(p2, p3, color, entity_id);
		Renderer2D::draw_line(p3, p0, color, entity_id);
	}

	void Renderer2D::draw_rect(const glm::mat4 &transform, const glm::vec4 &color, int entity_id) {
		glm::vec3 line_vertices[4];

		for (size_t i = 0; i < 4; i++) {
			line_vertices[i] = transform * data.quad_vertex_position[i];
		}

		Renderer2D::draw_line(line_vertices[0], line_vertices[1], color, entity_id);
		Renderer2D::draw_line(line_vertices[1], line_vertices[2], color, entity_id);
		Renderer2D::draw_line(line_vertices[2], line_vertices[3], color, entity_id);
		Renderer2D::draw_line(line_vertices[3], line_vertices[0], color, entity_id);
	}

	float Renderer2D::get_line_width(void) {
		return data.line_width;
	}

	void Renderer2D::set_line_width(float width) {
		data.line_width = width;
	}

#ifdef SL_PROFILE
	Renderer2D::Statistics Renderer2D::get_stats(void) {
		return data.stats;
	}

	void Renderer2D::ResetStats(void) {
		data.stats.draw_calls = 0;
		data.stats.quad_count = 0;
	}
#endif
} // namespace Solace
