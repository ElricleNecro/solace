#include <filesystem>
#include <fstream>

#include <glm/gtc/type_ptr.hpp>

#include <shaderc/shaderc.hpp>

#include <spirv_cross/spirv_cross.hpp>
#include <spirv_cross/spirv_glsl.hpp>

#include "Solace/core/log.h"
#include "Solace/renderer/opengl/opengl_shader.h"
#include "Solace/spch.h"

namespace Solace {
	namespace Utils {
		static std::string get_cache_directory(void) {
			return "assets/cache/shader/opengl";
		}

		static void create_cache_directory(void) {
			std::string cache_dir = get_cache_directory();
			if (!std::filesystem::exists(cache_dir))
				std::filesystem::create_directories(cache_dir);
		}

		// static const char *shader_type_cached_vulkan_fileextension(gl::GLenum stage) {
		// 	switch (stage) {
		// 	case gl::GL_VERTEX_SHADER:
		// 		return ".vulkan.cache.vert";
		// 	case gl::GL_FRAGMENT_SHADER:
		// 		return ".vulkan.cache.frag";
		// 	default:
		// 		break;
		// 	}
		// 	SOLACE_CORE_ASSERT(false, "Unsupported shader type");
		// 	return nullptr;
		// }

		static const char *shader_type_cached_opengl_fileextension(gl::GLenum stage) {
			switch (stage) {
			case gl::GL_VERTEX_SHADER:
				return ".opengl.cache.vert";
			case gl::GL_FRAGMENT_SHADER:
				return ".opengl.cache.frag";
			default:
				break;
			}
			SOLACE_CORE_ASSERT(false, "Unsupported shader type");
			return nullptr;
		}

		static shaderc_shader_kind shader_type_to_shaderc(gl::GLenum type) {
			switch (type) {
			case gl::GL_VERTEX_SHADER:
				return shaderc_glsl_vertex_shader;
			case gl::GL_FRAGMENT_SHADER:
				return shaderc_glsl_fragment_shader;
			default:
				break;
			}
			SOLACE_CORE_ASSERT(false, "Unknown shader type.");
			return (shaderc_shader_kind)0;
		}

		static gl::GLenum shader_type_from_string(const std::string &type) {
			if (type == "vertex")
				return gl::GL_VERTEX_SHADER;

			if (type == "fragment" || type == "pixel")
				return gl::GL_FRAGMENT_SHADER;

			SOLACE_CORE_ASSERT(false, "Unknow shader type.");
			return (gl::GLenum)0;
		}

		static const char *shader_type_to_string(const gl::GLenum type) {
			switch (type) {
			case gl::GL_VERTEX_SHADER:
				return "GL_VERTEX_SHADER";
			case gl::GL_FRAGMENT_SHADER:
				return "GL_FRAGMENT_SHADER";
			default:
				SOLACE_CORE_ASSERT(false, "Unsupported shader stage.");
				return nullptr;
			}
		}
	} // namespace Utils

	OpenGLShader::OpenGLShader(const std::string &filepath) : file_path(filepath) {
		SL_PROFILE_FUNCTION();

		Utils::create_cache_directory();

		std::string source = this->read_file(filepath);
		auto shader_sources = this->preprocess(source);

		this->compile(shader_sources);

		auto last_slash = filepath.find_last_of("/\\");
		last_slash = last_slash == std::string::npos ? 0 : last_slash + 1;
		auto last_dot = filepath.rfind('.');
		auto count = last_dot == std::string::npos ? filepath.size() - last_slash : last_dot - last_slash;
		this->name = filepath.substr(last_slash, count);
	}

	OpenGLShader::OpenGLShader(const std::string &name, const std::string &vertex, const std::string &fragment) : file_path(name), name(name) {
		this->compile({ { gl::GL_VERTEX_SHADER, vertex }, { gl::GL_FRAGMENT_SHADER, fragment } });
	}

	OpenGLShader::~OpenGLShader(void) {
		SL_PROFILE_FUNCTION();

		for (auto shader : this->shaders)
			gl::glDeleteShader(shader);

		gl::glDeleteProgram(this->id);
	}

	void OpenGLShader::bind(void) const {
		SL_PROFILE_FUNCTION();

		gl::glUseProgram(this->id);
	}

	void OpenGLShader::unbind(void) const {
		SL_PROFILE_FUNCTION();

		gl::glUseProgram(0);
	}

	void OpenGLShader::set_mat(const std::string &name, const glm::mat3 &matrix) {
		SL_PROFILE_FUNCTION();

		gl::GLint location = gl::glGetUniformLocation(this->id, name.c_str());
		gl::glUniformMatrix3fv(location, 1, gl::GL_FALSE, glm::value_ptr(matrix));
	}

	void OpenGLShader::set_mat(const std::string &name, const glm::mat4 &matrix) {
		SL_PROFILE_FUNCTION();

		gl::GLint location = gl::glGetUniformLocation(this->id, name.c_str());
		gl::glUniformMatrix4fv(location, 1, gl::GL_FALSE, glm::value_ptr(matrix));
	}

	void OpenGLShader::set_int(const std::string &name, const int value) {
		SL_PROFILE_FUNCTION();

		gl::GLint location = gl::glGetUniformLocation(this->id, name.c_str());
		gl::glUniform1i(location, value);
	}

	void OpenGLShader::set_int(const std::string &name, const int *values, uint32_t count) {
		SL_PROFILE_FUNCTION();

		gl::GLint location = gl::glGetUniformLocation(this->id, name.c_str());
		gl::glUniform1iv(location, count, values);
	}

	void OpenGLShader::set_float(const std::string &name, const float value) {
		SL_PROFILE_FUNCTION();

		gl::GLint location = gl::glGetUniformLocation(this->id, name.c_str());
		gl::glUniform1f(location, value);
	}

	void OpenGLShader::set_float(const std::string &name, const glm::vec2 &values) {
		SL_PROFILE_FUNCTION();

		gl::GLint location = gl::glGetUniformLocation(this->id, name.c_str());
		gl::glUniform2f(location, values.x, values.y);
	}

	void OpenGLShader::set_float(const std::string &name, const glm::vec3 &values) {
		SL_PROFILE_FUNCTION();

		gl::GLint location = gl::glGetUniformLocation(this->id, name.c_str());
		gl::glUniform3f(location, values.x, values.y, values.z);
	}

	void OpenGLShader::set_float(const std::string &name, const glm::vec4 &values) {
		SL_PROFILE_FUNCTION();

		gl::GLint location = gl::glGetUniformLocation(this->id, name.c_str());
		gl::glUniform4f(location, values.x, values.y, values.z, values.w);
	}

	std::unordered_map<gl::GLenum, std::string> OpenGLShader::preprocess(const std::string &source) {
		SL_PROFILE_FUNCTION();

		std::unordered_map<gl::GLenum, std::string> sources;

		const char *tokenType = "#type";
		size_t toeknTypeLength = strlen(tokenType);
		size_t pos = source.find(tokenType, 0);
		while (pos != std::string::npos) {
			size_t eol = source.find_first_of("\r\n", pos);
			SOLACE_CORE_ASSERT(eol != std::string::npos, "Syntax error.");

			size_t begin = pos + toeknTypeLength + 1;
			std::string type = source.substr(begin, eol - begin);
			SOLACE_CORE_ASSERT(Utils::shader_type_from_string(type) != 0, "Invalid shader type specified.");

			size_t nextLinePos = source.find_first_not_of("\r\n", eol);
			pos = source.find(tokenType, nextLinePos);
			sources[Utils::shader_type_from_string(type)] = source.substr(nextLinePos, pos - (nextLinePos == std::string::npos ? source.size() - 1 : nextLinePos));
		}

		return sources;
	}

	std::string OpenGLShader::read_file(const std::string &filepath) {
		SL_PROFILE_FUNCTION();

		std::ifstream in(filepath, std::ios::in | std::ios::binary);
		std::string result;

		if (!in) {
			SL_CORE_ERROR("Could not open file '{0}'.", filepath);
			return "";
		}

		in.seekg(0, std::ios::end);
		result.resize(in.tellg());

		in.seekg(0, std::ios::beg);
		in.read(&result[0], result.size());

		in.close();

		return result;
	}

	void OpenGLShader::compile(const std::unordered_map<gl::GLenum, std::string> &shader_sources) {
		SL_PROFILE_FUNCTION();

		this->id = gl::glCreateProgram();

		this->spirv.clear();
		this->shaders.reserve(shader_sources.size());
		for (auto &kv : shader_sources) {
			gl::GLenum type = kv.first;
			const std::string &source = kv.second;
			this->compile(type, source);
		}

		for (auto &&[type, data] : this->spirv) {
			this->reflect(type, data);
		}

		for (auto &&[type, spirv] : this->spirv) {
			gl::GLuint shader_id = gl::glCreateShader(type);
			this->shaders.emplace_back(shader_id);

			gl::glShaderBinary(1, &shader_id, gl::GL_SHADER_BINARY_FORMAT_SPIR_V, spirv.data(), spirv.size() * sizeof(uint32_t));
			gl::glSpecializeShader(shader_id, "main", 0, nullptr, nullptr);
			gl::glAttachShader(this->id, shader_id);
		}

		gl::glLinkProgram(this->id);

		gl::GLboolean success = 1;
		gl::glGetProgramiv(this->id, gl::GL_LINK_STATUS, &success);

		if (success == gl::GL_FALSE) {
			int info_length = 0;
			gl::glGetProgramiv(this->id, gl::GL_INFO_LOG_LENGTH, &info_length);

			std::vector<char> error_msg(info_length + 1);
			gl::glGetProgramInfoLog(this->id, info_length, nullptr, &error_msg[0]);

			gl::glDeleteProgram(this->id);

			for (auto shader : this->shaders)
				gl::glDeleteShader(shader);

			SL_CORE_ERROR("Error while creating the shader program: {0}.", error_msg.data());
			SOLACE_CORE_ASSERT(false, "Error while creating the shader program.");
		}

		for (auto shader : this->shaders) {
			gl::glDetachShader(this->id, shader);
		}
	}

	void OpenGLShader::compile(const gl::GLenum type, const std::string &src) {
		SL_PROFILE_FUNCTION();

		shaderc::Compiler compiler;
		shaderc::CompileOptions options;

		options.SetTargetEnvironment(shaderc_target_env_opengl, shaderc_env_version_opengl_4_5);
		options.SetOptimizationLevel(shaderc_optimization_level_performance);
		options.AddMacroDefinition("OPENGL");

		std::filesystem::path cache_directory = Utils::get_cache_directory();
		std::filesystem::path file_path = this->file_path;
		std::filesystem::path cached_file_path = cache_directory / (file_path.filename().string() + Utils::shader_type_cached_opengl_fileextension(type));

		// If the cached file exists, load it:
		if (std::filesystem::exists(cached_file_path)) {
			std::ifstream in(cached_file_path, std::ios::in | std::ios::binary);
			in.seekg(0, std::ios::end);
			auto size = in.tellg();
			in.seekg(0, std::ios::beg);

			auto &data = this->spirv[type];
			data.resize(size / sizeof(uint32_t));
			in.read((char *)data.data(), size);
		} else { // Else we create it:
			shaderc::SpvCompilationResult module = compiler.CompileGlslToSpv(src, Utils::shader_type_to_shaderc(type), this->file_path.c_str(), options);
			if (module.GetCompilationStatus() != shaderc_compilation_status_success) {
				SOLACE_CORE_ASSERT(false, module.GetErrorMessage());
			}

			spirv_cross::CompilerGLSL glsl(std::vector<uint32_t>(module.cbegin(), module.cend()));
			const auto recompiled_src = glsl.compile();
			module = compiler.CompileGlslToSpv(recompiled_src, Utils::shader_type_to_shaderc(type), this->file_path.c_str());
			if (module.GetCompilationStatus() != shaderc_compilation_status_success) {
				SOLACE_CORE_ASSERT(false, module.GetErrorMessage());
			}

			this->spirv[type] = std::vector<uint32_t>(module.cbegin(), module.cend());

			std::ofstream out(cached_file_path, std::ios::out | std::ios::binary);
			if (out.is_open()) {
				auto &data = this->spirv[type];
				out.write((char *)data.data(), data.size() * sizeof(uint32_t));
				out.flush();
				out.close();
			}
		}
	}

	void OpenGLShader::reflect(gl::GLenum type, const std::vector<uint32_t> data) {
		spirv_cross::Compiler compiler(data);
		spirv_cross::ShaderResources resources = compiler.get_shader_resources();

		SL_CORE_TRACE("OpenGLShader::reflect: {} {}", Utils::shader_type_to_string(type), this->file_path);
		SL_CORE_TRACE("\t{} uniform buffers", resources.uniform_buffers.size());
		SL_CORE_TRACE("\t{} resources", resources.sampled_images.size());

		if (resources.uniform_buffers.size() > 0) {
			SL_CORE_TRACE("Uniform buffers:");
			for (const auto &resource : resources.uniform_buffers) {
				const auto &buffer_type = compiler.get_type(resource.base_type_id);
				uint32_t buffer_size = compiler.get_declared_struct_size(buffer_type);
				uint32_t binding = compiler.get_decoration(resource.id, spv::DecorationBinding);
				int member_count = buffer_type.member_types.size();

				SL_CORE_TRACE("\t{}:", resource.name);
				SL_CORE_TRACE("\t\tsize: {}", buffer_size);
				SL_CORE_TRACE("\t\tbinding: {}", binding);
				SL_CORE_TRACE("\t\tmembers: {}", member_count);
			}
		}
	}
} // namespace Solace
