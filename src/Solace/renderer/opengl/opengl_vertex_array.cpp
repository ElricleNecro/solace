#include "Solace/renderer/opengl/opengl_vertex_array.h"
#include "Solace/spch.h"

namespace Solace {
	static gl::GLenum ShaderDataType_to_GLenum(Solace::ShaderDataType type) {
		switch (type) {
		case Solace::ShaderDataType::Float:
		case Solace::ShaderDataType::Float2:
		case Solace::ShaderDataType::Float3:
		case Solace::ShaderDataType::Float4:
		case Solace::ShaderDataType::Mat3:
		case Solace::ShaderDataType::Mat4:
			return gl::GL_FLOAT;
		case Solace::ShaderDataType::Int:
		case Solace::ShaderDataType::Int2:
		case Solace::ShaderDataType::Int3:
		case Solace::ShaderDataType::Int4:
			return gl::GL_INT;
		case Solace::ShaderDataType::Bool:
			return gl::GL_BOOL;
		case Solace::ShaderDataType::None:
			SOLACE_ASSERT(false, "Unknown shader type!");
		}

		return gl::GLenum(0);
	}

	OpenGLVertexArray::OpenGLVertexArray(void) : id(0) {
		SL_PROFILE_FUNCTION();

		gl::glCreateVertexArrays(1, &this->id);
	}

	OpenGLVertexArray::~OpenGLVertexArray(void) {
		SL_PROFILE_FUNCTION();

		gl::glDeleteVertexArrays(1, &this->id);
	}

	void OpenGLVertexArray::bind(void) const {
		SL_PROFILE_FUNCTION();

		gl::glBindVertexArray(this->id);
	}

	void OpenGLVertexArray::unbind(void) const {
		SL_PROFILE_FUNCTION();

		gl::glBindVertexArray(0);
	}

	void OpenGLVertexArray::add_vertex_buffer(const Ref<VertexBuffer> &buffer) {
		SL_PROFILE_FUNCTION();

		SOLACE_CORE_ASSERT(buffer->get_layout().get_elements().size(), "VertexBuffer object has no layout.");

		gl::glBindVertexArray(this->id);
		buffer->bind();

		uint32_t idx = 0;
		const auto &layout = buffer->get_layout();
		for (const auto &element : buffer->get_layout()) {
			const auto count = element.get_component_count();

			switch (element.type) {
			case Solace::ShaderDataType::Float:
			case Solace::ShaderDataType::Float2:
			case Solace::ShaderDataType::Float3:
			case Solace::ShaderDataType::Float4:
				gl::glEnableVertexAttribArray(idx);
				gl::glVertexAttribPointer(idx,
							  element.get_component_count(),
							  ShaderDataType_to_GLenum(element.type),
							  element.normalised ? gl::GL_TRUE : gl::GL_FALSE,
							  layout.get_stride(),
							  reinterpret_cast<const void *>(element.offset));

				idx++;
				break;

			case Solace::ShaderDataType::Int:
			case Solace::ShaderDataType::Int2:
			case Solace::ShaderDataType::Int3:
			case Solace::ShaderDataType::Int4:
			case Solace::ShaderDataType::Bool:
				gl::glEnableVertexAttribArray(idx);
				gl::glVertexAttribIPointer(idx,
							   element.get_component_count(),
							   ShaderDataType_to_GLenum(element.type),
							   layout.get_stride(),
							   reinterpret_cast<const void *>(element.offset));

				idx++;
				break;

			case Solace::ShaderDataType::Mat3:
			case Solace::ShaderDataType::Mat4:
				for (uint8_t elem = 0; elem < count; elem++) {
					gl::glEnableVertexAttribArray(idx);
					gl::glVertexAttribPointer(idx,
								  element.get_component_count(),
								  ShaderDataType_to_GLenum(element.type),
								  element.normalised ? gl::GL_TRUE : gl::GL_FALSE,
								  layout.get_stride(),
								  reinterpret_cast<const void *>(element.offset + sizeof(float) * count * elem));
					gl::glVertexAttribDivisor(idx, 1);

					idx++;
				}
				break;
			case Solace::ShaderDataType::None:
				SOLACE_ASSERT(false, "Unknown shader type!");
				break;
			}
		}

		gl::glBindVertexArray(0);
		buffer->unbind();

		this->vertex_buffers.push_back(buffer);
	}

	void OpenGLVertexArray::set_index_buffer(const Ref<IndexBuffer> &buffer) {
		SL_PROFILE_FUNCTION();

		gl::glBindVertexArray(this->id);
		buffer->bind();
		gl::glBindVertexArray(0);
		buffer->unbind();

		this->index_buffer = buffer;
	}

	const std::vector<Ref<VertexBuffer>> &OpenGLVertexArray::get_vertex_buffers(void) const {
		return this->vertex_buffers;
	}

	const Ref<IndexBuffer> &OpenGLVertexArray::get_index_buffer(void) const {
		return this->index_buffer;
	}
} // namespace Solace
