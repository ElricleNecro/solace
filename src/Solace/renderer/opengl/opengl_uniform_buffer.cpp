#include "Solace/spch.h"

#include "Solace/renderer/opengl/opengl_uniform_buffer.h"

namespace Solace {
	OpenGLUniformBuffer::OpenGLUniformBuffer(size_t size, uint32_t binding) {
		gl::glCreateBuffers(1, &this->id);
		gl::glNamedBufferData(this->id, size, nullptr, gl::GL_DYNAMIC_DRAW);
		gl::glBindBufferBase(gl::GL_UNIFORM_BUFFER, binding, this->id);
	}

	OpenGLUniformBuffer::~OpenGLUniformBuffer(void) {
		gl::glDeleteBuffers(1, &this->id);
	}

	void OpenGLUniformBuffer::set_data(const void *data, size_t size, size_t offset) {
		gl::glNamedBufferSubData(this->id, offset, size, data);
	}
} // namespace Solace
