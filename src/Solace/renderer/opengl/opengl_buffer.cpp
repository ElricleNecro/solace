#include "Solace/renderer/opengl/opengl_buffer.h"
#include "Solace/spch.h"

namespace Solace {
	OpenGLVertexBuffer::OpenGLVertexBuffer(const uint32_t size) {
		SL_PROFILE_FUNCTION();

		gl::glCreateBuffers(1, &this->id);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, this->id);
		gl::glBufferData(gl::GL_ARRAY_BUFFER, size, nullptr, gl::GL_DYNAMIC_DRAW);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
	}

	OpenGLVertexBuffer::OpenGLVertexBuffer(const float *vertices, const uint32_t size) {
		SL_PROFILE_FUNCTION();

		gl::glCreateBuffers(1, &this->id);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, this->id);
		gl::glBufferData(gl::GL_ARRAY_BUFFER, size, vertices, gl::GL_STATIC_DRAW);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
	}

	OpenGLVertexBuffer::~OpenGLVertexBuffer(void) {
		SL_PROFILE_FUNCTION();

		gl::glDeleteBuffers(1, &this->id);
	}

	void OpenGLVertexBuffer::bind(void) const {
		SL_PROFILE_FUNCTION();

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, this->id);
	}

	void OpenGLVertexBuffer::unbind(void) const {
		SL_PROFILE_FUNCTION();

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
	}

	void OpenGLVertexBuffer::set_layout(const BufferLayout &layout) {
		SL_PROFILE_FUNCTION();

		this->layout = layout;
	}

	void OpenGLVertexBuffer::set_data(const void *data, uint32_t size) {
		SL_PROFILE_FUNCTION();

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, this->id);
		gl::glBufferSubData(gl::GL_ARRAY_BUFFER, 0, size, data);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
	}

	OpenGLIndexBuffer::OpenGLIndexBuffer(const uint32_t *vertices, const uint32_t count) : count(count) {
		SL_PROFILE_FUNCTION();

		gl::glCreateBuffers(1, &this->id);
		gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, this->id);
		gl::glBufferData(gl::GL_ELEMENT_ARRAY_BUFFER, count * sizeof(uint32_t), vertices, gl::GL_STATIC_DRAW);
		gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	OpenGLIndexBuffer::~OpenGLIndexBuffer(void) {
		SL_PROFILE_FUNCTION();

		gl::glDeleteBuffers(1, &this->id);
	}

	void OpenGLIndexBuffer::bind(void) const {
		SL_PROFILE_FUNCTION();

		gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, this->id);
	}

	void OpenGLIndexBuffer::unbind(void) const {
		SL_PROFILE_FUNCTION();

		gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, 0);
	}
} // namespace Solace
