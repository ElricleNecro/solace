#include "Solace/renderer/opengl/opengl_rendererapi.h"
#include "Solace/spch.h"

namespace Solace {
	void OpenGLRendererAPI::init(void) {
		SL_PROFILE_FUNCTION();

		gl::glEnable(gl::GL_BLEND);
		gl::glBlendFunc(gl::GL_SRC_ALPHA, gl::GL_ONE_MINUS_SRC_ALPHA);

		gl::glEnable(gl::GL_DEPTH_TEST);
		gl::glEnable(gl::GL_LINE_SMOOTH);
	}

	void OpenGLRendererAPI::set_clear_color(const glm::vec4 &color) {
		gl::glClearColor(color.r, color.g, color.b, color.a);
	}

	void OpenGLRendererAPI::clear(void) {
		gl::glClear(gl::GL_COLOR_BUFFER_BIT | gl::GL_DEPTH_BUFFER_BIT);
	}

	void OpenGLRendererAPI::draw_indexed(const Ref<VertexArray> &vao, uint32_t index_count) {
		index_count = index_count == 0 ? vao->get_index_buffer()->get_count() : index_count;
		gl::glDrawElements(gl::GL_TRIANGLES, index_count, gl::GL_UNSIGNED_INT, nullptr);
	}

	void OpenGLRendererAPI::draw_lines([[maybe_unused]] const Ref<VertexArray> &vao, uint32_t index_count) {
		gl::glDrawArrays(gl::GL_LINES, 0, index_count);
	}

	void OpenGLRendererAPI::set_line_width(float width) {
		gl::glLineWidth(width);
	}

	void OpenGLRendererAPI::viewport(int x, int y, int width, int height) {
		gl::glViewport(x, y, width, height);
	}
} // namespace Solace
