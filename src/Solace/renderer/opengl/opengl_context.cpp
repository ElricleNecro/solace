#ifdef USE_GLEW
#include <GL/glew.h>
#elif defined(USE_GLBINDING)
#include <glbinding/gl/gl.h>
#include <glbinding/glbinding.h> // Initialize with glbinding::initialize()
#endif

#include "Solace/core/exception.h"
#include "Solace/core/log.h"
#include "Solace/renderer/opengl/opengl_context.h"
#include "Solace/spch.h"

namespace Solace {
	OpenGLRenderer::OpenGLRenderer(void) : window(nullptr), ogl_context(nullptr) {
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
		SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	};

	OpenGLRenderer::~OpenGLRenderer(void) {
		SDL_GL_DeleteContext(this->ogl_context);
	}

	void OpenGLRenderer::init(SDL_Window *window) {
		SL_PROFILE_FUNCTION();

		this->window = window;
		this->ogl_context = SDL_GL_CreateContext(this->window);
		if (this->ogl_context == nullptr) {
			SDL_DestroyWindow(window);
			SDL_Quit();
			throw SDLCreateOGLContextException(SDL_GetError());
		}

		SDL_GL_MakeCurrent(this->window, this->ogl_context);
		SDL_GL_SetSwapInterval(1); // Enable vsync

#ifdef USE_GLEW
		GLenum err = glewInit();
		if (err != GLEW_OK) {
			SDL_GL_DeleteContext(this->ogl_context);
			SDL_DestroyWindow(window);
			SDL_Quit();
			throw OGLInitException(glewGetErrorString(err));
		}
#elif defined(USE_GLBINDING)
		glbinding::initialize([](const char *name) { return (glbinding::ProcAddress)SDL_GL_GetProcAddress(name); });
#endif

		SL_CORE_INFO("OpenGL vendor: {}.", gl::glGetString(gl::GL_VENDOR));
		SL_CORE_INFO("OpenGL renderer: {}.", gl::glGetString(gl::GL_RENDERER));
		SL_CORE_INFO("OpenGL version: {}", gl::glGetString(gl::GL_VERSION));
	}

	uint32_t OpenGLRenderer::flags(void) {
		return SDL_WINDOW_OPENGL;
	}

	void OpenGLRenderer::swap_buffers(void) {
		SL_PROFILE_FUNCTION();

		SDL_GL_SwapWindow(this->window);
	}

	void OpenGLRenderer::update_window_size(int *width, int *height) const {
		SL_PROFILE_FUNCTION();

		SDL_GL_GetDrawableSize(this->window, width, height);
	}
} // namespace Solace
