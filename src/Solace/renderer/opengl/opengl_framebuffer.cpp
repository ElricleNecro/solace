#include "Solace/renderer/opengl/opengl_framebuffer.h"
#include "Solace/spch.h"

namespace Solace {
	static const uint32_t max_framebuffer_size = 8192;

	namespace Utils {
		static bool is_depth_format(const FrameBufferTextureFormat format) {
			switch (format) {
			case FrameBufferTextureFormat::DEPTH24STENCIL8:
				return true;

			case FrameBufferTextureFormat::RED_INTEGER:
			case FrameBufferTextureFormat::RGBA8:
			case FrameBufferTextureFormat::None:
				return false;
			}

			SOLACE_CORE_ASSERT(false, "We should not get out of the switch.");
			return false;
		}

		inline static gl::GLenum texture_target(bool multi_sampled) {
			return multi_sampled ? gl::GL_TEXTURE_2D_MULTISAMPLE : gl::GL_TEXTURE_2D;
		}

		inline static void create_texture(const bool multi_sample, uint32_t *ids, const size_t count) {
			gl::glCreateTextures(texture_target(multi_sample), count, ids);
		}

		inline static void bind_texture(bool multi_sample, uint32_t id) {
			gl::glBindTexture(texture_target(multi_sample), id);
		}

		static void attach_color_texture(uint32_t id, int samples, gl::GLenum internal_format, gl::GLenum format, uint32_t width, uint32_t height, size_t index) {
			const bool multi_sample = samples > 1;

			if (multi_sample) {
				gl::glTexImage2DMultisample(gl::GL_TEXTURE_2D_MULTISAMPLE, samples, format, width, height, gl::GL_FALSE);
			} else {
				gl::glTexImage2D(gl::GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, gl::GL_UNSIGNED_BYTE, nullptr);

				gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
				gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
				gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_R, gl::GL_CLAMP_TO_EDGE);
				gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
				gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);
			}

			gl::glFramebufferTexture2D(gl::GL_FRAMEBUFFER, gl::GL_COLOR_ATTACHMENT0 + index, texture_target(multi_sample), id, 0);
		}

		static void attach_depth_texture(uint32_t id, int samples, gl::GLenum format, gl::GLenum attachment_type, uint32_t width, uint32_t height) {
			const bool multi_sample = samples > 1;

			if (multi_sample) {
				gl::glTexStorage2DMultisample(gl::GL_TEXTURE_2D_MULTISAMPLE, samples, format, width, height, gl::GL_FALSE);
			} else {
				gl::glTexStorage2D(gl::GL_TEXTURE_2D, 1, format, width, height);

				gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
				gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
				gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_R, gl::GL_CLAMP_TO_EDGE);
				gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
				gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);
			}

			//gl::glTexImage2D(gl::GL_TEXTURE_2D, 0, gl::GL_DEPTH24_STENCIL8, this->spec.width, this->spec.height, 0, gl::GL_DEPTH_STENCIL, gl::GL_UNSIGNED_INT_24_8, nullptr);
			gl::glFramebufferTexture2D(gl::GL_FRAMEBUFFER, attachment_type, texture_target(multi_sample), id, 0);
		}

		static gl::GLenum solace_texture_format_to_gl(FrameBufferTextureFormat format) {
			switch (format) {
			// Color:
			case FrameBufferTextureFormat::RGBA8:
				return gl::GL_RGBA8;
			case FrameBufferTextureFormat::RED_INTEGER:
				return gl::GL_RED_INTEGER;
			// Depth/stencil:
			case FrameBufferTextureFormat::DEPTH24STENCIL8:
			case FrameBufferTextureFormat::None:
				SOLACE_CORE_ASSERT(false, "We should not reach this statement!");
				return (gl::GLenum)0;
			}

			SOLACE_CORE_ASSERT(false, "We should not reach this statement!");
			return (gl::GLenum)0;
		}
	} // namespace Utils

	OpenGLFrameBuffer::OpenGLFrameBuffer(const FrameBufferSpecification &spec) : spec(spec) {
		for (auto format : this->spec.attachments.attachments) {
			if (!Utils::is_depth_format(format.texture_format))
				this->color_attachment_specs.emplace_back(format);
			else
				this->depth_attachment_spec = format;
		}

		this->invalidate();
	}

	OpenGLFrameBuffer::~OpenGLFrameBuffer(void) {
		if (this->renderer_id != 0) {
			gl::glDeleteFramebuffers(1, &this->renderer_id);
			gl::glDeleteTextures(this->id_color_attachments.size(), this->id_color_attachments.data());
			gl::glDeleteTextures(1, &this->id_depth_attachment);
		}
	}

	void OpenGLFrameBuffer::invalidate(void) {
		if (this->renderer_id != 0) {
			gl::glDeleteFramebuffers(1, &this->renderer_id);
			gl::glDeleteTextures(this->id_color_attachments.size(), this->id_color_attachments.data());
			gl::glDeleteTextures(1, &this->id_depth_attachment);

			this->id_color_attachments.clear();
			this->id_depth_attachment = 0;
		}

		gl::glCreateFramebuffers(1, &this->renderer_id);

		gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, this->renderer_id);

		bool multi_sample = this->spec.samples > 1;
		if (this->color_attachment_specs.size()) {
			this->id_color_attachments.resize(this->color_attachment_specs.size());
			Utils::create_texture(multi_sample, this->id_color_attachments.data(), this->id_color_attachments.size());

			// Attachements:
			for (size_t i = 0; i < this->color_attachment_specs.size(); i++) {
				Utils::bind_texture(multi_sample, this->id_color_attachments[i]);

				switch (this->color_attachment_specs[i].texture_format) {
				case FrameBufferTextureFormat::RGBA8:
					Utils::attach_color_texture(this->id_color_attachments[i],
								    this->spec.samples,
								    gl::GL_RGBA8,
								    gl::GL_RGBA,
								    this->spec.width,
								    this->spec.height,
								    i);
					break;

				case FrameBufferTextureFormat::RED_INTEGER:
					Utils::attach_color_texture(this->id_color_attachments[i],
								    this->spec.samples,
								    gl::GL_R32I,
								    gl::GL_RED_INTEGER,
								    this->spec.width,
								    this->spec.height,
								    i);
					break;

				case FrameBufferTextureFormat::None:
				case FrameBufferTextureFormat::DEPTH24STENCIL8:
					SOLACE_CORE_ASSERT(false, "We should not hit this!");
					break;
				}
			}
		}

		if (this->depth_attachment_spec.texture_format != FrameBufferTextureFormat::None) {
			Utils::create_texture(multi_sample, &this->id_depth_attachment, 1);
			Utils::bind_texture(multi_sample, this->id_depth_attachment);

			switch (this->depth_attachment_spec.texture_format) {
			case FrameBufferTextureFormat::DEPTH24STENCIL8:
				Utils::attach_depth_texture(this->id_depth_attachment,
							    this->spec.samples,
							    gl::GL_DEPTH24_STENCIL8,
							    gl::GL_DEPTH_STENCIL_ATTACHMENT,
							    this->spec.width,
							    this->spec.height);
				break;

			case FrameBufferTextureFormat::RED_INTEGER:
			case FrameBufferTextureFormat::RGBA8:
			case FrameBufferTextureFormat::None:
				SOLACE_CORE_ASSERT(false, "We should not hit this!");
				break;
			}
		}

		if (this->id_color_attachments.size() > 1) {
			SOLACE_CORE_ASSERT(this->id_color_attachments.size() <= 4, "Can't have more than 4 colors attachement to a framebuffer.");
			gl::GLenum buffers[4] = { gl::GL_COLOR_ATTACHMENT0, gl::GL_COLOR_ATTACHMENT1, gl::GL_COLOR_ATTACHMENT2, gl::GL_COLOR_ATTACHMENT3 };
			gl::glDrawBuffers(this->id_color_attachments.size(), buffers);
		} else if (this->id_color_attachments.empty()) {
			// Only depth pass
			gl::glDrawBuffer(gl::GL_NONE);
		}

		SOLACE_CORE_ASSERT(gl::glCheckFramebufferStatus(gl::GL_FRAMEBUFFER) == gl::GL_FRAMEBUFFER_COMPLETE, "Framebuffer is incomplete.");

		gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);
	}

	void OpenGLFrameBuffer::resize(uint32_t width, uint32_t height) {
		if (width == 0 || height == 0 || width > max_framebuffer_size || height > max_framebuffer_size) {
			SL_CORE_WARN("Trying to resize framebuffer with invalid size ({}, {})!", width, height);
			return;
		}

		this->spec.width = width;
		this->spec.height = height;

		this->invalidate();
	}

	void OpenGLFrameBuffer::bind(void) const {
		gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, this->renderer_id);
		gl::glViewport(0, 0, this->spec.width, this->spec.height);
	}

	void OpenGLFrameBuffer::unbind(void) const {
		gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);
	}

	int OpenGLFrameBuffer::read_pixel(uint32_t attachment_idx, uint32_t x, uint32_t y) const {
		SOLACE_CORE_ASSERT(attachment_idx < this->id_color_attachments.size(), "Wrong attachment id.");

		gl::glReadBuffer(gl::GL_COLOR_ATTACHMENT0 + attachment_idx);

		int pixel_data = 0;
		gl::glReadPixels(x, y, 1, 1, gl::GL_RED_INTEGER, gl::GL_INT, &pixel_data);

		return pixel_data;
	}

	void OpenGLFrameBuffer::clear_color_attachment(size_t index, int value) const {
		SOLACE_CORE_ASSERT(index < this->id_color_attachments.size(), "Wrong attachment id.");

		gl::glClearTexImage(this->id_color_attachments[index],
				    0,
				    Utils::solace_texture_format_to_gl(this->color_attachment_specs[index].texture_format),
				    gl::GL_INT,
				    &value);
	}
} // namespace Solace
