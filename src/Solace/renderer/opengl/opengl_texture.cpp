#include "Solace/renderer/opengl/opengl_texture.h"
#include "Solace/spch.h"

namespace Solace {
	OpenGLTexture2D::OpenGLTexture2D(const std::string &path) : path(path) {
		SL_PROFILE_FUNCTION();

		int width, height, channels;
		stbi_set_flip_vertically_on_load(1);
		stbi_uc *data = nullptr;
		{
			SL_PROFILE_SCOPE("OpenGLTexture2D::OpenGLTexture2D(const std::string &)::stbi_load");
			data = stbi_load(this->path.c_str(), &width, &height, &channels, 0);
		}

		SOLACE_CORE_ASSERT(data, "Failed to load image.");

		this->width = width;
		this->height = height;

		this->internal_format = (gl::GLenum)0;
		this->data_format = (gl::GLenum)0;

		switch (channels) {
		case 4:
			this->internal_format = gl::GL_RGBA8;
			this->data_format = gl::GL_RGBA;
			break;
		case 3:
			this->internal_format = gl::GL_RGB8;
			this->data_format = gl::GL_RGB;
			break;
		}

		SOLACE_CORE_ASSERT(this->internal_format != 0 && this->data_format != 0, "Format not supported.");

		gl::glCreateTextures(gl::GL_TEXTURE_2D, 1, &this->renderer_id);
		gl::glTextureStorage2D(this->renderer_id, 1, this->internal_format, this->width, this->height);

		gl::glTextureParameteri(this->renderer_id, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
		gl::glTextureParameteri(this->renderer_id, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);

		gl::glTextureParameteri(this->renderer_id, gl::GL_TEXTURE_WRAP_S, gl::GL_REPEAT);
		gl::glTextureParameteri(this->renderer_id, gl::GL_TEXTURE_WRAP_T, gl::GL_REPEAT);

		gl::glTextureSubImage2D(this->renderer_id, 0, 0, 0, this->width, this->height, this->data_format, gl::GL_UNSIGNED_BYTE, data);

		stbi_image_free(data);
	}

	OpenGLTexture2D::OpenGLTexture2D(const uint32_t width, const uint32_t height) : width(width), height(height) {
		SL_PROFILE_FUNCTION();

		this->internal_format = gl::GL_RGBA8;
		this->data_format = gl::GL_RGBA;

		gl::glCreateTextures(gl::GL_TEXTURE_2D, 1, &this->renderer_id);
		gl::glTextureStorage2D(this->renderer_id, 1, this->internal_format, this->width, this->height);

		gl::glTextureParameteri(this->renderer_id, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
		gl::glTextureParameteri(this->renderer_id, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);

		gl::glTextureParameteri(this->renderer_id, gl::GL_TEXTURE_WRAP_S, gl::GL_REPEAT);
		gl::glTextureParameteri(this->renderer_id, gl::GL_TEXTURE_WRAP_T, gl::GL_REPEAT);
	}

	OpenGLTexture2D::~OpenGLTexture2D(void) {
		SL_PROFILE_FUNCTION();

		gl::glDeleteTextures(1, &this->renderer_id);
	}

	void OpenGLTexture2D::set_data(void *data, [[maybe_unused]] uint32_t size) {
		SL_PROFILE_FUNCTION();

		SOLACE_CORE_ASSERT(size == this->width * this->height * (this->data_format == gl::GL_RGBA ? 4 : 3), "Declared texture size and data buffer size does not match");
		gl::glTextureSubImage2D(this->renderer_id, 0, 0, 0, this->width, this->height, this->data_format, gl::GL_UNSIGNED_BYTE, data);
	}

	void OpenGLTexture2D::bind(uint32_t slot) const {
		SL_PROFILE_FUNCTION();

		gl::glBindTextureUnit(slot, this->renderer_id);
	}

	void OpenGLTexture2D::unbind(uint32_t slot) const {
		SL_PROFILE_FUNCTION();

		gl::glBindTextureUnit(slot, 0);
		gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
	}
} // namespace Solace
