#include "Solace/core/log.h"
#include "Solace/spch.h"

#include <spdlog/sinks/stdout_color_sinks.h>

namespace Solace {
	Ref<spdlog::logger> Log::core_logger;
	Ref<spdlog::logger> Log::client_logger;

	void Log::init(void) {
		spdlog::set_pattern("%^[%T] %n: %v%$");

		core_logger = spdlog::stdout_color_mt("Solace");
		core_logger->set_level(spdlog::level::trace);

		client_logger = spdlog::stdout_color_mt("App");
		core_logger->set_level(spdlog::level::trace);
	}
} // namespace Solace
