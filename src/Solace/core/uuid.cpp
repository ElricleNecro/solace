#include "Solace/core/uuid.h"

#include <random>

namespace Solace {
	static std::random_device random_device;
	static std::mt19937_64 engine(random_device());
	static std::uniform_int_distribution<uint64_t> uniform_distrib;

	UUID::UUID(void) : uuid(uniform_distrib(engine)) {
	}

	UUID::UUID(uint64_t uuid) : uuid(uuid) {
	}
} // namespace Solace
