#include "Solace/core/events.h"
#include "Solace/spch.h"

#include "Solace/vendor/imgui_impl_sdl.h"

namespace Solace {
	Events *Events::instance = nullptr;

	Events::Events(const unsigned int width, const unsigned int height)
		: _evt({ 0 }), _winID(0), _x(0), _y(0), _dx(0), _dy(0), _wx(0), _wy(0), _close_window(false), _resized(false), _minimized(false), _event_used(false), _quit(false),
		  _windowSize(width, height) {
		Events::instance = this;
	}

	bool Events::update(void) {
		if (SDL_PollEvent(&this->_evt) == 0)
			return false;

		//ImGui_ImplSDL2_ProcessEvent(&this->_evt);

		// Getting the focused windowID:
		this->_winID = this->_evt.window.windowID;

		this->_wx = 0;
		this->_wy = 0;
		this->_dx = 0;
		this->_dy = 0;
		this->_resized = false;
		this->_event_used = false;

		// Processing the event:
		switch (this->_evt.type) {
		case SDL_WINDOWEVENT:
			if (this->_evt.window.event == SDL_WINDOWEVENT_CLOSE) {
				this->_close_window = true;
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_SHOWN) {
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_HIDDEN) {
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_EXPOSED) {
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_MOVED) {
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_RESIZED) {
				this->_resized = true;
				this->_windowSize = std::make_tuple(this->_evt.window.data1, this->_evt.window.data2);
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_MINIMIZED) {
				this->_minimized = true;
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_MAXIMIZED) {
				this->_minimized = false;
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_RESTORED) {
				this->_minimized = false;
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_ENTER) {
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_LEAVE) {
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_FOCUS_GAINED) {
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_FOCUS_LOST) {
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_TAKE_FOCUS) {
			} else if (this->_evt.window.event == SDL_WINDOWEVENT_HIT_TEST) {
			}
			break;

		case SDL_KEYDOWN:
			this->_last_register = this->_evt.key.keysym.scancode;
			this->_kPress[this->_evt.key.keysym.scancode] = true;
			break;

		case SDL_KEYUP:
			if (this->_last_register == this->_evt.key.keysym.scancode)
				this->_last_register = SDL_NUM_SCANCODES;

			this->_kPress[this->_evt.key.keysym.scancode] = false;
			break;

		case SDL_MOUSEBUTTONDOWN:
			this->_kMouse[this->_evt.button.button] = true;
			break;

		case SDL_MOUSEBUTTONUP:
			this->_kMouse[this->_evt.button.button] = false;
			break;

		case SDL_MOUSEMOTION:
			this->_dx = this->_evt.motion.xrel;
			this->_dy = this->_evt.motion.yrel;
			break;

		case SDL_MOUSEWHEEL:
			this->_wx = this->_evt.wheel.x;
			this->_wy = this->_evt.wheel.y;
			break;

		case SDL_QUIT:
			this->_quit = true;
			break;
		}

		// Getting cursor position:
		this->_x = this->_evt.motion.x;
		this->_y = this->_evt.motion.y;

		return true;
	}

	bool Events::operator[](SDL_Scancode code) const {
		return this->_kPress[code];
	}
} // namespace Solace
