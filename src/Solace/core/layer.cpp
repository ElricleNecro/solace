#include "Solace/core/layer.h"
#include "Solace/spch.h"

namespace Solace {
	bool Layer::on_event([[maybe_unused]] Events &evt) {
		return false;
	}
} // namespace Solace
