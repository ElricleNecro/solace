#include <filesystem>

#include "Solace/core/application.h"
#include "Solace/core/exception.h"
#include "Solace/spch.h"

#include "Solace/renderer/renderer.h"

namespace Solace {
	Application *Application::instance = nullptr;

	Application::Application(const ApplicationSpecification &spec) : Application(spec, Renderer::create()) {
	}

	Application::Application(const ApplicationSpecification &spec, Ref<Renderer> context)
		: running(true), width(spec.width), height(spec.height), events(width, height), spec(spec) {
		SL_PROFILE_FUNCTION();

		{
			SL_PROFILE_SCOPE("SDL2 init");
			if (SDL_Init(SDL_INIT_VIDEO) < 0) {
				throw SDLInitException(SDL_GetError());
			}
		}

		this->renderer = std::move(context);

		if( !spec.working_directory.empty() )
			std::filesystem::current_path(spec.working_directory);

		instance = this;

		{
			SL_PROFILE_SCOPE("SDL2 window creation");
			this->window = SDL_CreateWindow(spec.title.c_str(),
							SDL_WINDOWPOS_UNDEFINED,
							SDL_WINDOWPOS_UNDEFINED,
							this->width,
							this->height,
							SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI | this->renderer->flags());
			if (this->window == nullptr) {
				SDL_Quit();
				throw SDLCreateWindowException(SDL_GetError());
			}
		}

		this->imgui_layer = new ImGuiLayer("Internal ImGuiLayer");
		this->imgui_layer->window = this->window;

		this->renderer->init(this->window);
		this->renderer->update_window_size(&this->width, &this->height);

		this->push_overlay(this->imgui_layer);

		Renderer::init();
	}

	void Application::push(Layer *layer) {
		layer->on_attach();
		this->layer_subsystem.push(layer);
	}

	void Application::push_overlay(Layer *layer) {
		layer->on_attach();
		this->layer_subsystem.push_overlay(layer);
	}

	LayerStack &Application::layers(void) {
		return this->layer_subsystem;
	}

	void Application::run(void) {
		SL_PROFILE_FUNCTION();

		auto previous = std::chrono::system_clock::now();
		auto current = std::chrono::system_clock::now();

		while (this->running) {
			SL_PROFILE_SCOPE("Application::run::main_loop");

			current = std::chrono::system_clock::now();
			std::chrono::duration<float> elapsedTime = current - previous;
			previous = current;

			// TODO: We should process each event as a queue and not a whole (and give each events to all layer until someone accept it or no layer are available):
			while (this->events.update()) {
				if (this->events.quit() || (this->events.close_window() && SDL_GetWindowID(this->window) == this->events.event().window.windowID)) {
					this->running = false;
					return;
				}

				if ((this->events.event().type == SDL_WINDOWEVENT) && (this->events.event().window.event == SDL_WINDOWEVENT_RESIZED)) {
					auto window_size = this->events.size();
					this->width = std::get<0>(window_size);
					this->height = std::get<1>(window_size);
					Renderer::viewport(std::get<0>(window_size), std::get<1>(window_size));
				}

				// Dispatching events:
				for (auto layer : this->layer_subsystem) {
					if (layer->on_event(this->events)) {
						this->events.set_used();
						break;
					}
				}
			}

			if (!this->events.minimized()) {
				// Rendering each OpenGL layer:
				{
					SL_PROFILE_SCOPE("layers on_update");
					for (auto layer : this->layer_subsystem) {
						layer->on_update(elapsedTime.count());
					}
				}

				// Rendering ImGui:
				this->imgui_layer->begin();
				{
					for (auto layer : this->layer_subsystem) {
						SL_PROFILE_SCOPE("layers on_imgui_render");
						layer->on_imgui_render();
					}
				}
				this->imgui_layer->end();

				this->renderer->swap_buffers();
			}
		}
	}

	void Application::quit(void) {
		this->running = false;
	}

	Application::~Application(void) {
		SL_PROFILE_FUNCTION();

		Renderer::shutdown();

		for (auto layer : this->layer_subsystem) {
			layer->on_detach();
			delete layer;
		}

		SDL_DestroyWindow(this->window);
		SDL_Quit();
	}
} // namespace Solace
