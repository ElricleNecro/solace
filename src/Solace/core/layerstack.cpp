#include "Solace/core/layerstack.h"
#include "Solace/spch.h"

namespace Solace {
	LayerStack::~LayerStack(void) {
	}

	void LayerStack::push(Layer *layer) {
		SL_PROFILE_FUNCTION();

		this->layers.emplace(this->layers.begin() + this->index, layer);
		this->index++;
	}

	void LayerStack::push_overlay(Layer *overlay) {
		SL_PROFILE_FUNCTION();

		this->layers.emplace_back(overlay);
	}

	void LayerStack::pop(Layer *layer) {
		SL_PROFILE_FUNCTION();

		auto it = std::find(this->layers.begin(), this->layers.begin() + this->index, layer);
		if (it != this->layers.begin() + this->index) {
			layer->on_detach();
			this->layers.erase(it);
			this->index--;
		}
	}

	Layer *LayerStack::pop(const std::string &layer_name) {
		SL_PROFILE_FUNCTION();

		Layer *layer = this->at(layer_name);
		this->pop(layer);
		return layer;
	}

	Layer *LayerStack::pop_overlay(const std::string &layer_name) {
		SL_PROFILE_FUNCTION();

		Layer *layer = this->at(layer_name);
		this->pop_overlay(layer);
		return layer;
	}

	void LayerStack::pop_overlay(Layer *overlay) {
		SL_PROFILE_FUNCTION();

		auto it = std::find(this->layers.begin() + this->index, this->layers.end(), overlay);
		if (it != this->layers.begin() + this->index) {
			overlay->on_detach();
			this->layers.erase(it);
		}
	}

	const Layer *LayerStack::at(const std::string &pos) const {
		SL_PROFILE_FUNCTION();

		auto layer = std::find_if(std::begin(layers), std::end(layers), [&pos](Layer *val) { return val->get_name() == pos; });

		if (layer == std::end(layers))
			return nullptr;

		return *layer;
	}

	const Layer *LayerStack::operator[](const std::string &pos) const {
		SL_PROFILE_FUNCTION();

		auto layer = std::find_if(std::begin(layers), std::end(layers), [&pos](Layer *val) { return val->get_name() == pos; });

		if (layer == std::end(layers))
			return nullptr;

		return *layer;
	}

	Layer *LayerStack::at(const std::string &pos) {
		auto layer = std::find_if(std::begin(layers), std::end(layers), [&pos](Layer *val) { return val->get_name() == pos; });

		if (layer == std::end(layers))
			return nullptr;

		return *layer;
	}

	Layer *LayerStack::operator[](const std::string &pos) {
		auto layer = std::find_if(std::begin(layers), std::end(layers), [&pos](Layer *val) { return val->get_name() == pos; });

		if (layer == std::end(layers))
			return nullptr;

		return *layer;
	}
} // namespace Solace
