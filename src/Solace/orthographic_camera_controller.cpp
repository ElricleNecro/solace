#include "Solace/orthographic_camera_controller.h"
#include "Solace/core/application.h"
#include "Solace/core/log.h"
#include "Solace/spch.h"

namespace Solace {
	OrthographicCameraController::OrthographicCameraController(float aspect_ratio, bool rotation)
		: aspect_ratio(aspect_ratio), bounds({ -aspect_ratio * this->zoom_level, aspect_ratio * this->zoom_level, -this->zoom_level, this->zoom_level }),
		  camera(this->bounds.left, this->bounds.right, this->bounds.bottom, this->bounds.top), rotation(rotation) {
	}

	void OrthographicCameraController::on_update(float elapsed) {
		SL_PROFILE_FUNCTION();

		bool used = false;

		if (!Solace::Application::get().get_events().used()) {
			if (Application::get().get_events()[SDL_SCANCODE_A]) {
				this->position.x -= this->camera_translation_speed * elapsed;
				used = true;
			}

			if (Application::get().get_events()[SDL_SCANCODE_D]) {
				this->position.x += this->camera_translation_speed * elapsed;
				used = true;
			}

			if (Application::get().get_events()[SDL_SCANCODE_S]) {
				this->position.y -= this->camera_translation_speed * elapsed;
				used = true;
			}

			if (Application::get().get_events()[SDL_SCANCODE_W]) {
				this->position.y += this->camera_translation_speed * elapsed;
				used = true;
			}
		}

		this->camera.set_position(this->position);

		if (!Solace::Application::get().get_events().used() && this->rotation) {
			if (Application::get().get_events()[SDL_SCANCODE_Q]) {
				this->camera_rotation -= this->camera_rotation_speed * elapsed;
				used = true;
			}

			if (Application::get().get_events()[SDL_SCANCODE_E]) {
				this->camera_rotation += this->camera_rotation_speed * elapsed;
				used = true;
			}

			this->camera.set_rotation(this->camera_rotation);
		}

		if (used)
			Solace::Application::get().get_events().set_used();
	}

	bool OrthographicCameraController::on_event([[maybe_unused]] Events &evt) {
		SL_PROFILE_FUNCTION();

		if (evt.used())
			return false;

		this->zoom_level -= evt.wheel_y() * 0.25f;
		this->zoom_level = std::max(this->zoom_level, 0.25f);
		this->camera_translation_speed = this->zoom_level;

		if (evt.resized()) {
			auto size = evt.size();
			this->aspect_ratio = (float)std::get<0>(size) / (float)std::get<1>(size);
			SL_CORE_INFO("Aspect ratio: {} ({}, {})", this->aspect_ratio, std::get<0>(size), std::get<1>(size));
		}

		this->update_camera();

		if (evt.wheel_y() != 0)
			return true;
		return false;
	}

	void OrthographicCameraController::resize(float width, float height) {
		SL_PROFILE_FUNCTION();

		this->aspect_ratio = width / height;
		this->update_camera();
	}
} // namespace Solace
