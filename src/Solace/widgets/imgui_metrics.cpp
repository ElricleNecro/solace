#include "Solace/widgets/imgui_metrics.h"

namespace Solace {
	void Metrics::on_imgui_render(void) {
		ImGui::ShowMetricsWindow();
	}
} // namespace Solace
