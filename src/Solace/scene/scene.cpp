#include "Solace/spch.h"
#include "glm/glm.hpp"

#include "Solace/renderer/editor_camera.h"
#include "Solace/renderer/renderer2D.h"
#include "Solace/scene/components.h"
#include "Solace/scene/entity.h"
#include "Solace/scene/scene.h"
#include "Solace/scene/scriptable_entity.h"

#include "box2d/b2_body.h"
#include "box2d/b2_circle_shape.h"
#include "box2d/b2_fixture.h"
#include "box2d/b2_polygon_shape.h"
#include "box2d/b2_world.h"

namespace Solace {
	static b2BodyType bodytype_to_box2d(RigidBody2DComponent::BodyType value) {
		switch (value) {
		case RigidBody2DComponent::BodyType::Dynamic:
			return b2BodyType::b2_dynamicBody;
		case RigidBody2DComponent::BodyType::Kinematic:
			return b2BodyType::b2_kinematicBody;
		case RigidBody2DComponent::BodyType::Static:
			return b2BodyType::b2_staticBody;
		default:
			SOLACE_ASSERT(false, "Unreachable branch.");
			return b2BodyType::b2_staticBody;
		}
	}

	Scene::Scene(void) {
	}

	Scene::~Scene(void) {
		{
			this->registry.view<NativeScriptComponent>().each([&]([[maybe_unused]] auto entity, auto &nsc) {
				if (nsc.instance != nullptr) {
					nsc.instance->on_destroy();
					nsc.destroy_script(&nsc);
				}
			});
		}

		if( this->world != nullptr )
			delete this->world, this->world = nullptr;
	}

	template <typename... Component>
	static void copy_component(entt::registry &dst, entt::registry &src, std::unordered_map<UUID, entt::entity> dst_map) {
		([&]() {
			auto view = src.view<Component>();
			for(auto src_entity: view) {
				entt::entity dst_entity = dst_map.at(src.get<IDComponent>(src_entity).id);
				auto &src_component = src.get<Component>(src_entity);
				dst.emplace_or_replace<Component>(dst_entity, src_component);
			}
		}(), ...);
	}

	template <typename... Component>
	static void copy_component(ComponentGroup<Component...>, entt::registry &dst, entt::registry &src, std::unordered_map<UUID, entt::entity> dst_map) {
		copy_component<Component...>(dst, src, dst_map);
	}

	template <typename... Component>
	static void copy_component_if_exist(Entity dst, Entity src) {
		([&]() {
			if( src.has<Component>() )
			dst.add_or_replace<Component>(src.get<Component>());
		}(), ...);
	}

	template <typename... Component>
	static void copy_component_if_exist(ComponentGroup<Component...>, Entity dst, Entity src) {
		copy_component_if_exist<Component...>(dst, src);
	}

	Ref<Scene> Scene::copy(Ref<Scene> from) {
		Ref<Scene> new_scene = make_ref<Scene>();

		new_scene->viewport_height = from->viewport_height;
		new_scene->viewport_width = from->viewport_width;

		auto &src = from->registry;
		auto &dst = new_scene->registry;

		std::unordered_map<UUID, entt::entity> dst_map;

		for (auto e : src.view<IDComponent>()) {
			UUID uuid = src.get<IDComponent>(e).id;
			const auto &name = src.get<TagComponent>(e).tag;
			dst_map[uuid] = new_scene->add(uuid, name);
		}

		copy_component(AllComponents{}, dst, src, dst_map);

		return new_scene;
	}

	void Scene::on_update_editor(EditorCamera &camera, [[maybe_unused]] float timestep) {
		Renderer2D::begin_scene(camera);

		this->render_entity();

		Renderer2D::end_scene();
	}

	void Scene::on_update_runtime(float timestep) {
		// Update scripts
		this->registry.view<NativeScriptComponent>().each([&](auto entity, auto &nsc) {
			if (nsc.instance == nullptr) {
				nsc.instance = nsc.instantiate_script();
				nsc.instance->entity = { entity, this };
				nsc.instance->on_create();
			}

			nsc.instance->on_update(timestep);
		});

		const Camera *main_camera = nullptr;
		glm::mat4 main_transform;

		{
			auto view = this->registry.view<TransformComponent, CameraComponent>();
			for (auto entity : view) {
				auto [transform, camera] = view.get<TransformComponent, CameraComponent>(entity);

				if (camera.primary) {
					main_camera = &camera.camera;
					main_transform = transform;
					break;
				}
			}
		}

		if (main_camera == nullptr)
			return;

		// Render
		Renderer2D::begin_scene(main_camera->get_projection(), main_transform);

		this->render_entity();

		Renderer2D::end_scene();

		this->on_physics_update(timestep);
	}

	void Scene::on_update_simulation([[maybe_unused]] EditorCamera &camera, float timestep) {
		Renderer2D::begin_scene(camera);

		this->render_entity();

		Renderer2D::end_scene();

		this->on_physics_update(timestep);
	}

	void Scene::on_runtime_start(void) {
		this->on_physics_start();
	}

	void Scene::on_runtime_stop(void) {
		this->on_physics_stop();
	}

	void Scene::on_simulation_start(void) {
		this->on_physics_start();
	}

	void Scene::on_simulation_stop() {
		this->on_physics_stop();
	}

	void Scene::on_physics_start(void) {
		this->world = new b2World({ 0.0f, -9.8f });
		auto view = this->registry.view<RigidBody2DComponent>();

		for (auto e : view) {
			Entity entity = { e, this };
			auto &transform = entity.get<TransformComponent>();
			auto &rb2d = entity.get<RigidBody2DComponent>();

			b2BodyDef def;
			def.type = bodytype_to_box2d(rb2d.type);

			def.fixedRotation = rb2d.fixed_rotation;

			def.position.Set(transform.translation.x, transform.translation.y);
			def.angle = transform.rotation.z;

			b2Body *body = this->world->CreateBody(&def);
			rb2d.runtime_body = body;

			if (entity.has<BoxCollider2DComponent>()) {
				auto &bc2d = entity.get<BoxCollider2DComponent>();
				b2PolygonShape shape;
				shape.SetAsBox(transform.scale.x * bc2d.size.x, transform.scale.y * bc2d.size.y);

				b2FixtureDef fixture_def;
				fixture_def.shape = &shape;
				fixture_def.density = bc2d.density;
				fixture_def.friction = bc2d.friction;
				fixture_def.restitution = bc2d.restitution;
				fixture_def.restitutionThreshold = bc2d.restitution_threshold;

				bc2d.runtime_fixture = body->CreateFixture(&fixture_def);
			} else if (entity.has<CircleCollider2DComponent>()) {
				auto &bc2d = entity.get<CircleCollider2DComponent>();
				b2CircleShape shape;
				shape.m_p.Set(bc2d.offset.x, bc2d.offset.y);
				shape.m_radius = transform.scale.x * bc2d.radius;

				b2FixtureDef fixture_def;
				fixture_def.shape = &shape;
				fixture_def.density = bc2d.density;
				fixture_def.friction = bc2d.friction;
				fixture_def.restitution = bc2d.restitution;
				fixture_def.restitutionThreshold = bc2d.restitution_threshold;

				bc2d.runtime_fixture = body->CreateFixture(&fixture_def);
			}
		}
	}

	void Scene::on_physics_stop(void) {
		delete this->world;
		this->world = nullptr;
	}

	void Scene::on_physics_update(float timestep) {
		// Physics
		{
			const int32_t velocity_iterations = 6;
			const int32_t position_iterations = 2;
			this->world->Step(timestep, velocity_iterations, position_iterations);

			auto view = this->registry.view<RigidBody2DComponent>();
			for (auto e : view) {
				Entity entity = { e, this };
				auto &transform = entity.get<TransformComponent>();
				auto &rb2d = entity.get<RigidBody2DComponent>();

				b2Body *body = static_cast<b2Body *>(rb2d.runtime_body);
				const auto &position = body->GetPosition();
				transform.translation.x = position.x;
				transform.translation.y = position.y;
				transform.rotation.z = body->GetAngle();
			}
		}
	}

	void Scene::render_entity(void) {
		{
			auto group = this->registry.group<TransformComponent, SpriteRendererComponent>();
			for (auto entity : group) {
				auto [transform, sprite] = group.get<TransformComponent, SpriteRendererComponent>(entity);

				Renderer2D::draw_sprite(transform, sprite, (int)entity);
			}
		}

		{
			auto view = this->registry.view<TransformComponent, CircleRendererComponent>();
			for (auto entity : view) {
				auto [transform, circle] = view.get<TransformComponent, CircleRendererComponent>(entity);

				Renderer2D::draw_circle(transform, circle.color, circle.thickness, circle.fade, (int)entity);
			}
		}
	}

	Entity Scene::add(const std::string &name, bool with_transform) {
		return this->add(UUID(), name, with_transform);
	}

	Entity Scene::add(UUID uuid, const std::string &name, bool with_transform) {
		Entity entity = { this->registry.create(), this };

		entity.add<IDComponent>(uuid);
		entity.add<TagComponent>(name.empty() ? "Entity" : name);

		if (with_transform)
			entity.add<TransformComponent>();

		return entity;
	}

	void Scene::remove(Entity entity) {
		this->registry.destroy(entity);
	}

	void Scene::duplicate(Entity entity) {
		Entity new_entity = this->add(entity.get_name());

		copy_component_if_exist(AllComponents{}, new_entity, entity);
	}

	void Scene::on_viewport_resize(uint32_t width, uint32_t height) {
		this->viewport_width = width;
		this->viewport_height = height;

		auto view = this->registry.view<CameraComponent>();
		for (auto entity : view) {
			auto &camera = view.get<CameraComponent>(entity);

			if (!camera.fixed_aspect_ratio) {
				camera.camera.set_viewport_size(width, height);
			}
		}
	}

	Entity Scene::get_primary_camera_entity(void) {
		auto view = this->registry.view<CameraComponent>();
		for (auto entity : view) {
			const auto &camera = view.get<CameraComponent>(entity);
			if (camera.primary)
				return Entity{ entity, this };
		}

		return {};
	}

	template <typename T>
	void Scene::on_component_added([[maybe_unused]] Entity &entity, [[maybe_unused]] T &component) {
		static_assert(sizeof(T) == 0);
	}

	template <>
	void Scene::on_component_added<CameraComponent>([[maybe_unused]] Entity &entity, [[maybe_unused]] CameraComponent &component) {
		component.camera.set_viewport_size(this->viewport_width, this->viewport_height);
	}

	template <>
	void Scene::on_component_added<TransformComponent>([[maybe_unused]] Entity &entity, [[maybe_unused]] TransformComponent &component) {
	}

	template <>
	void Scene::on_component_added<SpriteRendererComponent>([[maybe_unused]] Entity &entity, [[maybe_unused]] SpriteRendererComponent &component) {
	}

	template <>
	void Scene::on_component_added<CircleRendererComponent>([[maybe_unused]] Entity &entity, [[maybe_unused]] CircleRendererComponent &component) {
	}

	template <>
	void Scene::on_component_added<NativeScriptComponent>([[maybe_unused]] Entity &entity, [[maybe_unused]] NativeScriptComponent &component) {
	}

	template <>
	void Scene::on_component_added<RigidBody2DComponent>([[maybe_unused]] Entity &entity, [[maybe_unused]] RigidBody2DComponent &component) {
	}

	template <>
	void Scene::on_component_added<BoxCollider2DComponent>([[maybe_unused]] Entity &entity, [[maybe_unused]] BoxCollider2DComponent &component) {
	}

	template <>
	void Scene::on_component_added<CircleCollider2DComponent>([[maybe_unused]] Entity &entity, [[maybe_unused]] CircleCollider2DComponent &component) {
	}

	template <>
	void Scene::on_component_added<IDComponent>([[maybe_unused]] Entity &entity, [[maybe_unused]] IDComponent &component) {
	}

	template <>
	void Scene::on_component_added<TagComponent>([[maybe_unused]] Entity &entity, [[maybe_unused]] TagComponent &component) {
	}
} // namespace Solace
