#include "Solace/spch.h"

#include <glm/gtc/matrix_transform.hpp>

#include "Solace/scene/scene_camera.h"

namespace Solace {
	SceneCamera::SceneCamera(void) {
		this->recalculate_projection();
	}

	SceneCamera::~SceneCamera(void) {
	}

	void SceneCamera::set_orthographic(float size, float near_clip, float far_clip) {
		this->orthographic_size = size;
		this->orthographic_near = near_clip;
		this->orthographic_far = far_clip;

		this->type = ProjectionType::Orthographic;

		this->recalculate_projection();
	}

	void SceneCamera::set_perspective(float fov, float near_clip, float far_clip) {
		this->perspective_fov = fov;
		this->perspective_near = near_clip;
		this->perspective_far = far_clip;

		this->type = ProjectionType::Perspective;

		this->recalculate_projection();
	}

	void SceneCamera::set_viewport_size(uint32_t width, uint32_t height) {
		this->aspect_ratio = (float)width / (float)height;

		this->recalculate_projection();
	}

	void SceneCamera::recalculate_projection(void) {
		switch (this->type) {
		case ProjectionType::Orthographic: {
			float ortho_left = -this->orthographic_size * this->aspect_ratio * 0.5f;
			float ortho_right = this->orthographic_size * this->aspect_ratio * 0.5f;
			float ortho_bottom = -this->orthographic_size * 0.5f;
			float ortho_top = this->orthographic_size * 0.5f;

			this->projection = glm::ortho(ortho_left, ortho_right, ortho_bottom, ortho_top, this->orthographic_near, this->orthographic_far);
		} break;

		case ProjectionType::Perspective:
			this->projection = glm::perspective(this->perspective_fov, this->aspect_ratio, this->perspective_near, this->perspective_far);
			break;
		}
	}
} // namespace Solace
