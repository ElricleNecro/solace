#include "Solace/spch.h"

#include "Solace/scene/components.h"
#include "Solace/scene/entity.h"
#include "Solace/scene/scene_serialiser.h"

#include <yaml-cpp/yaml.h>
#include <fstream>

namespace YAML {
	template <>
	struct convert<Solace::RigidBody2DComponent::BodyType> {
		static Node encode(const Solace::RigidBody2DComponent::BodyType &rhs) {
			Node node;

			switch (rhs) {
			case Solace::RigidBody2DComponent::BodyType::Static:
				node.push_back("static");
				break;
			case Solace::RigidBody2DComponent::BodyType::Dynamic:
				node.push_back("dynamic");
				break;
			case Solace::RigidBody2DComponent::BodyType::Kinematic:
				node.push_back("kinematic");
				break;
			default:
				SOLACE_ASSERT(false, "Unknown body type.");
				node.push_back("unknown");
				break;
			}

			node.SetStyle(EmitterStyle::Flow);

			return node;
		}

		static bool decode(const Node &node, Solace::RigidBody2DComponent::BodyType &rhs) {
			if (!node.IsScalar())
				return false;

			auto name = node.as<std::string>();
			if (name == "static") {
				rhs = Solace::RigidBody2DComponent::BodyType::Static;
			} else if (name == "dynamic") {
				rhs = Solace::RigidBody2DComponent::BodyType::Dynamic;
			} else if (name == "kinematic") {
				rhs = Solace::RigidBody2DComponent::BodyType::Kinematic;
			} else {
				SOLACE_ASSERT(false, "Unknown body type!");
			}

			return true;
		}
	};
	template <>
	struct convert<glm::vec2> {
		static Node encode(const glm::vec2 &rhs) {
			Node node;

			node.push_back(rhs.x);
			node.push_back(rhs.y);

			node.SetStyle(EmitterStyle::Flow);

			return node;
		}

		static bool decode(const Node &node, glm::vec2 &rhs) {
			if (!node.IsSequence() || node.size() != 2)
				return false;

			rhs.x = node[0].as<float>();
			rhs.y = node[1].as<float>();

			return true;
		}
	};

	template <>
	struct convert<glm::vec3> {
		static Node encode(const glm::vec3 &rhs) {
			Node node;

			node.push_back(rhs.x);
			node.push_back(rhs.y);
			node.push_back(rhs.z);

			node.SetStyle(EmitterStyle::Flow);

			return node;
		}

		static bool decode(const Node &node, glm::vec3 &rhs) {
			if (!node.IsSequence() || node.size() != 3)
				return false;

			rhs.x = node[0].as<float>();
			rhs.y = node[1].as<float>();
			rhs.z = node[2].as<float>();

			return true;
		}
	};

	template <>
	struct convert<glm::vec4> {
		static Node encode(const glm::vec4 &rhs) {
			Node node;

			node.push_back(rhs.x);
			node.push_back(rhs.y);
			node.push_back(rhs.z);
			node.push_back(rhs.w);

			node.SetStyle(EmitterStyle::Flow);

			return node;
		}

		static bool decode(const Node &node, glm::vec4 &rhs) {
			if (!node.IsSequence() || node.size() != 4)
				return false;

			rhs.x = node[0].as<float>();
			rhs.y = node[1].as<float>();
			rhs.z = node[2].as<float>();
			rhs.w = node[3].as<float>();

			return true;
		}
	};
} // namespace YAML

namespace Solace {
	YAML::Emitter &operator<<(YAML::Emitter &out, const glm::vec2 &v) {
		out << YAML::Flow;
		out << YAML::BeginSeq << v.x << v.y << YAML::EndSeq;

		return out;
	}

	YAML::Emitter &operator<<(YAML::Emitter &out, const glm::vec3 &v) {
		out << YAML::Flow;
		out << YAML::BeginSeq << v.x << v.y << v.z << YAML::EndSeq;

		return out;
	}

	YAML::Emitter &operator<<(YAML::Emitter &out, const glm::vec4 &v) {
		out << YAML::Flow;
		out << YAML::BeginSeq << v.x << v.y << v.z << v.w << YAML::EndSeq;

		return out;
	}

	YAML::Emitter &operator<<(YAML::Emitter &out, const RigidBody2DComponent::BodyType &v) {
		out << YAML::Flow;
		switch (v) {
		case RigidBody2DComponent::BodyType::Static:
			out << "static";
			break;
		case RigidBody2DComponent::BodyType::Dynamic:
			out << "dynamic";
			break;
		case RigidBody2DComponent::BodyType::Kinematic:
			out << "kinematic";
			break;
		default:
			SOLACE_ASSERT(false, "Unknown body type.");
			out << "unknown";
			break;
		}

		return out;
	}

	SceneSerialiser::SceneSerialiser(const Ref<Scene> &scene) : scene(scene) {
	}

	static void serialise_entity(YAML::Emitter &out, Entity entity) {
		SOLACE_CORE_ASSERT(entity.has<IDComponent>(), "Entity without ID are serialisable.");
		out << YAML::BeginMap;
		out << YAML::Key << "Entity" << YAML::Value << entity.get<IDComponent>().id;

		if (entity.has<TagComponent>()) {
			out << YAML::Key << "TagComponent" << YAML::Value << YAML::BeginMap;

			auto &tag = entity.get<TagComponent>().tag;

			out << YAML::Key << "tag" << YAML::Value << tag;

			out << YAML::EndMap;
		}

		if (entity.has<TransformComponent>()) {
			out << YAML::Key << "TransformComponent" << YAML::Value << YAML::BeginMap;

			auto &tc = entity.get<TransformComponent>();

			out << YAML::Key << "translation" << YAML::Value << tc.translation;
			out << YAML::Key << "rotation" << YAML::Value << tc.rotation;
			out << YAML::Key << "scale" << YAML::Value << tc.scale;

			out << YAML::EndMap;
		}

		if (entity.has<CameraComponent>()) {
			out << YAML::Key << "CameraComponent" << YAML::Value << YAML::BeginMap;

			auto &cc = entity.get<CameraComponent>();
			auto &camera = cc.camera;

			out << YAML::Key << "camera" << YAML::Value;
			out << YAML::BeginMap;
			out << YAML::Key << "projection_type" << YAML::Value << (size_t)camera;

			out << YAML::Key << "perspective_fov" << YAML::Value << camera.get_perspective_fov();
			out << YAML::Key << "perspective_near" << YAML::Value << camera.get_perspective_near();
			out << YAML::Key << "perspective_far" << YAML::Value << camera.get_perspective_far();

			out << YAML::Key << "orthographic_size" << YAML::Value << camera.get_orthographic_size();
			out << YAML::Key << "orthographic_near" << YAML::Value << camera.get_orthographic_near();
			out << YAML::Key << "orthographic_far" << YAML::Value << camera.get_orthographic_far();
			out << YAML::EndMap;

			out << YAML::Key << "primary" << YAML::Value << cc.primary;
			out << YAML::Key << "fixed_aspect_ratio" << YAML::Value << cc.fixed_aspect_ratio;

			out << YAML::EndMap;
		}

		if (entity.has<SpriteRendererComponent>()) {
			out << YAML::Key << "SpriteRendererComponent" << YAML::Value << YAML::BeginMap;

			auto &sprite = entity.get<SpriteRendererComponent>();

			out << YAML::Key << "color" << YAML::Value << sprite.color;
			if( sprite.texture )
				out << YAML::Key << "texture_path" << YAML::Value << sprite.texture->get_path();
			out << YAML::Key << "tiling_factor" << YAML::Value << sprite.tiling_factor;

			out << YAML::EndMap;
		}

		if (entity.has<CircleRendererComponent>()) {
			out << YAML::Key << "CircleRendererComponent" << YAML::Value << YAML::BeginMap;

			auto &circle = entity.get<CircleRendererComponent>();

			out << YAML::Key << "color" << YAML::Value << circle.color;
			out << YAML::Key << "radius" << YAML::Value << circle.radius;
			out << YAML::Key << "thickness" << YAML::Value << circle.thickness;
			out << YAML::Key << "fade" << YAML::Value << circle.fade;

			out << YAML::EndMap;
		}

		if (entity.has<RigidBody2DComponent>()) {
			out << YAML::Key << "RigidBody2DComponent" << YAML::Value << YAML::BeginMap;

			auto &body = entity.get<RigidBody2DComponent>();

			out << YAML::Key << "body_type" << YAML::Value << body.type;
			out << YAML::Key << "fixed_rotation" << YAML::Value << body.fixed_rotation;

			out << YAML::EndMap;
		}

		if (entity.has<BoxCollider2DComponent>()) {
			out << YAML::Key << "BoxCollider2DComponent" << YAML::Value << YAML::BeginMap;

			auto &body = entity.get<BoxCollider2DComponent>();

			out << YAML::Key << "offset" << YAML::Value << body.offset;
			out << YAML::Key << "size" << YAML::Value << body.size;

			out << YAML::Key << "density" << YAML::Value << body.density;
			out << YAML::Key << "friction" << YAML::Value << body.friction;
			out << YAML::Key << "restitution" << YAML::Value << body.restitution;
			out << YAML::Key << "restitution_threshold" << YAML::Value << body.restitution_threshold;

			out << YAML::EndMap;
		}

		if (entity.has<CircleCollider2DComponent>()) {
			out << YAML::Key << "CircleCollider2DComponent" << YAML::Value << YAML::BeginMap;

			auto &body = entity.get<CircleCollider2DComponent>();

			out << YAML::Key << "offset" << YAML::Value << body.offset;
			out << YAML::Key << "radius" << YAML::Value << body.radius;

			out << YAML::Key << "density" << YAML::Value << body.density;
			out << YAML::Key << "friction" << YAML::Value << body.friction;
			out << YAML::Key << "restitution" << YAML::Value << body.restitution;
			out << YAML::Key << "restitution_threshold" << YAML::Value << body.restitution_threshold;

			out << YAML::EndMap;
		}

		out << YAML::EndMap;
	}

	void SceneSerialiser::serialise(const std::string &filepath) {
		YAML::Emitter out;
		out << YAML::BeginMap;
		out << YAML::Key << "Scene" << YAML::Value << "Untitled";
		out << YAML::Key << "Entities" << YAML::Value << YAML::BeginSeq;
		this->scene->registry.each([&](auto entity_id) {
			Entity entity = { entity_id, this->scene.get() };
			if (!entity)
				return;

			serialise_entity(out, entity);
		});

		out << YAML::EndSeq;
		out << YAML::EndMap;

		std::ofstream fout(filepath);
		fout << out.c_str();
	}

	void SceneSerialiser::serialise_runtime([[maybe_unused]] const std::string &filepath) {
		SOLACE_CORE_ASSERT(false, "Runtime serialisation not implemented yet!");
	}

	bool SceneSerialiser::deserialise(const std::string &filepath) {
		YAML::Node data;

		try {
			data = YAML::LoadFile(filepath);
		} catch(YAML::ParserException &e) {
			SL_CORE_ERROR("Failed to load file '{0}': {1}.\n", filepath, e.what());
			return false;
		}

		if (!data["Scene"])
			return false;

		std::string scene_name = data["Scene"].as<std::string>();

		auto entities = data["Entities"];
		if (entities) {
			for (auto entity : entities) {
				uint64_t uuid = entity["Entity"].as<uint64_t>();

				std::string name;
				auto tag = entity["TagComponent"];
				if (tag)
					name = tag["tag"].as<std::string>();

				Entity deserialised_entity = this->scene->add(uuid, name);

				auto transform = entity["TransformComponent"];
				if (transform) {
					// All entites have transform:
					auto &tc = deserialised_entity.get<TransformComponent>();
					tc.translation = transform["translation"].as<glm::vec3>();
					tc.rotation = transform["rotation"].as<glm::vec3>();
					tc.scale = transform["scale"].as<glm::vec3>();
				}

				auto camera = entity["CameraComponent"];
				if (camera) {
					// All entites have transform:
					auto &cc = deserialised_entity.add<CameraComponent>();
					const auto &props = camera["camera"];

					cc.camera.set_type(props["projection_type"].as<size_t>());

					cc.camera.set_perspective_fov(props["perspective_fov"].as<float>());
					cc.camera.set_perspective_far(props["perspective_far"].as<float>());
					cc.camera.set_perspective_near(props["perspective_near"].as<float>());

					cc.camera.set_orthographic_size(props["orthographic_size"].as<float>());
					cc.camera.set_orthographic_far(props["orthographic_far"].as<float>());
					cc.camera.set_orthographic_near(props["orthographic_near"].as<float>());

					cc.primary = camera["primary"].as<bool>();
					cc.fixed_aspect_ratio = camera["fixed_aspect_ratio"].as<bool>();
				}

				auto sprite = entity["SpriteRendererComponent"];
				if (sprite) {
					// All entites have transform:
					auto &sc = deserialised_entity.add<SpriteRendererComponent>();
					sc.color = sprite["color"].as<glm::vec4>();
					sc.tiling_factor = sprite["tiling_factor"].as<float>();

					if( sprite["texture_path"] ) {
						std::string path = sprite["texture_path"].as<std::string>();
						if( !path.empty() )
							sc.texture = Texture2D::create(path);
					}
				}

				auto circle = entity["CircleRendererComponent"];
				if (circle) {
					// All entites have transform:
					auto &cc = deserialised_entity.add<CircleRendererComponent>();
					cc.color = circle["color"].as<glm::vec4>();
					cc.radius = circle["radius"].as<float>();
					cc.thickness = circle["thickness"].as<float>();
					cc.fade = circle["fade"].as<float>();
				}

				auto rigid_body = entity["RigidBody2DComponent"];
				if (rigid_body) {
					// All entites have transform:
					auto &rb2d = deserialised_entity.add<RigidBody2DComponent>();
					rb2d.type = rigid_body["body_type"].as<RigidBody2DComponent::BodyType>();
					rb2d.fixed_rotation = rigid_body["fixed_rotation"].as<bool>();
				}

				auto box_col = entity["BoxCollider2DComponent"];
				if (box_col) {
					// All entites have transform:
					auto &box = deserialised_entity.add<BoxCollider2DComponent>();

					box.offset = box_col["offset"].as<glm::vec2>();
					box.size = box_col["size"].as<glm::vec2>();

					box.density = box_col["density"].as<float>();
					box.friction = box_col["friction"].as<float>();
					box.restitution = box_col["restitution"].as<float>();
					box.restitution_threshold = box_col["restitution_threshold"].as<float>();
				}

				auto circle_col = entity["CircleCollider2DComponent"];
				if (circle_col) {
					// All entites have transform:
					auto &circle = deserialised_entity.add<CircleCollider2DComponent>();

					circle.offset = circle_col["offset"].as<glm::vec2>();
					circle.radius = circle_col["radius"].as<float>();

					circle.density = circle_col["density"].as<float>();
					circle.friction = circle_col["friction"].as<float>();
					circle.restitution = circle_col["restitution"].as<float>();
					circle.restitution_threshold = circle_col["restitution_threshold"].as<float>();
				}
			}
		}

		return true;
	}

	bool SceneSerialiser::deserialise_runtime([[maybe_unused]] const std::string &filepath) {
		SOLACE_CORE_ASSERT(false, "Runtime deserialisation not implemented yet!");
		return false;
	}
} // namespace Solace
