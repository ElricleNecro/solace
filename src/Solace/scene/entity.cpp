#include "Solace/spch.h"

#include "Solace/scene/entity.h"

namespace Solace {
	Entity::Entity(entt::entity handle, Scene *scene) : handle(handle), scene(scene) {
	}
} // namespace Solace
