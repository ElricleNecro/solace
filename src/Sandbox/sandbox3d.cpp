#include "Sandbox/sandbox3d.h"

void SandBox3D::on_attach(void) {
	this->library.load("assets/shaders/basic.glsl");
	this->library.load("assets/shaders/color.glsl");
	this->library.load("assets/shaders/texture.glsl");

	this->texture = Solace::Texture2D::create("assets/textures/Checkerboard.png");
	this->logo_tex = Solace::Texture2D::create("assets/textures/ChernoLogo.png");

	this->library.get("texture")->bind();
	this->library.get("texture")->set_int("u_texture", 0);
	this->library.get("texture")->unbind();

	// clang-format off
	static const std::array<float, 3 * 7> vertex = {
		-0.5, -0.5, 0.0,  0.8f, 0.2f, 0.8f, 1.0f,
		 0.5, -0.5, 0.0,  0.2f, 0.2f, 0.8f, 1.0f,
		 0.0,  0.5, 0.0,  0.8f, 0.8f, 0.2f, 1.0f,
	};
	// clang-format on
	static const std::array<uint32_t, 3> indices = { 0, 1, 2 };

	Solace::Ref<Solace::VertexBuffer> vbo = Solace::VertexBuffer::create(vertex.data(), vertex.size() * sizeof(float));
	Solace::Ref<Solace::IndexBuffer> vio = Solace::IndexBuffer::create(indices.data(), indices.size());

	this->vao = Solace::VertexArray::create();

	{
		Solace::BufferLayout layout{
			{ Solace::ShaderDataType::Float3, "position" },
			{ Solace::ShaderDataType::Float4, "color" },
		};
		vbo->set_layout(layout);
	}

	this->vao->add_vertex_buffer(vbo);
	this->vao->set_index_buffer(vio);

	this->square_vao = Solace::VertexArray::create();
	// clang-format off
	static const std::array<float, 4 * 5> square_vertex = {
		-0.5, -0.5, 0.0, 0.0f, 0.0f,
		 0.5, -0.5, 0.0, 1.0f, 0.0f,
		 0.5, 0.5,  0.0, 1.0f, 1.0f,
		-0.5, 0.5,  0.0, 0.0f, 1.0f,
	};
	// clang-format on
	Solace::Ref<Solace::VertexBuffer> square_vbo = Solace::VertexBuffer::create(square_vertex.data(), square_vertex.size() * sizeof(float));
	square_vbo->set_layout({
		{ Solace::ShaderDataType::Float3, "position" },
		{ Solace::ShaderDataType::Float2, "tex_coord" },
	});

	static const std::array<uint32_t, 6> square_indices = { 0, 1, 2, 2, 3, 0 };
	Solace::Ref<Solace::IndexBuffer> square_vio = Solace::IndexBuffer::create(square_indices.data(), square_indices.size());

	this->square_vao->add_vertex_buffer(square_vbo);
	this->square_vao->set_index_buffer(square_vio);
}

void SandBox3D::on_update(float elapsed_time) {
	this->camera_controller.on_update(elapsed_time);

	if (Solace::Application::get().get_events()[SDL_SCANCODE_H])
		this->square_position.x -= this->square_speed * elapsed_time;

	if (Solace::Application::get().get_events()[SDL_SCANCODE_L])
		this->square_position.x += this->square_speed * elapsed_time;

	if (Solace::Application::get().get_events()[SDL_SCANCODE_J])
		this->square_position.y -= this->square_speed * elapsed_time;

	if (Solace::Application::get().get_events()[SDL_SCANCODE_K])
		this->square_position.y += this->square_speed * elapsed_time;

	Solace::RenderCommand::set_clear_color({ this->clear_color.x, this->clear_color.y, this->clear_color.z, this->clear_color.w });
	Solace::RenderCommand::clear();

	Solace::Renderer::begin_scene(this->camera_controller);

	static glm::mat4 scale = glm::scale(glm::mat4(1.0f), glm::vec3(0.1f));

	for (int j = 0; j < 20; j++) {
		for (int i = 0; i < 20; i++) {
			glm::vec3 pos(i * 0.11f, j * 0.11f, 0.0f);
			glm::mat4 transform = glm::translate(glm::mat4(1.0f), pos) * scale;

			if (i % 2 == 0)
				this->library.get("color")->set_float("u_color", this->odd);
			else
				this->library.get("color")->set_float("u_color", this->even);

			Solace::Renderer::submit(this->library.get("color"), this->square_vao, transform);
		}
	}

	this->texture->bind();
	Solace::Renderer::submit(this->library.get("texture"), this->square_vao, glm::scale(glm::mat4(1.0f), glm::vec3(1.5f)));

	this->logo_tex->bind();
	Solace::Renderer::submit(this->library.get("texture"), this->square_vao, glm::scale(glm::mat4(1.0f), glm::vec3(1.5f)));

	Solace::Renderer::submit(this->library.get("basic"),
				 this->vao,
				 glm::translate(glm::mat4(1.0f), glm::vec3(2.5f, 0.0f, 0.0f)) * glm::scale(glm::mat4(1.0f), glm::vec3(0.5f)));

	Solace::Renderer::end_scene();
}

void SandBox3D::on_imgui_render(void) {
	ImGui::Begin("Settings");
	ImGui::Text("Test"); // Display some text (you can use a format strings too)

	ImGui::ColorEdit4("clear color",
			  glm::value_ptr(this->clear_color)); // Edit 3 floats representing a color
	ImGui::ColorEdit3("even square",
			  glm::value_ptr(this->even)); // Edit 3 floats representing a color
	ImGui::ColorEdit3("odd square",
			  glm::value_ptr(this->odd)); // Edit 3 floats representing a color

	ImGui::Text("Solace::Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::End();
}

bool SandBox3D::on_event(Solace::Events &evt) {
	this->camera_controller.on_event(evt);

	if (evt[SDL_SCANCODE_A] && evt[SDL_SCANCODE_LCTRL]) {
		Solace::Application::get().quit();
	}

	return false;
}

void SandBox3D::on_detach(void) {
	this->vao.reset();
	this->square_vao.reset();
}
