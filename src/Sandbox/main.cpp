#include <memory>
#include <string>

#include "Solace/core/entrypoint.h"
#include "Solace/spch.h"

#include "Sandbox/sandbox2d.h"
#include "Sandbox/sandbox3d.h"

class Sandbox : public Solace::Application {
public:
	Sandbox(const Solace::ApplicationSpecification &spec) : Application(spec) {
		this->push_overlay(new SandBox2D(this->get_width(), this->get_height()));
	}
};

Solace::Application *Solace::create_application(CLIArgs args) {
	ApplicationSpecification spec;
	spec.cli_args = args;

	return new Sandbox(spec);
}
