#include "Sandbox/sandbox2d.h"

#include <chrono>
#include <cmath>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "imgui/imgui.h"

#include "Solace/core/application.h"
#include "Solace/debug/instrumentor.h"

static const uint32_t MAP_WIDTH = 24;
static const char *MAP_TILES = "WWWWWWWWWWWWWWWWWWWWWWWW"
			       "WWWWWWWDDDDDDDDDWWWWWWWW"
			       "WWWWWDDDDDDDDDDDDDWWWWWW"
			       "WWWWDDDDDDDDDDDDDDDWWWWW"
			       "WWWDDDDDDDDDDDDDDDDDWWWW"
			       "WWDDDDWWDDDDDDDDDDCDDWWW"
			       "WDDDDWWWDDDDDDDDDDDDDDWW"
			       "WWDDDDWWDDDDDDDDDDDDDDWW"
			       "WWWWWDDDDDDDDDDDDDDDDWWW"
			       "WWWWWWDDDDDDDDDDDDDWWWWW"
			       "WWWWWWWDDDDDDDDDDDWWWWWW"
			       "WWWWWWWWWWDDDDDDWWWWWWWW"
			       "WWWWWWWWWWWWWWWWWWWWWWWW";

SandBox2D::SandBox2D(int width, int height) : Solace::Layer("SandBox2D"), camera_controller(width / height, true) {
}

void SandBox2D::on_attach(void) {
	SL_PROFILE_FUNCTION();
	this->texture = Solace::Texture2D::create("assets/textures/Checkerboard.png");
	this->cherno = Solace::Texture2D::create("assets/textures/ChernoLogo.png");
	this->sprite_cheet = Solace::Texture2D::create("assets/game/textures/rpgpack_sheet_2x.png");

	Solace::FrameBufferSpecification spec;
	spec.width = Solace::Application::get().get_width();
	spec.height = Solace::Application::get().get_height();
	this->framebuffer = Solace::FrameBuffer::create(spec);

	this->map_width = MAP_WIDTH;
	this->map_height = strlen(MAP_TILES) / MAP_WIDTH;

	this->tex_map['D'] = Solace::SubTexture2D::create_from_coordinates(this->sprite_cheet, { 6, 11 }, { 128, 128 });
	this->tex_map['W'] = Solace::SubTexture2D::create_from_coordinates(this->sprite_cheet, { 11, 11 }, { 128, 128 });

	this->stairs = Solace::SubTexture2D::create_from_coordinates(this->sprite_cheet, { 7, 6 }, { 128, 128 });
	this->barrel = Solace::SubTexture2D::create_from_coordinates(this->sprite_cheet, { 8, 2 }, { 128, 128 });
	this->tree = Solace::SubTexture2D::create_from_coordinates(this->sprite_cheet, { 2, 1 }, { 128, 128 }, { 1, 2 });

	this->particle.color_begin = { 0.4f, .8f, .7f, 1.0f };
	this->particle.color_end = { 0.9f, 0.3f, 0.1f, 1.0f };
	this->particle.size_begin = 0.3f, this->particle.size_variation = 0.1f, this->particle.size_end = 0.0f;
	this->particle.life_time = 5.0f;
	this->particle.velocity = { 0.0f, 0.0f };
	this->particle.velocity_variation = { 3.0f, 1.0f };
	this->particle.position = { 0.0f, 0.0f };

	this->camera_controller.set_zoom_level(5.f);
}

void SandBox2D::on_update(float elapsed_time) {
	SL_PROFILE_FUNCTION();

	this->camera_controller.on_update(elapsed_time);

#ifdef SL_PROFILE
	Solace::Renderer2D::ResetStats();
#endif
	{
		SL_PROFILE_SCOPE("Renderer preparation");
		this->framebuffer->bind();
		Solace::RenderCommand::set_clear_color({ this->clear_color.x, this->clear_color.y, this->clear_color.z, this->clear_color.w });
		Solace::RenderCommand::clear();
	}

	{
		static const float radius = 5.f;
		this->rotation += elapsed_time * .5f;
		if (this->rotation > (2.f * (float)M_PI))
			this->rotation -= 2.f * (float)M_PI;

		this->curve += elapsed_time * .25f;
		if (this->curve > (2.f * (float)M_PI))
			this->curve -= 2.f * (float)M_PI;

		SL_PROFILE_SCOPE("Renderer draw");
		Solace::Renderer2D::begin_scene(this->camera_controller);

		Solace::Renderer2D::draw_quad({ 0.0f, 0.0f, -0.1f }, { 20.0f, 20.0f }, 0.0f, this->texture, 10.0f);
		Solace::Renderer2D::draw_quad({ -1.f, 0.f }, { 1.f, 1.f }, 3.f * M_PI / 2.f, this->even);
		Solace::Renderer2D::draw_quad({ 1.f, 1.f }, { 2.f, 1.f }, this->rotation, this->even);
		Solace::Renderer2D::draw_quad({ 0.f, 0.5f }, { 1.f, 1.75f }, 0.0f, { 0.f, 0.3f, 1.0f, 1.0f });
		Solace::Renderer2D::draw_quad({ radius * std::cos(this->curve), radius * std::sin(this->curve), 0.f },
					      { 1.0f, 1.0f },
					      this->rotation,
					      this->cherno,
					      { 0.6f, 0.3f, 0.1, 1.0f });

		for (float y = -5.0f; y < 5.0f; y += 0.5f) {
			for (float x = -5.0f; x < 5.0f; x += 0.5f) {
				glm::vec4 color = { (x + 5.0f) / 10.f, 0.4f, (y + 5.0f) / 10.0f, 0.5f };
				Solace::Renderer2D::draw_quad({ x, y }, { 0.45f, 0.45f }, 0.f, color);
			}
		}

		Solace::Renderer2D::end_scene();
	}

	if (!Solace::Application::get().get_events().used() && Solace::Application::get().get_events().mouse()[SDL_BUTTON_LEFT]) {
		auto [x, y] = Solace::Application::get_events_data().mouse_position();
		auto [width, height] = Solace::Application::get_events_data().size();

		auto bounds = this->camera_controller.get_bounds();
		auto pos = this->camera_controller.get_camera().get_position();

		float pos_x = (x / (float)width) * bounds.width() - bounds.width() * 0.5f;
		float pos_y = bounds.height() * 0.5f - (y / (float)height) * bounds.height();
		this->particle.position = { pos_x + pos.x, pos_y + pos.y };

		//SL_INFO("Emitting particles at {}, {}.", this->particle.position.x, this->particle.position.y);

		for (int i = 0; i < 5; i++)
			this->particles.emit(this->particle);
	}

	this->particles.on_update(elapsed_time);
	this->particles.on_render(this->camera_controller);

	Solace::Renderer2D::begin_scene(this->camera_controller);

	for (uint32_t y = 0; y < this->map_height; y++) {
		for (uint32_t x = 0; x < this->map_width; x++) {
			const uint32_t idx = y * this->map_width + x;
			const char tile = MAP_TILES[idx];
			Solace::Ref<Solace::SubTexture2D> texture;

			if (this->tex_map.find(tile) != this->tex_map.end())
				texture = this->tex_map[tile];
			else
				texture = this->barrel;

			Solace::Renderer2D::draw_quad({ x - this->map_width / 2.f, this->map_height - y - this->map_height / 2.f, 0.5f }, { 1.f, 1.f }, 0.f, texture);
		}
	}

	Solace::Renderer2D::draw_quad({ 0.f, 0.f, 0.5f }, { 1.f, 1.f }, 0, this->stairs);
	Solace::Renderer2D::draw_quad({ 1.f, 0.f, 0.5f }, { 1.f, 1.f }, 0, this->barrel);
	Solace::Renderer2D::draw_quad({ 1.f, 2.f, 0.5f }, { 1.f, 2.f }, 0, this->tree);
	Solace::Renderer2D::end_scene();
	this->framebuffer->unbind();
}

void SandBox2D::on_imgui_render(void) {
	SL_PROFILE_FUNCTION();
	ImGui::Begin("Settings");
	ImGui::Text("Test"); // Display some text (you can use a format strings too)

	ImGui::ColorEdit4("clear color",
			  glm::value_ptr(this->clear_color)); // Edit 3 floats representing a color
	ImGui::ColorEdit3("Square color",
			  glm::value_ptr(this->even)); // Edit 3 floats representing a color

	ImGui::Text("Solace::Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

	uint64_t tid = this->framebuffer->get_color_attachement_renderer_id();
	ImGui::Image((void *)tid, ImVec2{ 320.0f, 180.0f });
	//ImGui::Image((void *)tid, ImVec2{ (float)this->framebuffer->get_specification().width, (float)this->framebuffer->get_specification().height });

#ifdef SL_PROFILE
	auto stats = Solace::Renderer2D::get_stats();
	ImGui::Text("Draw calls: %d", stats.draw_calls);
	ImGui::Text("Quads: %d", stats.quad_count);

	ImGui::Text("Vertices: %d", stats.get_total_vertex_count());
	ImGui::Text("Indices: %d", stats.get_total_index_count());
#endif

	ImGui::End();
}

bool SandBox2D::on_event(Solace::Events &evt) {
	this->camera_controller.on_event(evt);

	if (evt.resized()) {
		auto size = evt.size();
		this->framebuffer->get_specification().width = std::get<0>(size);
		this->framebuffer->get_specification().height = std::get<1>(size);
		this->framebuffer->invalidate();
	}

	return true;
}

void SandBox2D::on_detach(void) {
	SL_PROFILE_FUNCTION();
}
