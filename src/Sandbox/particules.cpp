#include "particules.h"

#include <glm/gtc/constants.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/compatibility.hpp>

#include <random>

class Random {
public:
	static void init() {
		random_engine.seed(std::random_device()());
	}

	static float as_float() {
		return (float)distribution(random_engine) / (float)distribution.max();
	}

private:
	static std::mt19937 random_engine;
	static std::uniform_int_distribution<std::mt19937::result_type> distribution;
};

std::mt19937 Random::random_engine;
std::uniform_int_distribution<std::mt19937::result_type> Random::distribution;

ParticleSystem::ParticleSystem(size_t size) : pool_index(size - 1) {
	particle_pool.resize(size);
}

void ParticleSystem::on_update(float ts) {
	for (auto &particle : particle_pool) {
		if (!particle.active)
			continue;

		if (particle.life_remaining <= 0.0f) {
			particle.active = false;
			continue;
		}

		particle.life_remaining -= ts;
		particle.position += particle.velocity * (float)ts;
		particle.rotation += 0.01f * ts;
	}
}

void ParticleSystem::on_render([[maybe_unused]] const Solace::OrthographicCamera &camera) {
	Solace::Renderer2D::begin_scene(camera);

	for (auto &particle : particle_pool) {
		if (!particle.active)
			continue;

		// Fade away particles
		float life = particle.life_remaining / particle.life_time;
		glm::vec4 color = glm::lerp(particle.color_end, particle.color_begin, life);
		//color.a = color.a * life;

		float size = glm::lerp(particle.size_end, particle.size_begin, life);

		// Render
		Solace::Renderer2D::draw_quad({ particle.position.x, particle.position.y, 0.2f }, { size, size }, particle.rotation, color);
	}

	Solace::Renderer2D::end_scene();
}

void ParticleSystem::emit(const ParticleProps &particle_props) {
	Particle &particle = particle_pool[pool_index];
	particle.active = true;
	particle.position = particle_props.position;
	particle.rotation = Random::as_float() * 2.0f * glm::pi<float>();

	// Velocity
	particle.velocity = particle_props.velocity;
	particle.velocity.x += particle_props.velocity_variation.x * (Random::as_float() - 0.5f);
	particle.velocity.y += particle_props.velocity_variation.y * (Random::as_float() - 0.5f);

	// Color
	particle.color_begin = particle_props.color_begin;
	particle.color_end = particle_props.color_end;

	particle.life_time = particle_props.life_time;
	particle.life_remaining = particle_props.life_time;
	float rand = Random::as_float() - 0.5f;
	particle.size_begin = particle_props.size_begin + particle_props.size_variation * (rand);
	//SL_INFO("Size is: {} === ({}, {}, {})", particle.size_begin, particle_props.size_begin, particle_props.size_variation, rand);
	particle.size_end = particle_props.size_end;

	pool_index--;
	pool_index = pool_index % particle_pool.size();
}
