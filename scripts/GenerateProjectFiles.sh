#!/usr/bin/env sh

set -e

pushd ../
if [[ "$(uname)" = "Darwin" ]]
then
	.bin/premake5.macos xcode4
else
	.bin/premake5 gmake2
fi
popd
