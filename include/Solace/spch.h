#ifndef __SOLACE_SPCH_PRECOMPILED_HEADER_HPP__
#define __SOLACE_SPCH_PRECOMPILED_HEADER_HPP__

#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <utility>

#include <array>
#include <chrono>
#include <exception>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include <SDL2/SDL.h>

#ifdef USE_GLEW
#include <GL/glew.h>
#elif defined(USE_GLBINDING)
#include <glbinding/gl/gl.h>
#include <glbinding/glbinding.h> // Initialize with glbinding::initialize()
#endif

#include "Solace/debug/instrumentor.h"

//#ifdef S_PLATFORM_WINDOWS
//#include <Windows.h>
//#elif defined(S_PLATFORM_LINUX)
//#endif
#endif
