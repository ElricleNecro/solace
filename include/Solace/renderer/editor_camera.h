#pragma once

#include "Solace/core/events.h"
#include "Solace/renderer/camera.h"

#include <glm/glm.hpp>

namespace Solace {
	class EditorCamera : public Camera {
	public:
		EditorCamera() = default;
		EditorCamera(float fov, float aspect_ratio, float near_clip, float far_clip);

		void on_update(float ts);
		bool on_event(Events &e);

		inline float get_distance() const {
			return this->distance;
		}
		inline void set_distance(float distance) {
			this->distance = distance;
		}

		inline void set_viewport_size(float width, float height) {
			this->viewport_width = width;
			this->viewport_height = height;
			update_projection();
		}

		const glm::mat4 &get_view_matrix() const {
			return this->view_matrix;
		}
		glm::mat4 get_view_projection() const {
			return this->projection * this->view_matrix;
		}

		glm::vec3 get_up_direction() const;
		glm::vec3 get_right_direction() const;
		glm::vec3 get_forward_direction() const;
		const glm::vec3 &GetPosition() const {
			return this->position;
		}
		glm::quat get_orientation() const;

		float get_pitch() const {
			return this->pitch;
		}
		float get_yaw() const {
			return this->yaw;
		}

	private:
		void update_projection();
		void update_view();

		void mouse_pan(const glm::vec2 &delta);
		void mouse_rotate(const glm::vec2 &delta);
		void mouse_zoom(float delta);

		glm::vec3 calculate_position() const;

		std::pair<float, float> pan_speed() const;
		float rotation_speed() const;
		float zoom_speed() const;

	private:
		float fov = 45.0f, aspect_ratio = 1.778f, near_clip = 0.1f, far_clip = 1000.0f;

		glm::mat4 view_matrix;
		glm::vec3 position = { 0.0f, 0.0f, 0.0f };
		glm::vec3 focal_point = { 0.0f, 0.0f, 0.0f };

		glm::vec2 initial_mouse_position = { 0.0f, 0.0f };

		float distance = 10.0f;
		float pitch = 0.0f, yaw = 0.0f;

		float viewport_width = 1280, viewport_height = 720;
	};

} // namespace Solace
