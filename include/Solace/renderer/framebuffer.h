#pragma once

#include "Solace/core/core.h"

namespace Solace {
	enum class FrameBufferTextureFormat {
		None = 0,

		// Color:
		RGBA8,
		RED_INTEGER,

		// Depth/stencil:
		DEPTH24STENCIL8,

		// Defaults:
		Depth = DEPTH24STENCIL8,
	};

	struct FrameBufferTextureSpecification {
		FrameBufferTextureSpecification(void) = default;
		FrameBufferTextureSpecification(FrameBufferTextureFormat format) : texture_format(format) {
		}

		FrameBufferTextureFormat texture_format = FrameBufferTextureFormat::None;
		// TODO: filtering/wrap
	};

	struct FrameBufferAttachmentSpecification {
		FrameBufferAttachmentSpecification(void) = default;
		FrameBufferAttachmentSpecification(const std::initializer_list<FrameBufferTextureSpecification> attachments) : attachments(attachments) {
		}

		std::vector<FrameBufferTextureSpecification> attachments;
	};

	struct FrameBufferSpecification {
		uint32_t width = 0, height = 0;

		FrameBufferAttachmentSpecification attachments;

		uint32_t samples = 1;

		bool swap_chain_target = false;
	};

	class FrameBuffer {
	public:
		virtual ~FrameBuffer(void) = default;

		virtual FrameBufferSpecification &get_specification(void) = 0;
		virtual const FrameBufferSpecification &get_specification(void) const = 0;

		virtual uint32_t get_color_attachement_renderer_id(size_t index = 0) const = 0;
		virtual uint32_t get_depth_buffer_renderer_id(void) const = 0;
		virtual int read_pixel(uint32_t attachment_idx, uint32_t x, uint32_t y) const = 0;

		virtual void clear_color_attachment(size_t index, int value) const = 0;

		virtual void invalidate(void) = 0;

		virtual void resize(uint32_t width, uint32_t height) = 0;

		virtual void bind(void) const = 0;
		virtual void unbind(void) const = 0;

		static Ref<FrameBuffer> create(const FrameBufferSpecification &spec);
	};
} // namespace Solace
