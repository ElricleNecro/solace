#pragma once

#include <glm/glm.hpp>

namespace Solace {
	class Camera {
	public:
		Camera(void) = default;
		Camera(const glm::mat4 &projection) : projection(projection){};
		virtual ~Camera(void) = default;

		inline const glm::mat4 &get_projection(void) const {
			return this->projection;
		}

		inline operator const glm::mat4 &(void) const {
			return this->projection;
		}

	protected:
		glm::mat4 projection = glm::mat4(1.0f);
	};
} // namespace Solace
