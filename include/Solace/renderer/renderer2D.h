#pragma once

#include <glm/glm.hpp>

#include "Solace/orthographic_camera_controller.h"
#include "Solace/renderer/camera.h"
#include "Solace/renderer/editor_camera.h"
#include "Solace/renderer/subtexture2D.h"
#include "Solace/renderer/texture.h"
#include "Solace/scene/components.h"

namespace Solace {
	class Renderer2D {
	public:
		static void init(void);
		static void shutdown(void);

		static void begin_scene(const Camera &camera, const glm::mat4 &transform);
		static void begin_scene(const EditorCamera &camera);
		static void begin_scene(const OrthographicCamera &camera);
		static void end_scene(void);
		static void flush(void);

		// Primitives:
		static inline void draw_quad(const glm::vec2 &position, const glm::vec2 &size, float rotation, const glm::vec4 &color, float tiling_factor = 1.0f) {
			Renderer2D::draw_quad({ position.x, position.y, 0.f }, size, rotation, color, tiling_factor);
		}
		static inline void draw_quad(const glm::vec2 &position, const glm::vec2 &size, float rotation, const Ref<Texture2D> &texture, float tiling_factor = 1.0f) {
			Renderer2D::draw_quad({ position.x, position.y, 0.f }, size, rotation, texture, glm::vec4(1.f), tiling_factor);
		}
		static inline void draw_quad(const glm::vec2 &position, const glm::vec2 &size, float rotation, const Ref<SubTexture2D> &texture, float tiling_factor = 1.0f) {
			Renderer2D::draw_quad({ position.x, position.y, 0.f }, size, rotation, texture, glm::vec4(1.f), tiling_factor);
		}

		static void draw_quad(const glm::vec3 &position, const glm::vec2 &size, float rotation, const glm::vec4 &color, float tiling_factor = 1.0f);
		static inline void draw_quad(const glm::vec3 &position, const glm::vec2 &size, float rotation, const Ref<Texture2D> &texture, float tiling_factor = 1.0f) {
			Renderer2D::draw_quad(position, size, rotation, texture, glm::vec4(1.f), tiling_factor);
		}
		static inline void
		draw_quad(const glm::vec3 &position, const glm::vec2 &size, float rotation, const Ref<Texture2D> &texture, const glm::vec4 &color, float tiling_factor = 1.0f) {
			static const glm::vec2 tex_coords[] = {
				{ 0.f, 0.f },
				{ 1.f, 0.f },
				{ 1.f, 1.f },
				{ 0.f, 1.f },
			};
			Renderer2D::draw_quad(position, size, rotation, texture, tex_coords, color, tiling_factor);
		}
		static inline void draw_quad(const glm::vec3 &position, const glm::vec2 &size, float rotation, const Ref<SubTexture2D> &texture, float tiling_factor = 1.0f) {
			Renderer2D::draw_quad(position, size, rotation, texture, glm::vec4(1.f), tiling_factor);
		}

		static inline void
		draw_quad(const glm::vec3 &position, const glm::vec2 &size, float rotation, const Ref<SubTexture2D> &sub_tex, const glm::vec4 &color, float tiling_factor = 1.0f) {
			Renderer2D::draw_quad(position, size, rotation, *sub_tex, *sub_tex, color, tiling_factor);
		}

		static void draw_quad(const glm::vec3 &position,
				      const glm::vec2 &size,
				      float rotation,
				      const Ref<Texture2D> &texture,
				      const glm::vec2 *coords,
				      const glm::vec4 &color,
				      float tiling_factor = 1.0f);

		static void draw_quad(const glm::mat4 &transform, const glm::vec4 &color, float tiling_factor = 1.0f);
		static inline void draw_quad(const glm::mat4 &transform, const Ref<Texture2D> &texture, float tiling_factor = 1.0f) {
			static const glm::vec2 tex_coords[] = {
				{ 0.f, 0.f },
				{ 1.f, 0.f },
				{ 1.f, 1.f },
				{ 0.f, 1.f },
			};
			Renderer2D::draw_quad(transform, texture, tex_coords, glm::vec4{ 1.0f }, tiling_factor);
		}
		static inline void draw_quad(const glm::mat4 &transform, const Ref<Texture2D> &texture, const glm::vec4 &color, float tiling_factor = 1.0f) {
			static const glm::vec2 tex_coords[] = {
				{ 0.f, 0.f },
				{ 1.f, 0.f },
				{ 1.f, 1.f },
				{ 0.f, 1.f },
			};
			Renderer2D::draw_quad(transform, texture, tex_coords, color, tiling_factor);
		}
		static inline void draw_quad(const glm::mat4 &transform, const Ref<Texture2D> &texture, const glm::vec2 *coords, float tiling_factor = 1.0f) {
			Renderer2D::draw_quad(transform, texture, coords, glm::vec4{ 1.0f }, tiling_factor);
		}
		static void draw_quad(const glm::mat4 &transform,
				      const Ref<Texture2D> &texture,
				      const glm::vec2 *coords,
				      const glm::vec4 &color,
				      float tiling_factor = 1.0f,
				      int entity_id = -1);

		static void draw_circle(const glm::mat4 &transform, const glm::vec4 &color, float thickness = 1.0f, float fade = 0.005f, int entity_id = -1);

		static void draw_sprite(const glm::mat4 &transform, const SpriteRendererComponent &src, int entity_id);

		static void draw_line(const glm::vec3 &p0, const glm::vec3 &p1, const glm::vec4 &color, int entity_id = -1);

		static void draw_rect(const glm::mat4 &transform, const glm::vec4 &color, int entity_id = -1);
		static void draw_rect(const glm::vec3 &position, const glm::vec2 &size, const glm::vec4 &color, int entity_id = -1);

		static float get_line_width(void);
		static void set_line_width(float width);

#ifdef SL_PROFILE
		struct Statistics {
			uint32_t draw_calls = 0;
			uint32_t quad_count = 0;

			inline uint32_t get_total_vertex_count(void) {
				return this->quad_count * 4;
			}
			inline uint32_t get_total_index_count(void) {
				return this->quad_count * 6;
			}
		};
		static Statistics get_stats(void);
		static void ResetStats(void);
#endif
	private:
		static void start_batch(void);
	};
} // namespace Solace
