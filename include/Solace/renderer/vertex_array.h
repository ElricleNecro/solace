#pragma once

#include <vector>

#include "Solace/core/core.h"
#include "Solace/renderer/buffer.h"

namespace Solace {
	class VertexArray {
	public:
		virtual ~VertexArray(void) = default;

		virtual void bind(void) const = 0;
		virtual void unbind(void) const = 0;

		virtual void add_vertex_buffer(const Ref<VertexBuffer> &buffer) = 0;
		virtual void set_index_buffer(const Ref<IndexBuffer> &buffer) = 0;

		[[nodiscard]] virtual const std::vector<Ref<VertexBuffer>> &get_vertex_buffers(void) const = 0;
		[[nodiscard]] virtual const Ref<IndexBuffer> &get_index_buffer(void) const = 0;

		[[nodiscard]] static Ref<VertexArray> create(void);
	};
} // namespace Solace
