#pragma once

#include <string>

#include "Solace/core/core.h"

namespace Solace {
	class Texture {
	public:
		virtual ~Texture(void) = default;

		virtual uint32_t get_width(void) const = 0;
		virtual uint32_t get_height(void) const = 0;

		virtual const std::string get_path(void) const = 0;

		virtual uint32_t get_renderer_id(void) const = 0;

		virtual void set_data(void *data, uint32_t size) = 0;

		virtual void bind(uint32_t slot = 0) const = 0;
		virtual void unbind(uint32_t slot = 0) const = 0;

		virtual bool operator==(const Texture &other) const = 0;
	};

	class Texture2D : public Texture {
	public:
		static Ref<Texture2D> create(const std::string &path);
		static Ref<Texture2D> create(const uint32_t width, const uint32_t height);
	};
} // namespace Solace
