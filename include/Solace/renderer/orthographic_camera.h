#pragma once

#include <glm/glm.hpp>

namespace Solace {
	class OrthographicCamera {
	public:
		OrthographicCamera(float left, float right, float bottom, float top);

		inline const glm::vec3 &get_position(void) const {
			return this->position;
		}
		inline void set_position(const glm::vec3 &position) {
			this->position = position;
			this->recalculate_view_matrix();
		}

		inline float get_rotation(void) const {
			return this->rotation;
		}
		inline void set_rotation(const float rotation) {
			this->rotation = rotation;
			this->recalculate_view_matrix();
		}

		inline const glm::mat4 &get_projection_matrix(void) const {
			return this->projection_matrix;
		}
		inline const glm::mat4 &get_view_matrix(void) const {
			return this->view_matrix;
		}
		inline const glm::mat4 &get_vp_matrix(void) const {
			return this->view_proj_matrix;
		}

		void set_projection(float left, float right, float bottom, float top);

	private:
		void recalculate_view_matrix(void);

	private:
		glm::mat4 projection_matrix;
		glm::mat4 view_matrix;
		glm::mat4 view_proj_matrix;

		glm::vec3 position;
		float rotation = 0.0f;
	};
} // namespace Solace
