#pragma once

#include <string>
#include <unordered_map>

#include <glm/glm.hpp>

#include "Solace/core/core.h"

namespace Solace {
	class Shader {
	public:
		virtual ~Shader(void) = default;

		virtual void bind(void) const = 0;
		virtual void unbind(void) const = 0;

		virtual void set_mat(const std::string &name, const glm::mat3 &matrix) = 0;
		virtual void set_mat(const std::string &name, const glm::mat4 &matrix) = 0;

		virtual void set_int(const std::string &name, const int value) = 0;
		virtual void set_int(const std::string &name, const int *values, uint32_t count) = 0;

		virtual void set_float(const std::string &name, const float value) = 0;
		virtual void set_float(const std::string &name, const glm::vec2 &values) = 0;
		virtual void set_float(const std::string &name, const glm::vec3 &values) = 0;
		virtual void set_float(const std::string &name, const glm::vec4 &values) = 0;

		virtual const std::string &get_name(void) const = 0;

		[[nodiscard]] static Ref<Shader> create(const std::string &filepath);
		[[nodiscard]] static Ref<Shader> create(const std::string &name, const std::string &vertex, const std::string &fragment);
	};

	class ShaderLibrary {
	public:
		void add(const Ref<Shader> &shader);
		void add(const std::string &name, const Ref<Shader> &shader);

		Ref<Shader> load(const std::string &file_path);
		Ref<Shader> load(const std::string &name, const std::string &file_path);

		bool exist(const std::string &name) const;
		Ref<Shader> get(const std::string &name);

	private:
		std::unordered_map<std::string, Ref<Shader>> shaders;
	};
} // namespace Solace
