#pragma once

#include "Solace/core/core.h"
#include "Solace/renderer/rendererapi.h"

namespace Solace {
	class RenderCommand {
	public:
		inline static void init(void) {
			RenderCommand::renderer_api->init();
		};

		inline static void set_clear_color(const glm::vec4 &color) {
			RenderCommand::renderer_api->set_clear_color(color);
		};

		inline static void clear(void) {
			RenderCommand::renderer_api->clear();
		};

		inline static void draw_indexed(const Ref<VertexArray> &vao, uint32_t index_count = 0) {
			RenderCommand::renderer_api->draw_indexed(vao, index_count);
		};

		inline static void draw_lines(const Ref<VertexArray> &vao, uint32_t index_count) {
			RenderCommand::renderer_api->draw_lines(vao, index_count);
		};

		inline static void set_line_width(float width) {
			RenderCommand::renderer_api->set_line_width(width);
		}

		inline static Ref<RendererAPI> get_api(void) {
			return RenderCommand::renderer_api;
		};

		inline static void viewport(int x, int y, int width, int height) {
			RenderCommand::renderer_api->viewport(x, y, width, height);
		};

	private:
		static const Ref<RendererAPI> renderer_api;
	};
} // namespace Solace
