#pragma once

#include <glm/glm.hpp>

#include "Solace/core/core.h"
#include "Solace/renderer/texture.h"

namespace Solace {
	class SubTexture2D {
	public:
		SubTexture2D(const Ref<Texture2D> &texture, const glm::vec2 &min, const glm::vec2 &max);

		inline operator const Ref<Texture2D> &() const {
			return this->texture;
		};
		inline operator const glm::vec2 *() const {
			return this->tex_coords;
		};
		inline const glm::vec2 &operator[](size_t idx) const {
			return this->tex_coords[idx];
		};

		static Ref<SubTexture2D>
		create_from_coordinates(const Ref<Texture2D> &texture, const glm::vec2 &coords, const glm::vec2 &sprite_size, const glm::vec2 &size_offet = { 1.f, 1.f });

	private:
		Ref<Texture2D> texture;
		glm::vec2 tex_coords[4];
	};
} // namespace Solace
