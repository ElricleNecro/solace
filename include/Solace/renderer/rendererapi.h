#pragma once

#include <glm/glm.hpp>

#include "Solace/core/core.h"
#include "Solace/renderer/vertex_array.h"

namespace Solace {
	class RendererAPI {
	public:
		enum class API { None = 0, OpenGL = 1 };

	public:
		virtual ~RendererAPI(void) = default;

		virtual void init(void) = 0;
		virtual void set_clear_color(const glm::vec4 &color) = 0;
		virtual void clear(void) = 0;

		virtual void draw_indexed(const Ref<VertexArray> &vao, uint32_t index_count = 0) = 0;
		virtual void draw_lines(const Ref<VertexArray> &vao, uint32_t index_count) = 0;
		virtual void viewport(int x, int y, int width, int height) = 0;

		virtual void set_line_width(float width) = 0;

		inline static API get_api(void) {
			return RendererAPI::api;
		}

	private:
		static API api;
	};
} // namespace Solace
