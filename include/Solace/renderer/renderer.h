#pragma once

#include <cstdint>
#include <SDL2/SDL.h>

#include <glm/glm.hpp>

#include "Solace/core/core.h"
#include "Solace/renderer/orthographic_camera.h"
#include "Solace/renderer/rendererapi.h"
#include "Solace/renderer/shader.h"
#include "Solace/vendor/imgui_layer.h"

namespace Solace {
	class Renderer {
	public:
		virtual ~Renderer(void) = default;

		virtual void init(SDL_Window *window) = 0;
		virtual uint32_t flags(void) = 0;
		virtual void swap_buffers(void) = 0;
		virtual void update_window_size(int *width, int *height) const = 0;

		static void init(void);
		static void shutdown(void);
		static void begin_scene(const OrthographicCamera &camera);
		static void end_scene(void);
		static void submit(const Ref<VertexArray> &vao);
		static void submit(const Ref<Shader> &shader, const Ref<VertexArray> &vao, const glm::mat4 &transform = glm::mat4(1.0f));
		static void viewport(int width, int height);

		inline static RendererAPI::API get_api(void) {
			return RendererAPI::get_api();
		}

		static Ref<Renderer> create(void);

	private:
		static glm::mat4 view_proj_matrix;
		static Ref<Shader> last_shader;
	};
} // namespace Solace
