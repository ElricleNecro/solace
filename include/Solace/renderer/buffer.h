#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include "Solace/core/core.h"

namespace Solace {
	enum class ShaderDataType {
		None = 0,

		Float,
		Float2,
		Float3,
		Float4,

		Mat3,
		Mat4,

		Int,
		Int2,
		Int3,
		Int4,

		Bool
	};

	struct BufferElement {
		std::string name;
		ShaderDataType type;

		uint32_t size;
		uint32_t offset;

		bool normalised;

		BufferElement(void) = default;
		BufferElement(ShaderDataType type, const std::string &name, bool normalised = false);

		[[nodiscard]] uint32_t get_component_count(void) const;
	};

	class BufferLayout {
	public:
		BufferLayout(void) = default;
		BufferLayout(const std::initializer_list<BufferElement> &elements);

		[[nodiscard]] inline const std::vector<BufferElement> &get_elements(void) const {
			return this->elems;
		};
		[[nodiscard]] inline uint32_t get_stride(void) const {
			return this->stride;
		};

		inline std::vector<BufferElement>::iterator begin(void) {
			return this->elems.begin();
		};
		inline std::vector<BufferElement>::iterator end(void) {
			return this->elems.end();
		};

		[[nodiscard]] inline std::vector<BufferElement>::const_iterator begin(void) const {
			return this->elems.begin();
		};
		[[nodiscard]] inline std::vector<BufferElement>::const_iterator end(void) const {
			return this->elems.end();
		};

	private:
		void calculate_offset_and_stride(void);

		std::vector<BufferElement> elems;
		uint32_t stride = 0;
	};

	class VertexBuffer {
	public:
		virtual ~VertexBuffer(void) = default;

		virtual void bind(void) const = 0;
		virtual void unbind(void) const = 0;

		virtual void set_layout(const BufferLayout &layout) = 0;
		[[nodiscard]] virtual const BufferLayout &get_layout(void) const = 0;

		virtual void set_data(const void *data, uint32_t size) = 0;

		[[nodiscard]] static Ref<VertexBuffer> create(uint32_t size);
		[[nodiscard]] static Ref<VertexBuffer> create(const float *vertices, const uint32_t size);
	};

	class IndexBuffer {
	public:
		virtual ~IndexBuffer(void) = default;

		virtual void bind(void) const = 0;
		virtual void unbind(void) const = 0;

		virtual uint32_t get_count(void) const = 0;

		static Ref<IndexBuffer> create(const uint32_t *vertices, const uint32_t count);
	};
} // namespace Solace
