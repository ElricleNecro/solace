#pragma once

#include <string>

#include "Solace/renderer/texture.h"
#include "Solace/vendor/stb/stb_image.h"

#ifdef USE_GLEW
#include <GL/glew.h>
#elif defined(USE_GLBINDING)
#include <glbinding/gl/gl.h>
#include <glbinding/glbinding.h> // Initialize with glbinding::initialize()
#endif

namespace Solace {
	class OpenGLTexture2D : public Texture2D {
	public:
		OpenGLTexture2D(const std::string &path);
		OpenGLTexture2D(const uint32_t width, const uint32_t height);
		virtual ~OpenGLTexture2D(void);

		inline uint32_t get_width(void) const override {
			return this->width;
		};
		inline uint32_t get_height(void) const override {
			return this->height;
		};
		inline uint32_t get_renderer_id(void) const override {
			return this->renderer_id;
		}
		inline const std::string get_path(void) const override {
			return this->path;
		}

		void set_data(void *data, uint32_t size) override;

		void bind(uint32_t slot = 0) const override;
		void unbind(uint32_t slot = 0) const override;

		inline bool operator==(const Texture &other) const override {
			return this->renderer_id == other.get_renderer_id();
		}

	private:
		std::string path;
		uint32_t width, height;
		uint32_t renderer_id;

		gl::GLenum internal_format, data_format;
	};
} // namespace Solace
