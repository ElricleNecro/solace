#pragma once

#include <string>
#include <vector>
#include <unordered_map>

#include <glm/glm.hpp>

#ifdef USE_GLEW
#include <GL/glew.h>
#elif defined(USE_GLBINDING)
#include <glbinding/gl/gl.h>
#include <glbinding/glbinding.h>
#endif

#include "Solace/renderer/shader.h"

namespace Solace {
	class OpenGLShader : public Shader {
	public:
		OpenGLShader(const std::string &filepath);
		OpenGLShader(const std::string &name, const std::string &vertex, const std::string &fragment);
		~OpenGLShader(void) override;

		void bind(void) const override;
		void unbind(void) const override;

		const std::string &get_name(void) const override {
			return this->name;
		};

		void set_mat(const std::string &name, const glm::mat3 &matrix) override;
		void set_mat(const std::string &name, const glm::mat4 &matrix) override;

		void set_int(const std::string &name, const int value) override;
		void set_int(const std::string &name, const int *values, uint32_t count) override;

		void set_float(const std::string &name, const float value) override;
		void set_float(const std::string &name, const glm::vec2 &values) override;
		void set_float(const std::string &name, const glm::vec3 &values) override;
		void set_float(const std::string &name, const glm::vec4 &values) override;

	private:
		std::string file_path;
		std::string name;
		uint32_t id;
		std::vector<uint32_t> shaders;

		std::unordered_map<gl::GLenum, std::vector<uint32_t>> spirv;

		std::string read_file(const std::string &filepath);
		std::unordered_map<gl::GLenum, std::string> preprocess(const std::string &source);
		void compile(const std::unordered_map<gl::GLenum, std::string> &shader_sources);
		void compile(const gl::GLenum type, const std::string &src);
		void reflect(gl::GLenum type, const std::vector<uint32_t> data);
	};
} // namespace Solace
