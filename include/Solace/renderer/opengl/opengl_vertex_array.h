#pragma once

#include <vector>

#include "Solace/core/core.h"
#include "Solace/renderer/vertex_array.h"

namespace Solace {
	class OpenGLVertexArray : public VertexArray {
	public:
		OpenGLVertexArray(void);
		~OpenGLVertexArray(void) override;

		void bind(void) const override;
		void unbind(void) const override;

		void add_vertex_buffer(const Ref<VertexBuffer> &buffer) override;
		void set_index_buffer(const Ref<IndexBuffer> &buffer) override;

		[[nodiscard]] const std::vector<Ref<VertexBuffer>> &get_vertex_buffers(void) const override;
		[[nodiscard]] const Ref<IndexBuffer> &get_index_buffer(void) const override;

	private:
		std::vector<Ref<VertexBuffer>> vertex_buffers;
		Ref<IndexBuffer> index_buffer;
		uint32_t id;
	};
} // namespace Solace
