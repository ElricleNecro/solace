#pragma once

#include "Solace/renderer/framebuffer.h"

namespace Solace {
	class OpenGLFrameBuffer : public FrameBuffer {
	public:
		OpenGLFrameBuffer(const FrameBufferSpecification &spec);
		virtual ~OpenGLFrameBuffer(void);

		virtual inline FrameBufferSpecification &get_specification(void) override {
			return this->spec;
		};
		virtual inline const FrameBufferSpecification &get_specification(void) const override {
			return this->spec;
		};

		virtual inline uint32_t get_color_attachement_renderer_id(size_t index = 0) const override {
			SOLACE_CORE_ASSERT(index < this->id_color_attachments.size(), "Accessing inexisting color attachment");
			return this->id_color_attachments[index];
		};
		virtual inline uint32_t get_depth_buffer_renderer_id(void) const override {
			return this->id_depth_attachment;
		};

		virtual int read_pixel(uint32_t attachment_idx, uint32_t x, uint32_t y) const override;

		virtual void clear_color_attachment(size_t index, int value) const override;

		virtual void invalidate(void) override;

		virtual void resize(uint32_t width, uint32_t height) override;

		virtual void bind(void) const override;
		virtual void unbind(void) const override;

	private:
		FrameBufferSpecification spec;
		uint32_t renderer_id = 0;
		uint32_t id_depth_attachment = 0;
		std::vector<uint32_t> id_color_attachments;

		std::vector<FrameBufferTextureSpecification> color_attachment_specs;
		FrameBufferTextureSpecification depth_attachment_spec = FrameBufferTextureFormat::None;
	};
} // namespace Solace
