#pragma once

#include "Solace/renderer/buffer.h"

namespace Solace {
	class OpenGLVertexBuffer : public VertexBuffer {
	public:
		OpenGLVertexBuffer(const uint32_t size);
		OpenGLVertexBuffer(const float *vertices, const uint32_t size);
		~OpenGLVertexBuffer(void) override;

		void bind(void) const override;
		void unbind(void) const override;

		void set_layout(const BufferLayout &layout) override;
		[[nodiscard]] const BufferLayout &get_layout(void) const override {
			return this->layout;
		};

		void set_data(const void *data, uint32_t size) override;

	private:
		uint32_t id;
		BufferLayout layout;
	};

	class OpenGLIndexBuffer : public IndexBuffer {
	public:
		OpenGLIndexBuffer(const uint32_t *vertices, const uint32_t count);
		~OpenGLIndexBuffer(void) override;

		void bind(void) const override;
		void unbind(void) const override;

		[[nodiscard]] uint32_t get_count(void) const override {
			return this->count;
		};

	private:
		uint32_t id;
		uint32_t count;
	};
} // namespace Solace
