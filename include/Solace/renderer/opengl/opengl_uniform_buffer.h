#pragma once

#include "Solace/renderer/uniform_buffer.h"

namespace Solace {
	class OpenGLUniformBuffer : public UniformBuffer {
	public:
		OpenGLUniformBuffer(size_t size, uint32_t binding);
		virtual ~OpenGLUniformBuffer(void);

		virtual void set_data(const void *data, size_t size, size_t offset = 0) override;

	private:
		uint32_t id = 0;
	};
} // namespace Solace
