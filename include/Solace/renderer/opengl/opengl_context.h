#pragma once

#include <SDL2/SDL.h>

#include "Solace/core/exception.h"
#include "Solace/core/log.h"
#include "Solace/renderer/renderer.h"
#include "Solace/vendor/imgui_layer.h"

namespace Solace {
	class OpenGLRenderer : public Renderer {
	public:
		OpenGLRenderer(void);
		~OpenGLRenderer(void) override;

		void init(SDL_Window *window) override;
		uint32_t flags(void) override;
		void swap_buffers(void) override;

		void update_window_size(int *width, int *height) const override;

		inline SDL_GLContext get_context(void) const {
			return this->ogl_context;
		};

	private:
		SDL_Window *window;
		SDL_GLContext ogl_context;
	};
} // namespace Solace
