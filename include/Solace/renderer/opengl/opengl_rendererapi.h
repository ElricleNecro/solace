#pragma once

#include "Solace/core/core.h"
#include "Solace/renderer/rendererapi.h"

namespace Solace {
	class OpenGLRendererAPI : public RendererAPI {
	public:
		virtual void init(void) override;
		virtual void set_clear_color(const glm::vec4 &color) override;
		virtual void clear(void) override;
		virtual void viewport(int x, int y, int width, int height) override;

		virtual void draw_indexed(const Ref<VertexArray> &vao, uint32_t index_count = 0) override;
		virtual void draw_lines(const Ref<VertexArray> &vao, uint32_t index_count) override;

		virtual void set_line_width(float width) override;
	};
} // namespace Solace
