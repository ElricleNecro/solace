#pragma once

#include "Solace/core/core.h"

namespace Solace {
	class UniformBuffer {
	public:
		virtual ~UniformBuffer(void) = default;

		virtual void set_data(const void *data, size_t size, size_t offset = 0) = 0;

		static Ref<UniformBuffer> create(size_t size, uint32_t binding);
	};
} // namespace Solace
