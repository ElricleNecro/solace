#pragma once

#include <string>

namespace Solace {
	namespace FileDialogs {
		std::string open_file(const char *filter);
		std::string save_file(const char *filter);
	} // namespace FileDialogs
} // namespace Solace
