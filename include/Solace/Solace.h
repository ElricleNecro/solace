#pragma once

#include "Solace/core/application.h"
#include "Solace/core/layer.h"
#include "Solace/core/log.h"

#include "Solace/core/events.h"
#include "Solace/orthographic_camera_controller.h"

#include "Solace/renderer/rendercommand.h"
#include "Solace/renderer/renderer.h"
#include "Solace/renderer/renderer2D.h"

#include "Solace/renderer/buffer.h"
#include "Solace/renderer/framebuffer.h"
#include "Solace/renderer/shader.h"
#include "Solace/renderer/subtexture2D.h"
#include "Solace/renderer/texture.h"
#include "Solace/renderer/vertex_array.h"

#include "Solace/renderer/orthographic_camera.h"

#include "Solace/scene/components.h"
#include "Solace/scene/entity.h"
#include "Solace/scene/scene.h"
#include "Solace/scene/scriptable_entity.h"
