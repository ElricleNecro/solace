#pragma once

#include <spdlog/spdlog.h>

#include "Solace/core/core.h"

namespace Solace {
	class SOLACE_API Log {
	public:
		static void init(void);

		inline static Ref<spdlog::logger> &get_core_logger(void) {
			return core_logger;
		}
		inline static Ref<spdlog::logger> &get_client_logger(void) {
			return client_logger;
		}

	private:
		static Ref<spdlog::logger> core_logger;
		static Ref<spdlog::logger> client_logger;
	};
} // namespace Solace

#define SL_CORE_ERROR(...) ::Solace::Log::get_core_logger()->error(__VA_ARGS__)
#define SL_CORE_WARN(...) ::Solace::Log::get_core_logger()->warn(__VA_ARGS__)
#define SL_CORE_INFO(...) ::Solace::Log::get_core_logger()->info(__VA_ARGS__)
#define SL_CORE_TRACE(...) ::Solace::Log::get_core_logger()->trace(__VA_ARGS__)
#define SL_CORE_FATAL(...) ::Solace::Log::get_core_logger()->fatal(__VA_ARGS__)

#define SL_ERROR(...) ::Solace::Log::get_client_logger()->error(__VA_ARGS__)
#define SL_WARN(...) ::Solace::Log::get_client_logger()->warn(__VA_ARGS__)
#define SL_INFO(...) ::Solace::Log::get_client_logger()->info(__VA_ARGS__)
#define SL_TRACE(...) ::Solace::Log::get_client_logger()->trace(__VA_ARGS__)
