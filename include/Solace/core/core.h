#pragma once

#include <memory>
#include <utility>

#ifdef S_PLATFORM_WINDOWS
#ifdef HZ_BUILD_DLL
#define SOLACE_API __declspec(dllexport)
#else
#define SOLACE_API __declspec(dllimport)
#endif
#elif defined(S_PLATFORM_LINUX)
#define SOLACE_API
#else
#error Only LINUX or windows (kind of) are supported.
#endif

#ifdef SOLACE_DEBUG
#if defined(S_PLATFORM_WINDOWS)
#define SOLACE_DEBUGBREAK() __debugbreak()
#elif defined(S_PLATFORM_LINUX)
#include <signal.h>
#define SOLACE_DEBUGBREAK() raise(SIGTRAP)
#else
#error "Platform doesn't support debugbreak!"
#endif
#define SOLACE_ENABLE_ASSERTS
#else
#define SOLACE_DEBUGBREAK()
#endif

namespace Solace {
	template <typename T> using Scope = std::unique_ptr<T>;

	template <typename T, typename... Args> constexpr inline Scope<T> make_scope(Args &&...args) {
		return std::make_unique<T>(std::forward<Args>(args)...);
	}

	template <typename T> using Ref = std::shared_ptr<T>;

	template <typename T, typename... Args> constexpr inline Ref<T> make_ref(Args &&...args) {
		return std::make_shared<T>(std::forward<Args>(args)...);
	}
} // namespace Solace

#include "Solace/core/assert.h"
#include "Solace/core/log.h"
