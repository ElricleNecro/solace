#pragma once

#include <array>
#include <tuple>

#include <SDL2/SDL.h>

#include "imgui/imgui.h"

namespace Solace {
	class Events {
	public:
		Events(const unsigned int width, const unsigned int height);
		virtual ~Events(void) = default;

		bool update(void);

		[[nodiscard]] inline uint32_t id(void) const {
			return this->_winID;
		};
		[[nodiscard]] inline bool close_window(void) const {
			return this->_close_window;
		};
		[[nodiscard]] inline bool quit(void) const {
			return this->_quit;
		};
		[[nodiscard]] inline bool resized(void) const {
			return this->_resized;
		};
		[[nodiscard]] inline bool minimized(void) const {
			return this->_minimized;
		};
		[[nodiscard]] inline const std::array<bool, 8> &mouse(void) const {
			return this->_kMouse;
		};

		[[nodiscard]] inline unsigned int x(void) const {
			return this->_x;
		};
		[[nodiscard]] inline unsigned int y(void) const {
			return this->_y;
		};

		[[nodiscard]] inline std::tuple<unsigned int, unsigned int> mouse_position(void) const {
			return std::make_tuple(this->_x, this->_y);
		};

		[[nodiscard]] inline int dx(void) const {
			return this->_dx;
		};
		[[nodiscard]] inline int dy(void) const {
			return this->_dy;
		};

		[[nodiscard]] inline int wheel_x(void) const {
			return this->_wx;
		};
		[[nodiscard]] inline int wheel_y(void) const {
			return this->_wy;
		};

		[[nodiscard]] inline const std::tuple<unsigned int, unsigned int> &size(void) const {
			return this->_windowSize;
		};

		bool operator[](SDL_Scancode code) const;

		[[nodiscard]] inline const SDL_Event &event(void) const {
			return this->_evt;
		};

		[[nodiscard]] inline bool used(void) const {
			return this->_event_used;
		}
		inline void set_used(void) {
			this->_event_used = true;
		}

		inline operator const SDL_Event &(void) const {
			return this->_evt;
		}

		inline operator SDL_Scancode(void) const {
			return this->_last_register;
		}

		inline operator uint64_t(void) const {
			return this->_last_register;
		}

	private:
		/* data */
		SDL_Event _evt;
		SDL_Scancode _last_register = SDL_NUM_SCANCODES;

		uint32_t _winID;

		std::array<bool, SDL_NUM_SCANCODES> _kPress = { false };
		std::array<bool, 8> _kMouse = { false };

		unsigned int _x, _y;
		int _dx, _dy;
		int _wx, _wy;

		bool _close_window, _resized, _minimized, _event_used, _quit;

		std::tuple<unsigned int, unsigned int> _windowSize;

		static Events *instance;
	};
} // namespace Solace
