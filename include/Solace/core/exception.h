#pragma once

#include <exception>

namespace Solace {
	class SDLInitException : public std::exception {
	public:
		SDLInitException(const char *error_msg) : error_msg(error_msg){};

		[[nodiscard]] const char *what(void) const noexcept override {
			return this->error_msg;
		};

	private:
		const char *error_msg;
	};

	class SDLCreateWindowException : public std::exception {
	public:
		SDLCreateWindowException(const char *error_msg) : error_msg(error_msg){};

		[[nodiscard]] const char *what(void) const noexcept override {
			return this->error_msg;
		};

	private:
		const char *error_msg;
	};

	class SDLCreateOGLContextException : public std::exception {
	public:
		SDLCreateOGLContextException(const char *error_msg) : error_msg(error_msg){};

		[[nodiscard]] const char *what(void) const noexcept override {
			return this->error_msg;
		};

	private:
		const char *error_msg;
	};

	class OGLInitException : public std::exception {
	public:
		OGLInitException(const char *error_msg) : error_msg(error_msg){};

		[[nodiscard]] const char *what(void) const noexcept override {
			return this->error_msg;
		};

	private:
		const char *error_msg;
	};
} // namespace Solace
