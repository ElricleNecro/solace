#pragma once

#include <string>

#include "Solace/core/events.h"

namespace Solace {
	class Layer {
	public:
		Layer(std::string name) : name(std::move(name)){};
		Layer(const Layer &dw) = default;
		Layer(Layer &&dw) noexcept : name(std::move(dw.name)){};
		virtual ~Layer(void) = default;

		Layer &operator=([[maybe_unused]] const Layer &dw) noexcept {
			return *this;
		}

		Layer &operator=([[maybe_unused]] Layer &&dw) noexcept {
			return *this;
		}

		virtual void on_attach(void){};
		virtual void on_detach(void){};
		virtual void on_update([[maybe_unused]] float delta){};
		virtual void on_imgui_render(void){};
		virtual bool on_event(Events &);

		[[nodiscard]] inline const std::string &get_name(void) const {
			return this->name;
		}

	protected:
		std::string name;
	};
} // namespace Solace
