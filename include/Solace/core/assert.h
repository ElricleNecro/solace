#pragma once

#include "Solace/core/log.h"

#ifdef SOLACE_ENABLE_ASSERTS
#define SOLACE_ASSERT(x, ...)                                                                                          \
	{                                                                                                              \
		if (!(x)) {                                                                                            \
			SL_ERROR("Assertion failed: {0}", __VA_ARGS__);                                                \
			SOLACE_DEBUGBREAK();                                                                           \
		}                                                                                                      \
	}
#define SOLACE_CORE_ASSERT(x, ...)                                                                                     \
	{                                                                                                              \
		if (!(x)) {                                                                                            \
			SL_CORE_ERROR("Assertion failed: {0}", __VA_ARGS__);                                           \
			SOLACE_DEBUGBREAK();                                                                           \
		}                                                                                                      \
	}
#else
#define SOLACE_ASSERT(x, ...)
#define SOLACE_CORE_ASSERT(x, ...)
#endif

