#pragma once

#include <string>
#include <vector>

#include "Solace/core/core.h"
#include "Solace/core/events.h"
#include "Solace/core/exception.h"
#include "Solace/core/layer.h"
#include "Solace/core/layerstack.h"
#include "Solace/renderer/renderer.h"
#include "Solace/utils/platform_utils.h"
#include "Solace/vendor/imgui_layer.h"

#ifndef DEFAULT_WIDTH
#define DEFAULT_WIDTH 800
#endif

#ifndef DEFAULT_HEIGHT
#define DEFAULT_HEIGHT 600
#endif

namespace Solace {
	struct CLIArgs {
		int argc = 0;
		char **argv = nullptr;

		const char* operator[](const int index) const {
			SOLACE_CORE_ASSERT(index < this->argc, "Access to a non existant cli argument.");

			return this->argv[index];
		}
	};

	struct ApplicationSpecification {
		std::string title = "Solace Renderer";
		std::string working_directory;
		int width = DEFAULT_WIDTH;
		int height = DEFAULT_HEIGHT;
		CLIArgs cli_args;
	};

	class SOLACE_API Application {
	public:
		Application(const ApplicationSpecification &spec);
		Application(const ApplicationSpecification &spec, Ref<Renderer> renderer);
		virtual ~Application(void);

		Application(Application &copy) = delete;
		Application(Application &&copy) = delete;
		Application &operator=(Application &copy) = delete;
		Application &operator=(Application &&copy) = delete;

		[[nodiscard]] inline int get_width(void) const {
			return this->width;
		};
		[[nodiscard]] inline int get_height(void) const {
			return this->height;
		};

		[[nodiscard]] inline ImGuiLayer *get_imgui_layer(void) {
			return this->imgui_layer;
		}

		void push(Layer *layer);
		void push_overlay(Layer *layer);

		inline void pop(Layer *layer) {
			this->layer_subsystem.pop(layer);
		};
		inline Layer *pop(const std::string &name) {
			return this->layer_subsystem.pop(name);
		};
		inline void pop_overlay(Layer *layer) {
			this->layer_subsystem.pop_overlay(layer);
		};
		inline Layer *pop_overlay(const std::string &name) {
			return this->layer_subsystem.pop_overlay(name);
		};

		[[nodiscard]] LayerStack &layers(void);

		void run(void);
		void quit(void);

		inline Events &get_events(void) {
			return this->events;
		};
		const inline Events &get_events(void) const {
			return this->events;
		};
		const inline static Events &get_events_data(void) {
			return instance->events;
		};
		const inline ApplicationSpecification& get_spec(void) const {
			return this->spec;
		}

		inline static Application &get(void) {
			return *instance;
		};

	protected:
		const inline static Ref<Renderer> get_renderer(void) {
			return instance->renderer;
		}

		inline static SDL_Window *get_sdl_window(void) {
			return instance->window;
		}

	private:
		bool running;
		int width = 0, height = 0;
		SDL_Window *window = nullptr;
		ImGuiLayer *imgui_layer = nullptr;

		Events events;

		LayerStack layer_subsystem;

		Ref<Renderer> renderer;

		const ApplicationSpecification spec;

		static Application *instance;

		friend class ImGuiLayer;
		friend std::string FileDialogs::open_file(const char *filter);
		friend std::string FileDialogs::save_file(const char *filter);
	};

	Application *create_application(CLIArgs args);
} // namespace Solace
