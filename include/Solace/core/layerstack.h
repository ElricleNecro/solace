#pragma once

#include <string>
#include <vector>

#include "Solace/core/layer.h"

namespace Solace {
	class LayerStack {
	public:
		LayerStack(void) = default;
		~LayerStack(void);

		void push(Layer *layer);
		void pop(Layer *layer);
		Layer *pop(const std::string &layer);

		void push_overlay(Layer *overlay);
		void pop_overlay(Layer *overlay);
		Layer *pop_overlay(const std::string &layer);

		inline std::vector<Layer *>::iterator begin(void) {
			return this->layers.begin();
		};
		inline std::vector<Layer *>::iterator end(void) {
			return this->layers.end();
		};

		inline std::vector<Layer *>::reverse_iterator rbegin(void) {
			return this->layers.rbegin();
		};
		inline std::vector<Layer *>::reverse_iterator rend(void) {
			return this->layers.rend();
		};

		[[nodiscard]] inline std::vector<Layer *>::const_iterator begin(void) const {
			return this->layers.begin();
		};
		[[nodiscard]] inline std::vector<Layer *>::const_iterator end(void) const {
			return this->layers.end();
		};

		[[nodiscard]] inline std::vector<Layer *>::const_reverse_iterator rbegin(void) const {
			return this->layers.rbegin();
		};
		[[nodiscard]] inline std::vector<Layer *>::const_reverse_iterator rend(void) const {
			return this->layers.rend();
		};

		[[nodiscard]] inline uint64_t overlay_start(void) const {
			return this->index;
		};
		[[nodiscard]] inline uint64_t size(void) const {
			return this->layers.size();
		};

		[[nodiscard]] inline const Layer *at(uint64_t pos) const {
			return this->layers.at(pos);
		};
		inline const Layer *operator[](uint64_t pos) const {
			return this->layers[pos];
		};

		[[nodiscard]] inline Layer *at(uint64_t pos) {
			return this->layers.at(pos);
		};
		inline Layer *operator[](uint64_t pos) {
			return this->layers[pos];
		};

		[[nodiscard]] const Layer *at(const std::string &pos) const;
		const Layer *operator[](const std::string &pos) const;

		[[nodiscard]] Layer *at(const std::string &pos);
		Layer *operator[](const std::string &pos);

	private:
		std::vector<Layer *> layers;
		uint64_t index = 0;
	};
} // namespace Solace
