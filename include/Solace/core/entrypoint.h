#pragma once

#include "Solace/core/application.h"
#include "Solace/core/log.h"

#include "Solace/debug/instrumentor.h"

extern Solace::Application *Solace::create_application(CLIArgs args);

int main(int argc, char *argv[]) {
	Solace::Log::init();

	SL_PROFILE_BEGIN_SESSION("startup", "SolaceProfile-startup.json");
	auto app = Solace::create_application({argc, argv});
	SL_PROFILE_END_SESSION();

	SL_PROFILE_BEGIN_SESSION("runtime", "SolaceProfile-runtime.json");
	app->run();
	SL_PROFILE_END_SESSION();

	SL_PROFILE_BEGIN_SESSION("shutdown", "SolaceProfile-shutdown.json");
	delete app;
	SL_PROFILE_END_SESSION();

	return EXIT_SUCCESS;
}
