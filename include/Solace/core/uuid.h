#pragma once

#include <functional>
#include <inttypes.h>

namespace Solace {
	class UUID {
	public:
		UUID(void);
		UUID(uint64_t uuid);
		UUID(const UUID &) = default;

		operator uint64_t(void) const {
			return this->uuid;
		}

	private:
		uint64_t uuid;
	};
} // namespace Solace

namespace std {
	template <>
	struct hash<Solace::UUID> {
		std::size_t operator()(const Solace::UUID &uuid) const {
			return hash<uint64_t>()(uuid);
		}
	};
} // namespace std
