#pragma once

#include <glm/glm.hpp>

#include "Solace/core/events.h"
#include "Solace/renderer/orthographic_camera.h"

namespace Solace {
	struct OrthographicCameraBounds {
		float left, right;
		float bottom, top;

		float width() const {
			return this->right - this->left;
		}
		float height() const {
			return this->top - this->bottom;
		}
	};

	class OrthographicCameraController {
	public:
		OrthographicCameraController(float aspect_ratio, bool rotation = false);

		inline float get_zoom_level(void) const {
			return this->zoom_level;
		};
		inline void set_zoom_level(const float level) {
			this->zoom_level = level;
			this->update_camera();
		};

		void on_update(float elapsed);
		bool on_event(Events &evt);

		OrthographicCamera &get_camera(void) {
			return this->camera;
		};

		const OrthographicCamera &get_camera(void) const {
			return this->camera;
		};

		operator const OrthographicCamera &(void) const {
			return this->camera;
		};

		const OrthographicCameraBounds &get_bounds(void) const {
			return this->bounds;
		};

		void resize(float width, float height);

	private:
		float aspect_ratio;
		float zoom_level = 1.0f;
		OrthographicCameraBounds bounds;
		OrthographicCamera camera;
		bool rotation;

		glm::vec3 position = { 0.f, 0.f, 0.f };
		float camera_rotation = 0.0f;
		float camera_translation_speed = 180.f;
		float camera_rotation_speed = 180.;

		inline void update_camera(void) {
			this->bounds = { -this->aspect_ratio * this->zoom_level, this->aspect_ratio * this->zoom_level, -this->zoom_level, this->zoom_level };

			this->camera.set_projection(this->bounds.left, this->bounds.right, this->bounds.bottom, this->bounds.top);
		};
	};
} // namespace Solace
