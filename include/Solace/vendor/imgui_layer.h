#pragma once

#include <SDL2/SDL.h>

#include "imgui/imgui.h"

#include "Solace/core/events.h"
#include "Solace/core/layer.h"
#include "Solace/widgets/utils.h"

namespace Solace {
	class ImGuiLayer : public Layer {
	public:
		ImGuiLayer(const std::string &name);
		~ImGuiLayer(void) override = default;
		COPY_MOVE_BASIC_LAYER(ImGuiLayer)

		void on_attach(void) final;
		void on_detach(void) final;
		bool on_event(Events &) final;

		void begin(void);
		void end(void);

		SDL_Window *window = nullptr;
		void init_gl_imgui(SDL_GLContext ogl);

		void clean_up(void);

		void set_block_events(bool block) {
			this->block_events = block;
		}

		void set_dark_theme_colors(void);

	private:
		bool block_events = true;
		std::array<bool, 3> g_MousePressed = { false, false, false };
	};
} // namespace Solace
