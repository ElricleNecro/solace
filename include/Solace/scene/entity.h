#pragma once

#include "Solace/core/core.h"
#include "Solace/core/uuid.h"

#include "Solace/scene/components.h"
#include "Solace/scene/scene.h"

#include "entt.h"

namespace Solace {
	class Entity {
	public:
		Entity(void) = default;
		Entity(const Entity &other) = default;

		Entity(entt::entity handle, Scene *scene);
		Entity(int handle, Scene *scene) : Entity((entt::entity)handle, scene){};

		template <typename T>
		bool has(void) const {
			return this->scene->registry.all_of<T>(this->handle);
		}

		template <typename T, typename... Args>
		T &add(Args &&...args) {
			SOLACE_CORE_ASSERT(!this->has<T>(), "Entity alread has component!");
			T &component = this->scene->registry.emplace<T>(this->handle, std::forward<Args>(args)...);
			this->scene->on_component_added<T>(*this, component);
			return component;
		}

		template <typename T, typename... Args>
		T &add_or_replace(Args &&...args) {
			T &component = this->scene->registry.emplace_or_replace<T>(this->handle, std::forward<Args>(args)...);
			this->scene->on_component_added<T>(*this, component);
			return component;
		}

		template <typename T>
		T &get(void) {
			SOLACE_CORE_ASSERT(this->has<T>(), "Entity does not have component!");
			return this->scene->registry.get<T>(this->handle);
		}

		template <typename T>
		void remove(void) {
			this->scene->registry.remove<T>(this->handle);
		}

		inline UUID get_id(void) {
			SOLACE_CORE_ASSERT(this->has<IDComponent>(), "Entity without id!");
			return this->get<IDComponent>().id;
		}

		inline std::string get_name(void) {
			SOLACE_CORE_ASSERT(this->has<TagComponent>(), "Entity without tag!");
			return this->get<TagComponent>().tag;
		}

		inline operator entt::entity(void) const {
			return this->handle;
		}

		inline operator bool(void) const {
			return this->scene != nullptr && this->handle != entt::null;
		};

		inline operator uint64_t(void) const {
			return (uint64_t)this->handle;
		}

		inline bool operator==(const Entity &other) const {
			return this->handle == other.handle && this->scene == other.scene;
		}

		inline bool operator!=(const Entity &other) const {
			return !(*this == other);
		}

	private:
		entt::entity handle{ entt::null };
		Scene *scene = nullptr;
	};
} // namespace Solace
