#pragma once

#include "Solace/renderer/camera.h"

namespace Solace {
	class SceneCamera : public Camera {
	public:
		enum class ProjectionType { Perspective = 0, Orthographic = 1 };

	public:
		SceneCamera(void);
		virtual ~SceneCamera(void);

		void set_orthographic(float size, float near_clip, float far_clip);
		void set_perspective(float fov, float near_clip, float far_clip);
		void set_viewport_size(uint32_t width, uint32_t height);

		inline float get_orthographic_size(void) const {
			return this->orthographic_size;
		}
		inline void set_orthographic_size(float size) {
			this->orthographic_size = size;
			this->recalculate_projection();
		}

		inline float get_orthographic_near(void) const {
			return this->orthographic_near;
		}
		inline void set_orthographic_near(float near) {
			this->orthographic_near = near;
			this->recalculate_projection();
		}

		inline float get_orthographic_far(void) const {
			return this->orthographic_far;
		}
		inline void set_orthographic_far(float far) {
			this->orthographic_far = far;
			this->recalculate_projection();
		}

		inline float get_perspective_fov(void) const {
			return this->perspective_fov;
		}
		inline void set_perspective_fov(float fov) {
			this->perspective_fov = fov;
			this->recalculate_projection();
		}

		inline float get_perspective_near(void) const {
			return this->perspective_near;
		}
		inline void set_perspective_near(float near) {
			this->perspective_near = near;
			this->recalculate_projection();
		}

		inline float get_perspective_far(void) const {
			return this->perspective_far;
		}
		inline void set_perspective_far(float far) {
			this->perspective_far = far;
			this->recalculate_projection();
		}

		inline void set_type(size_t type) {
			this->set_type((ProjectionType)type);
		}
		inline void set_type(ProjectionType type) {
			this->type = type;
		}

		inline operator ProjectionType(void) const {
			return this->type;
		}

		inline operator size_t(void) const {
			return (size_t)this->type;
		}

	private:
		void recalculate_projection(void);

		ProjectionType type = ProjectionType::Orthographic;

		float orthographic_size = 10.0f;
		/** Set the near and far clip for the camera. */
		float orthographic_near = -1.0f, orthographic_far = 1.0f;

		float perspective_fov = glm::radians(45.0f);
		/** Set the near and far clip for the camera. */
		float perspective_near = 0.01f, perspective_far = 1000.0f;

		float aspect_ratio = 0.0f;
	};
} // namespace Solace
