#pragma once

#include "Solace/core/core.h"
#include "Solace/scene/scene.h"

namespace Solace {
	class SceneSerialiser {
	public:
		SceneSerialiser(const Ref<Scene> &scene);

		void serialise(const std::string &filepath);
		void serialise_runtime(const std::string &filepath);

		bool deserialise(const std::string &filepath);
		bool deserialise_runtime(const std::string &filepath);

	private:
		Ref<Scene> scene;
	};
} // namespace Solace
