#pragma once

#include "Solace/scene/entity.h"
#include "Solace/scene/scene.h"

namespace Solace {
	class ScriptableEntity {
	public:
		virtual ~ScriptableEntity(void) = default;

		template <typename T>
		T &get(void) {
			return this->entity.get<T>();
		}

	protected:
		virtual void on_create(void) {};
		virtual void on_destroy(void) {};
		virtual void on_update([[maybe_unused]] float timestep) {};

	private:
		Entity entity;

		friend class Scene;
	};
} // namespace Solace
