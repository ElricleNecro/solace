#pragma once

#include <functional>
#include <string>

#include "Solace/core/uuid.h"
#include "Solace/scene/scene_camera.h"

#include "Solace/renderer/texture.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

namespace Solace {
	struct IDComponent {
		UUID id;

		IDComponent(void) = default;
		IDComponent(UUID id) : id(id){};
		IDComponent(const IDComponent &) = default;
	};

	struct TagComponent {
		std::string tag;

		TagComponent(void) = default;
		TagComponent(const TagComponent &) = default;
		TagComponent(const std::string &tag) : tag(tag) {
		}

		operator const std::string &(void) const {
			return this->tag;
		}

		operator const char *(void) const {
			return this->tag.c_str();
		}
	};

	struct TransformComponent {
		glm::vec3 translation = { 0.f, 0.f, 0.f };
		glm::vec3 rotation = { 0.f, 0.f, 0.f };
		glm::vec3 scale = { 1.f, 1.f, 1.f };

		TransformComponent(void) = default;
		TransformComponent(const TransformComponent &) = default;
		TransformComponent(const glm::vec3 &translation) : translation(translation) {
		}

		operator const glm::mat4(void) const {
			glm::mat4 rotation = glm::toMat4(glm::quat(this->rotation));

			return glm::translate(glm::mat4(1.f), this->translation) * rotation * glm::scale(glm::mat4(1.f), this->scale);
		}

		glm::mat4 get_transform(void) const {
			return *this;
		}
	};

	struct SpriteRendererComponent {
		glm::vec4 color{ 1.0f, 1.0f, 1.0f, 1.0f };
		Ref<Texture2D> texture = nullptr;
		float tiling_factor = 1.0f;

		SpriteRendererComponent(void) = default;
		SpriteRendererComponent(const SpriteRendererComponent &) = default;
		SpriteRendererComponent(const glm::vec4 &color) : color(color) {
		}
		SpriteRendererComponent(const Ref<Texture2D> &tex, const float tiling_factor = 1.0f) : texture(tex), tiling_factor(tiling_factor) {
		}
		SpriteRendererComponent(const glm::vec4 &color, const Ref<Texture2D> &tex, const float tiling_factor = 1.0f)
			: color(color), texture(tex), tiling_factor(tiling_factor) {
		}
	};

	struct CircleRendererComponent {
		glm::vec4 color{ 1.0f, 1.0f, 1.0f, 1.0f };
		float radius = 0.5f;
		float thickness = 1.0f;
		float fade = 5e-3;

		CircleRendererComponent(void) = default;
		CircleRendererComponent(const CircleRendererComponent &) = default;
	};

	struct CameraComponent {
		SceneCamera camera;
		bool primary = true;
		bool fixed_aspect_ratio = false;

		CameraComponent(void) = default;
		CameraComponent(const CameraComponent &) = default;
		CameraComponent(bool primary) : primary(primary) {
		}
	};

	class ScriptableEntity;
	struct NativeScriptComponent {
		ScriptableEntity *instance = nullptr;

		ScriptableEntity *(*instantiate_script)(void);
		void (*destroy_script)(NativeScriptComponent *);

		template <typename T>
		void bind() {
			this->instantiate_script = []() { return static_cast<ScriptableEntity *>(new T()); };
			this->destroy_script = [](NativeScriptComponent *component) {
				delete component->instance;
				component->instance = nullptr;
			};
		}
	};

	struct RigidBody2DComponent {
		enum class BodyType {
			Static = 0,
			Dynamic,
			Kinematic,
		};
		BodyType type = BodyType::Static;
		bool fixed_rotation = false;

		// Storage for runtime:
		void *runtime_body = nullptr;

		RigidBody2DComponent(void) = default;
		RigidBody2DComponent(const RigidBody2DComponent &) = default;
	};

	struct BoxCollider2DComponent {
		glm::vec2 offset = { 0.0f, 0.0f };
		glm::vec2 size = { 0.5f, 0.5f };

		float density = 1.0f;
		float friction = 0.5;
		float restitution = 0.0f;
		float restitution_threshold = 0.5f;

		// Storage for runtime:
		void *runtime_fixture = nullptr;

		BoxCollider2DComponent(void) = default;
		BoxCollider2DComponent(const BoxCollider2DComponent &) = default;
	};

	struct CircleCollider2DComponent {
		glm::vec2 offset = { 0.0f, 0.0f };

		float radius = 0.5f;

		float density = 1.0f;
		float friction = 0.5;
		float restitution = 0.0f;
		float restitution_threshold = 0.5f;

		// Storage for runtime:
		void *runtime_fixture = nullptr;

		CircleCollider2DComponent(void) = default;
		CircleCollider2DComponent(const CircleCollider2DComponent &) = default;
	};

	template<typename... Component>
	struct ComponentGroup {
	};

	using AllComponents = ComponentGroup<TransformComponent, SpriteRendererComponent,CircleRendererComponent, CameraComponent, NativeScriptComponent, RigidBody2DComponent, BoxCollider2DComponent, CircleCollider2DComponent>;
} // namespace Solace
