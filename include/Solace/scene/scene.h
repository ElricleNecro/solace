#pragma once

#include <string>

#include "Solace/core/core.h"
#include "Solace/core/uuid.h"
#include "Solace/renderer/editor_camera.h"

#include "entt.h"

class b2World;

namespace Solace {
	class Entity;
	class SceneHierarchyPanel;
	class SceneSerialiser;

	class Scene {
	public:
		Scene(void);
		virtual ~Scene(void);

		static Ref<Scene> copy(Ref<Scene> from);

		void on_update_editor(EditorCamera &camera, float timestep);
		void on_update_runtime(float timestep);
		void on_update_simulation(EditorCamera &camera, float timestep);

		void on_runtime_start(void);
		void on_simulation_start(void);
		void on_runtime_stop(void);
		void on_simulation_stop(void);

		Entity add(const std::string &name = "", bool with_transform = true);
		Entity add(UUID uuid, const std::string &name = "", bool with_transform = true);
		void remove(Entity entity);
		void duplicate(Entity entity);

		void on_viewport_resize(uint32_t width, uint32_t height);

		Entity get_primary_camera_entity(void);

		template <typename... C>
		auto get_all_entities_with(void) {
			return this->registry.view<C...>();
		}

	private:
		void on_physics_start(void);
		void on_physics_stop(void);
		void on_physics_update(float timestep);

		void render_entity(void);

		template <typename T>
		void on_component_added(Entity &entity, T &component);

		entt::registry registry;
		uint32_t viewport_width = 0, viewport_height = 0;

		b2World *world = nullptr;

		friend class Entity;
		friend class SceneHierarchyPanel;
		friend class SceneSerialiser;
	};
} // namespace Solace
