#pragma once

#define COPY_MOVE_BASIC_LAYER(type)                                                                                    \
	type(const type &dw) : Layer(dw.name){};                                                                       \
	type(type &&dw) noexcept : Layer(dw.name){};                                                                   \
	type &operator=([[maybe_unused]] const type &dw) noexcept {                                                    \
		return *this;                                                                                          \
	}                                                                                                              \
	type &operator=([[maybe_unused]] type &&dw) noexcept {                                                         \
		return *this;                                                                                          \
	}
