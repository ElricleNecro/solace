#pragma once

#include "Solace/vendor/imgui_layer.h"
#include "Solace/widgets/utils.h"
#include "imgui/imgui.h"

namespace Solace {
	class Metrics : public Layer {
	public:
		Metrics(void) : Layer("ImGuiMetrics"){};
		~Metrics(void) override = default;
		COPY_MOVE_BASIC_LAYER(Metrics);

		void on_imgui_render(void) override;
	};
} // namespace Solace
