#pragma once

#include <glm/glm.hpp>

namespace Solace {
	namespace Math {
		bool decompose_transform(const glm::mat4 &transform, glm::vec3 &out_translation, glm::vec3 &out_rotation, glm::vec3 &out_scale);
	} // namespace Math
} // namespace Solace
