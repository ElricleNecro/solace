#pragma once

#include <vector>

#include <glm/glm.hpp>

#include "Solace/Solace.h"

#include "Sandbox/particules.h"

class SandBox2D : public Solace::Layer {
public:
	SandBox2D(int width, int height);

	void on_attach(void) override;
	void on_update(float elapsed_time) override;
	void on_imgui_render(void) override;
	bool on_event(Solace::Events &evt) override;
	void on_detach(void) override;

private:
	glm::vec4 clear_color = { 0.2f, 0.2f, 0.2f, 1.0f };
	glm::vec4 even = { 0.8f, 0.2f, 0.3f, 1.0f };

	Solace::OrthographicCameraController camera_controller;
	Solace::Ref<Solace::Texture2D> texture;
	Solace::Ref<Solace::Texture2D> cherno;
	Solace::Ref<Solace::Texture2D> sprite_cheet;
	Solace::Ref<Solace::SubTexture2D> stairs;
	Solace::Ref<Solace::SubTexture2D> barrel;
	Solace::Ref<Solace::SubTexture2D> tree;
	Solace::Ref<Solace::VertexArray> square_vao;
	Solace::Ref<Solace::FrameBuffer> framebuffer;

	float rotation = 0.f;
	float curve = 0.f;

	ParticleProps particle;
	ParticleSystem particles;

	uint32_t map_width = 0;
	uint32_t map_height = 0;
	std::unordered_map<char, Solace::Ref<Solace::SubTexture2D>> tex_map;
};
