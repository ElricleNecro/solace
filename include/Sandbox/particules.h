#pragma once

#include <glm/glm.hpp>

#include "Solace/Solace.h"

struct ParticleProps {
	glm::vec2 position;
	glm::vec2 velocity, velocity_variation;
	glm::vec4 color_begin, color_end;
	float size_begin, size_end, size_variation;
	float life_time = 1.0f;
};

class ParticleSystem {
public:
	ParticleSystem(size_t size = 100000);

	void on_update(float ts);
	void on_render(const Solace::OrthographicCamera &camera);

	void emit(const ParticleProps &particle_props);

private:
	struct Particle {
		glm::vec2 position;
		glm::vec2 velocity;
		glm::vec4 color_begin, color_end;
		float rotation = 0.0f;
		float size_begin, size_end;

		float life_time = 1.0f;
		float life_remaining = 0.0f;

		bool active = false;
	};
	std::vector<Particle> particle_pool;
	uint32_t pool_index = 999;
};
