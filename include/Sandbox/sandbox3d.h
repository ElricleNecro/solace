#pragma once

#include <SDL2/SDL.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Solace/Solace.h"

#include "Solace/core/core.h"
#include "Solace/core/events.h"
#include "Solace/core/layer.h"
#include "Solace/orthographic_camera_controller.h"
#include "Solace/renderer/buffer.h"
#include "Solace/renderer/rendercommand.h"
#include "Solace/renderer/shader.h"
#include "Solace/renderer/texture.h"
#include "Solace/renderer/vertex_array.h"

#include "Solace/renderer/opengl/opengl_shader.h"

class SandBox3D : public Solace::Layer {
public:
	SandBox3D(int width, int height)
		: Solace::Layer("SandBox3D"), camera_controller(width / height, true), square_position(0.0f){};

	void on_attach(void) override;
	void on_update(float elapsed_time) override;
	void on_imgui_render(void) override;
	bool on_event(Solace::Events &evt) override;
	void on_detach(void) override;

private:
	glm::vec4 clear_color = { 0.2f, 0.2f, 0.2f, 1.0f };

	Solace::OrthographicCameraController camera_controller;

	glm::vec3 square_position;
	float square_speed = 1.0f;

	glm::vec3 even = { 0.8f, 0.2f, 0.3f };
	glm::vec3 odd = { 0.2f, 0.3f, 0.8f };

	Solace::Ref<Solace::VertexArray> vao;
	Solace::Ref<Solace::VertexArray> square_vao;

	Solace::ShaderLibrary library;

	Solace::Ref<Solace::Texture2D> texture;
	Solace::Ref<Solace::Texture2D> logo_tex;
};
