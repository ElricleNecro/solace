#pragma once

#include "Solace/core/core.h"
#include "Solace/scene/entity.h"
#include "Solace/scene/scene.h"

#include <imgui/imgui.h>

namespace Solace {
	class SceneHierarchyPanel {
	public:
		SceneHierarchyPanel(void) = default;
		SceneHierarchyPanel(const Ref<Scene> &scene);

		void set_context(const Ref<Scene> &scene);
		ImGuiID set_dock_id(ImGuiID dock_id);

		void on_imgui_render(void);

		inline Entity get_selected_entity(void) const {
			return this->selection_context;
		};

		inline void set_selected_entity(Entity entity) {
			this->selection_context = entity;
		};

		inline operator Entity(void) const {
			return this->selection_context;
		};

	private:
		void draw_entity_node(Entity entity);
		void draw_components(Entity entity);
		void draw_properties(Entity entity);

		template<typename T>
		void display_add_component_entry(const std::string &entry);

		Ref<Scene> context;

		Entity selection_context;
		bool initial_layout_done = false;
		ImGuiID dock_id, properties_dock_id;

		friend class Scene;
	};
} // namespace Solace
