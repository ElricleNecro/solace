#pragma once

#include <filesystem>

#include "Solace/core/core.h"
#include "Solace/scene/scene.h"

#include "Solace/renderer/texture.h"

namespace Solace {
	class ContentBrowserPanel {
	public:
		ContentBrowserPanel(void);

		void on_imgui_render(void);

	private:
		static const std::filesystem::path root;
		std::filesystem::path current_path = "assets";

		Ref<Texture2D> dir_icon;
		Ref<Texture2D> file_icon;
	};
} // namespace Solace
