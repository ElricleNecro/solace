#pragma once

#include <string>
#include <vector>
#include <filesystem>

#include <glm/glm.hpp>

#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"

#include "Solace/Solace.h"
#include "Solace/renderer/editor_camera.h"

#include "Editor/panels/content_browser_panel.h"
#include "Editor/panels/scene_hierarchy_panel.h"

namespace Solace {
	class EditorLayer : public Layer {
	public:
		EditorLayer(int width, int height);

		void on_attach(void) override;
		void on_update(float elapsed_time) override;
		void on_imgui_render(void) override;
		bool on_event(Events &evt) override;
		void on_detach(void) override;

		void open_scene(std::filesystem::path path = "");

	private:
		void new_scene(void);
		void save_scene(std::filesystem::path path = "");

		void on_duplicate_entity(void);

		void on_overlay_render(void);

		void ui_toolbar(void);

		void scene_play(void);
		void scene_simulate(void);
		void scene_stop(void);

	private:
		glm::vec4 clear_color = { 0.2f, 0.2f, 0.2f, 1.0f };
		glm::vec2 viewport_size = { 0.f, 0.f };
		glm::vec2 viewport_bounds[2] = { { 0.f, 0.f }, { 0.f, 0.f } };

		Ref<FrameBuffer> framebuffer;

		bool viewport_focused = false;
		bool viewport_hovered = false;
		bool initial_layout_done = false;
		bool show_physics_colliders = false;

		glm::vec4 collider_color = { 0.f, 1.f, 0.f, 1.f };

		ImGuiID viewport_dock_id = 0;
		ImGuiID settings_dock_id = 0;
		ImGuiID stats_dock_id = 0;
		ImGuiID hierarchy_panel_dock_id = 0;
		ImGuiID browser_dock_id = 0;
		ImGuiID toolbar_dock_id = 0;

		Ref<Scene> active_scene, editor_scene;
		std::filesystem::path active_scene_path = "";

		EditorCamera editor_camera;

		int imguizmo_op = -1;
		int previous_imguizmo_op = -1;
		Entity hovered_entity = {};

		SceneHierarchyPanel hierarchy_panel;
		ContentBrowserPanel browser_panel;

		enum class SceneState {
			Edit = 0,
			Play = 1,
			Simulate = 2,
		};

		SceneState scene_state = SceneState::Edit;
		Ref<Texture2D> icon_play;
		Ref<Texture2D> icon_simulate;
		Ref<Texture2D> icon_stop;
	};
} // namespace Solace
