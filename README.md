# Installation
In theory, it could be used with Windows or Mac, but it wasn't tested. Only Linux was tested, and will be.

## Clone
```bash
git clone git@gitlab.com:ElricleNecro/solace.git
git submodule update --init --recursive
```

## Dependencies
You will need to manually install the following package:
- sdl2
- glm
- shaderc
- gtk3 (or zenity)
- glbinding
- spirv-cross

## Compile
```bash
premake5 gmake
premake5 export-compile-commands  # Only useful if you develop using LSP
ln -sf compile_commands/debug.json compile_commands.json  # Only useful if you develop using LSP
make
```

# Sources:
https://www.songho.ca/opengl/gl_sphere.html
