local function run_pkg_config(state, args)
	local to_run = "pkg-config --" .. state .. " " .. args
	local exec, err = os.outputof(to_run)

	if err ~= 0 then
		print("Problem while running `" .. to_run .. "`")
	end

	return exec
end

local function str_to_table(str, split)
	local res = {}
	for flag in string.gmatch(str, split) do
		table.insert(res, flag)
	end

	return res
end

local function table_to_str(data)
	local str = ""
	for _, elem in ipairs(data) do
		str = str .. " " .. elem
	end

	return str
end

local function process(state, pkgs)
	local pkg_str = table_to_str(pkgs)
	local flags = run_pkg_config(state, pkg_str)

	return str_to_table(flags, "%S+")
end

return {
	cflags = function (pkgs)
		return process("cflags", pkgs)
	end,

	libs = function (pkgs)
		return process("libs", pkgs)
	end
}
