newoption {
	trigger     = "linux_backend",
	value       = "B",
	description = "Choose a dialog backend for linux",
	allowed = {
		{ "gtk3", "GTK 3 - link to gtk3 directly" },
		{ "zenity", "Zenity - generate dialogs on the end users machine with zenity" }
	}
}

if not _OPTIONS["linux_backend"] then
   _OPTIONS["linux_backend"] = "gtk3"
end

project "nativefiledialog"
	kind "StaticLib"

	location "build/"
	targetdir "build/%{cfg.buildcfg}/lib"

	includedirs {
		"nativefiledialog/src/include/",
	}
	files {
		"nativefiledialog/src/*.h",
		"nativefiledialog/src/include/*.h",
		"nativefiledialog/src/nfd_common.c",
	}

	-- debug/release filters
	filter "configurations:Debug"
		defines {"DEBUG"}
		symbols "On"

	filter "configurations:Release"
		defines {"NDEBUG"}
		optimize "On"

	filter "system:windows"
		language "C++"
		files {"nativefiledialog/src/nfd_win.cpp"}

	filter {"action:gmake or action:xcode4"}
		buildoptions {"-fno-exceptions"}

	filter "system:macosx"
		language "C"
		files {"nativefiledialog/src/nfd_cocoa.m"}

	filter {"system:linux", "options:linux_backend=gtk3"}
		language "C"
		pic "On"
		files {"nativefiledialog/src/nfd_gtk.c"}
		buildoptions {"`pkg-config --cflags gtk+-3.0`"}

	filter {"system:linux", "options:linux_backend=zenity"}
		language "C"
		files {"nativefiledialog/src/nfd_zenity.c"}

	filter "action:vs*"
		defines { "_CRT_SECURE_NO_WARNINGS" }
