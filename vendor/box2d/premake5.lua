project "box2d"
	kind "StaticLib"
	language "C++"
	cppdialect "C++20"

	location "build/"
	targetdir "build/%{cfg.buildcfg}/lib"

	files
	{
		"box2d/src/**.h",
		"box2d/src/**.cpp",

		"box2d/include/**.h"
	}

	includedirs
	{
		"box2d/include",
		"box2d/src"
	}

	filter "system:windows"
		systemversion "latest"
		staticruntime "On"

	filter "system:linux"
		pic "On"
		systemversion "latest"
		staticruntime "On"

	filter "configurations:Debug"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		runtime "Release"
		optimize "on"
