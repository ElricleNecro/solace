#type vertex
#version 450 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;
layout(location = 2) in int entity_id;

layout(std140, binding = 0) uniform Camera {
	mat4 view_proj;
};

struct VertexOutput {
	vec4 color;
};

layout (location = 0) out VertexOutput vertex_output;
layout (location = 1) out flat int v_entity_id;

void main() {
	vertex_output.color = color;

	v_entity_id = entity_id;

	gl_Position = view_proj * vec4(position, 1.0);
}

#type fragment
#version 450 core
layout(location = 0) out vec4 output_color;
layout(location = 1) out int output_entity_id;

struct VertexOutput {
	vec4 color;
};

layout (location = 0) in VertexOutput in_data;
layout (location = 1) in flat int v_entity_id;

void main() {
	output_color = in_data.color;
	output_entity_id = v_entity_id;
}
