#type vertex
#version 330 core
layout(location = 0) in vec3 position;

uniform mat4 view_proj;
uniform mat4 transform;

out vec3 v_pos;

void main() {
	v_pos = position;
	gl_Position = view_proj * transform * vec4(position, 1.0);
}

#type fragment
#version 330 core
layout(location = 0) out vec4 color;

in vec3 v_pos;
uniform vec4 u_color;

void main(){
	color = u_color;
}
