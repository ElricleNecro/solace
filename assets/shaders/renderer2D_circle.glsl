#type vertex
#version 450 core

layout(location = 0) in vec3 world_position;
layout(location = 1) in vec3 local_position;
layout(location = 2) in vec4 color;
layout(location = 3) in float thickness;
layout(location = 4) in float fade;
layout(location = 5) in int entity_id;

layout(std140, binding = 0) uniform Camera {
	mat4 view_proj;
};

struct VertexOutput {
	vec3 local_position;
	vec4 color;
	float thickness;
	float fade;
};

layout (location = 0) out VertexOutput vertex_output;
layout (location = 4) out flat int v_entity_id;

void main() {
	vertex_output.local_position = local_position;
	vertex_output.color = color;
	vertex_output.thickness = thickness;
	vertex_output.fade = fade;

	v_entity_id = entity_id;

	gl_Position = view_proj * vec4(world_position, 1.0);
}

#type fragment
#version 450 core
layout(location = 0) out vec4 output_color;
layout(location = 1) out int output_entity_id;

struct VertexOutput {
	vec3 local_position;
	vec4 color;
	float thickness;
	float fade;
};

layout (location = 0) in VertexOutput in_data;
layout (location = 4) in flat int v_entity_id;

void main() {
	float distance = 1.0 - length(in_data.local_position);
	float alpha_color = smoothstep(0.0, in_data.fade, distance);
	alpha_color *= smoothstep(in_data.thickness + in_data.fade, in_data.thickness, distance);

	if (alpha_color == 0.0)
		discard;

	output_color = in_data.color;
	output_color.a *= alpha_color;

	output_entity_id = v_entity_id;
}
