#type vertex
#version 450 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;
layout(location = 2) in vec2 tex_coord;
layout(location = 3) in float tex_index;
layout(location = 4) in float tiling_factor;
layout(location = 5) in int entity_id;

layout(std140, binding = 0) uniform Camera {
	mat4 view_proj;
};

struct VertexOutput {
	vec4 color;
	vec2 tex_coord;
	float tiling_factor;
};

layout (location = 0) out VertexOutput vertex_output;
layout (location = 3) out flat float v_tex_index;
layout (location = 4) out flat int v_entity_id;

void main() {
	vertex_output.color = color;
	vertex_output.tex_coord = tex_coord;
	vertex_output.tiling_factor = tiling_factor;

	v_tex_index = tex_index;

	v_entity_id = entity_id;

	gl_Position = view_proj * vec4(position, 1.0);
}

#type fragment
#version 450 core
layout(location = 0) out vec4 color;
layout(location = 1) out int color2;

struct VertexOutput {
	vec4 color;
	vec2 tex_coord;
	float tiling_factor;
};

layout (location = 0) in VertexOutput in_data;
layout (location = 3) in flat float v_tex_index;
layout (location = 4) in flat int v_entity_id;

layout (binding = 0) uniform sampler2D u_textures[32];

void main() {
	vec4 tex_color = in_data.color;

	switch(int(v_tex_index)) {
		case  0: tex_color *= texture(u_textures[ 0], in_data.tex_coord * in_data.tiling_factor); break;
		case  1: tex_color *= texture(u_textures[ 1], in_data.tex_coord * in_data.tiling_factor); break;
		case  2: tex_color *= texture(u_textures[ 2], in_data.tex_coord * in_data.tiling_factor); break;
		case  3: tex_color *= texture(u_textures[ 3], in_data.tex_coord * in_data.tiling_factor); break;
		case  4: tex_color *= texture(u_textures[ 4], in_data.tex_coord * in_data.tiling_factor); break;
		case  5: tex_color *= texture(u_textures[ 5], in_data.tex_coord * in_data.tiling_factor); break;
		case  6: tex_color *= texture(u_textures[ 6], in_data.tex_coord * in_data.tiling_factor); break;
		case  7: tex_color *= texture(u_textures[ 7], in_data.tex_coord * in_data.tiling_factor); break;
		case  8: tex_color *= texture(u_textures[ 8], in_data.tex_coord * in_data.tiling_factor); break;
		case  9: tex_color *= texture(u_textures[ 9], in_data.tex_coord * in_data.tiling_factor); break;
		case 10: tex_color *= texture(u_textures[10], in_data.tex_coord * in_data.tiling_factor); break;
		case 11: tex_color *= texture(u_textures[11], in_data.tex_coord * in_data.tiling_factor); break;
		case 12: tex_color *= texture(u_textures[12], in_data.tex_coord * in_data.tiling_factor); break;
		case 13: tex_color *= texture(u_textures[13], in_data.tex_coord * in_data.tiling_factor); break;
		case 14: tex_color *= texture(u_textures[14], in_data.tex_coord * in_data.tiling_factor); break;
		case 15: tex_color *= texture(u_textures[15], in_data.tex_coord * in_data.tiling_factor); break;
		case 16: tex_color *= texture(u_textures[16], in_data.tex_coord * in_data.tiling_factor); break;
		case 17: tex_color *= texture(u_textures[17], in_data.tex_coord * in_data.tiling_factor); break;
		case 18: tex_color *= texture(u_textures[18], in_data.tex_coord * in_data.tiling_factor); break;
		case 19: tex_color *= texture(u_textures[19], in_data.tex_coord * in_data.tiling_factor); break;
		case 20: tex_color *= texture(u_textures[20], in_data.tex_coord * in_data.tiling_factor); break;
		case 21: tex_color *= texture(u_textures[21], in_data.tex_coord * in_data.tiling_factor); break;
		case 22: tex_color *= texture(u_textures[22], in_data.tex_coord * in_data.tiling_factor); break;
		case 23: tex_color *= texture(u_textures[23], in_data.tex_coord * in_data.tiling_factor); break;
		case 24: tex_color *= texture(u_textures[24], in_data.tex_coord * in_data.tiling_factor); break;
		case 25: tex_color *= texture(u_textures[25], in_data.tex_coord * in_data.tiling_factor); break;
		case 26: tex_color *= texture(u_textures[26], in_data.tex_coord * in_data.tiling_factor); break;
		case 27: tex_color *= texture(u_textures[27], in_data.tex_coord * in_data.tiling_factor); break;
		case 28: tex_color *= texture(u_textures[28], in_data.tex_coord * in_data.tiling_factor); break;
		case 29: tex_color *= texture(u_textures[29], in_data.tex_coord * in_data.tiling_factor); break;
		case 30: tex_color *= texture(u_textures[30], in_data.tex_coord * in_data.tiling_factor); break;
		case 31: tex_color *= texture(u_textures[31], in_data.tex_coord * in_data.tiling_factor); break;
	}

	color = tex_color;

	if( tex_color.a == 0.0 )
		discard;

	color2 = v_entity_id;
}
