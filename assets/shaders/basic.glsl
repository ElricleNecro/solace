#type vertex
#version 330 core
layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;

uniform mat4 view_proj;
uniform mat4 transform;

out vec3 v_pos;
out vec4 v_color;

void main() {
	v_pos = position;
	v_color = color;
	gl_Position = view_proj * transform * vec4(position, 1.0);
}

#type fragment
#version 330 core
layout(location = 0) out vec4 color;

in vec3 v_pos;
in vec4 v_color;

void main(){
	//color = vec4(v_pos * 0.5 + 0.5, 1.0);
	color = v_color;
}
