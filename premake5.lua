-- io.input("config.h.cmake")
-- local text = io.read("*a")

-- text = string.gsub(text, "@PLUGIN_API_VERSION_MAJOR@", "0")
-- text = string.gsub(text, "@PLUGIN_API_VERSION_MINOR@", "1")

-- text = string.gsub(text, "@AI_VERSION_MAJOR@", "0")
-- text = string.gsub(text, "@AI_VERSION_MINOR@", "1")

-- io.output("include/config.hpp")
-- io.write(text)
-- io.close()

if not _OPTIONS["linux_backend"] then
   _OPTIONS["linux_backend"] = "gtk3"
end

local pkgconfig = require "premake.pkgconfig"
local sdl2build = ""
local sdl2link  = ""

if _OPTIONS["linux_backend"] == "gtk3" then
	sdl2build = pkgconfig.cflags { "sdl2", "glm", "shaderc", "gtk+-3.0" }
	sdl2link  = pkgconfig.libs { "sdl2", "glm", "shaderc", "gtk+-3.0" }
else
	sdl2build = pkgconfig.cflags { "sdl2", "glm", "shaderc" }
	sdl2link  = pkgconfig.libs { "sdl2", "glm", "shaderc" }
end

workspace "Engine"
	architecture "x86_64"

	cppdialect "C++20"

	configurations { "Debug", "Release" }
		includedirs {
			"include/",
			"include/imgui",
			"vendor/entt/include",
			"vendor/spdlog/spdlog/include",
		}

		warnings "Extra"

		flags "MultiProcessorCompile"

		buildoptions(sdl2build)
		linkoptions(sdl2link)

group "Dependencies"
	include "vendor/imgui"
	include "vendor/yaml-cpp"
	include "vendor/nativefiledialog"
	include "vendor/imguizmo"
	include "vendor/box2d"
group ""

project "Solace"
	filename ".solace"
	kind "SharedLib"
	language "C++"

	location "build/"
	targetdir "build/%{cfg.buildcfg}/lib"

	pchheader "spch.h"

	files {
		"src/%{prj.name}/*.cpp",
		"src/%{prj.name}/core/**.cpp",
		"src/%{prj.name}/renderer/**.cpp",
		"src/%{prj.name}/scene/**.cpp",
		"src/%{prj.name}/utils/**.cpp",
		"src/%{prj.name}/math/**.cpp",
		"src/%{prj.name}/vendor/**.cpp",
		"src/%{prj.name}/widgets/**.cpp",
	}

	includedirs {
		"include/%{prj.name}",
		"vendor/imgui/imgui",
		"vendor/imgui",
		"vendor/imguizmo",
		"vendor/imguizmo/imguizmo",
		"vendor/entt/include",
		"vendor/spdlog/spdlog/include",
		"vendor/yaml-cpp/yaml-cpp/include",
		"vendor/nativefiledialog/nativefiledialog/src/include",
		"vendor/box2d/box2d/include",
	}

	defines {
		"USE_GLBINDING",
		"IMGUI_IMPL_OPENGL_LOADER_GLBINDING3"
	}

	links {
		"glbinding",
		"ImGui",
		"ImGuizmo",
		"yaml-cpp",
		"nativefiledialog",
		"box2d",
	}

	filter "system:windows"
		systemversion "latest"
		defines { "S_PLATFORM_WINDOWS", "HZ_BUILD_DLL" }
		staticruntime "On"
		pchsource "src/%{prj.name}/spch.cpp"

		files {
			"src/%{prj.name}/platform/windows/**.cpp",
		}

	filter "system:linux"
		systemversion "latest"
		defines { "S_PLATFORM_LINUX" }
		pic "On"
		staticruntime "On"

		files {
			"src/%{prj.name}/platform/linux/**.cpp",
		}

	filter "configurations:Debug"
		defines { "DEBUG", "SOLACE_DEBUG", "SL_PROFILE" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"
		flags { "LinkTimeOptimization", "FatalWarnings" }

project "Sandbox"
	filename ".sandbox"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++20"

	location "build/"
	targetdir "build/%{cfg.buildcfg}/bin"

	defines {
		"USE_GLBINDING"
	}

	files { "src/%{prj.name}/**.cpp" }

	includedirs {
		"include/Solace",
		"include/%{prj.name}",
		"vendor/imgui",
		"vendor/imguizmo",
		"vendor/entt/include",
		"vendor/spdlog/spdlog/include",
		"vendor/box2d/box2d/include",
	}

	links {
		"glbinding",
		"ImGui",
		"Solace",
		"spirv-cross-core",
		"spirv-cross-cpp",
		"spirv-cross-glsl",
	}

	libdirs {
		"build/%{cfg.buildcfg}/lib"
	}

	filter "system:windows"
		systemversion "latest"
		defines { "S_PLATFORM_WINDOWS", "HZ_BUILD_DLL" }
		staticruntime "On"

	filter "system:linux"
		systemversion "latest"
		defines { "S_PLATFORM_LINUX" }
		pic "On"
		staticruntime "On"

	filter "configurations:Debug"
		defines { "DEBUG", "SOLACE_DEBUG", "SL_PROFILE" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"
		flags { "LinkTimeOptimization", "FatalWarnings" }

project "Editor"
	filename ".editor"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++20"

	location "build/"
	targetdir "build/%{cfg.buildcfg}/bin"

	files { "src/%{prj.name}/**.cpp" }

	includedirs {
		"include/Solace",
		"include/%{prj.name}",
		"vendor/imgui",
		"vendor/imguizmo",
		"vendor/entt/include",
		"vendor/spdlog/spdlog/include",
		"vendor/box2d/box2d/include",
	}

	links {
		"glbinding",
		"ImGui",
		"Solace",
		"spirv-cross-core",
		"spirv-cross-cpp",
		"spirv-cross-glsl",
	}

	libdirs {
		"build/%{cfg.buildcfg}/lib"
	}

	filter "system:windows"
		systemversion "latest"
		defines { "S_PLATFORM_WINDOWS", "HZ_BUILD_DLL" }
		staticruntime "On"

	filter "system:linux"
		systemversion "latest"
		defines { "S_PLATFORM_LINUX" }
		pic "On"
		staticruntime "On"

	filter "configurations:Debug"
		defines { "DEBUG", "SOLACE_DEBUG", "SL_PROFILE" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"
		flags { "LinkTimeOptimization", "FatalWarnings" }
